package com.sers.mobility.study.app.fragments.optionsMenu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.api.webserviceImpl.WebService;
import com.sers.mobility.study.app.baseFiles.BaseReportFragment;
import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.constants.AppMessages;
import com.sers.mobility.study.app.custom.SystemProgressDialog;
import com.sers.mobility.study.app.models.transients.ServerProfileUpdate;
import com.sers.mobility.study.app.sessionManager.SessionManager;
import com.sers.mobility.study.app.utils.AppUtils;

import java.util.HashMap;


public class ProfileFragment extends BaseReportFragment {

    private TextView profilePasswordFieldHeader, profileConfirmPasswordFieldHeader;
    private EditText profileFirstNameField, profileLastNameField, profilePhoneNumberField, profileUsernameField, profilePasswordField, profileConfirmPasswordField;
    private AppCompatButton accountButton = null;
    private RadioButton profileEditPassword, profileDontEditPassword, profileGenderMale, profileGenderFemale;
    private String selectedGender = AppConstants.UNKNOWN;
    private SessionManager sessionManager;
    private String userId;
    private HashMap<String, String> user;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ProfileFragment(Activity activity) {
        super(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        profileFirstNameField = (EditText) rootView.findViewById(R.id.profileFirstNameField);
        profileLastNameField = (EditText) rootView.findViewById(R.id.profileLastNameField);
        profilePhoneNumberField = (EditText) rootView.findViewById(R.id.profilePhoneNumberField);
        profileUsernameField = (EditText) rootView.findViewById(R.id.profileUsernameField);

        profilePasswordFieldHeader = (TextView) rootView.findViewById(R.id.profilePasswordFieldHeader);
        profileConfirmPasswordFieldHeader = (TextView) rootView.findViewById(R.id.profileConfirmPasswordFieldHeader);
        profilePasswordField = (EditText) rootView.findViewById(R.id.profilePasswordField);
        profileConfirmPasswordField = (EditText) rootView.findViewById(R.id.profileConfirmPasswordField);
        profileEditPassword = (RadioButton) rootView.findViewById(R.id.profileEditPassword);
        profileDontEditPassword = (RadioButton) rootView.findViewById(R.id.profileDontEditPassword);
        profileGenderFemale = (RadioButton) rootView.findViewById(R.id.profileGenderFemale);
        profileGenderMale = (RadioButton) rootView.findViewById(R.id.profileGenderMale);
        accountButton = (AppCompatButton) rootView.findViewById(R.id.profileButton);
        accountButton.setOnClickListener(this);

        dateOfBirth = (EditText) rootView.findViewById(R.id.dateOfBirth);
        initializeDatesPickers(ProfileFragment.this);

        systemProgressDialog = new SystemProgressDialog(getActivity());
        sessionManager = new SessionManager(getActivity().getApplicationContext());
        user = sessionManager.getUserDetails();
        userId = user.get(SessionManager.KEY_USER_ID);


        if (user != null) {
            profileFirstNameField.setText(user.get(SessionManager.KEY_FIRST_NAME));
            profileLastNameField.setText(user.get(SessionManager.KEY_LAST_NAME));
            profilePhoneNumberField.setText(user.get(SessionManager.KEY_PHONE_NUMBER));
            profileUsernameField.setText(user.get(SessionManager.KEY_USERNAME));
            dateOfBirth.setText(user.get(SessionManager.KEY_DATE_OF_BIRTH));
            setUserGender(user.get(SessionManager.KEY_GENDER));
        }
        changeFieldStatus(View.VISIBLE);

        profileEditPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFieldStatus(View.VISIBLE);
            }
        });

        profileDontEditPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFieldStatus(View.GONE);
            }
        });

        profileGenderFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = AppConstants.FEMALE;
            }
        });

        profileGenderMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = AppConstants.MALE;
            }
        });
        return rootView;
    }

    private void setUserGender(String gender) {
        if (gender != null) {
            if (gender.equalsIgnoreCase(AppConstants.MALE)) {
                profileGenderMale.setChecked(true);
                profileGenderFemale.setChecked(false);
            } else if (gender.equalsIgnoreCase(AppConstants.FEMALE)) {
                profileGenderMale.setChecked(false);
                profileGenderFemale.setChecked(true);
            }
        }
    }

    private void changeFieldStatus(int status) {
        profilePasswordFieldHeader.setVisibility(status);
        profileConfirmPasswordFieldHeader.setVisibility(status);
        profilePasswordField.setVisibility(status);
        profileConfirmPasswordField.setVisibility(status);
    }

    @Override
    public void getContent(boolean isRefreshing) {

    }


    @Override
    public void onClick(View view) {
        if (view == accountButton) {
            if (String.valueOf(profileFirstNameField.getText()).isEmpty() || String.valueOf(profileFirstNameField.getText()) == null) {
                AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide a first name", getActivity());
            } else if (String.valueOf(profileLastNameField.getText()).isEmpty() || String.valueOf(profileLastNameField.getText()) == null) {
                AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide a last name", getActivity());
            }/* else if (String.valueOf(selectedGender).isEmpty() || String.valueOf(selectedGender) == null) {
                AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Please select your gender", getActivity());
            }*/ else if (String.valueOf(profilePasswordField.getText()).isEmpty() || String.valueOf(profilePasswordField.getText()) == null) {
                AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide a password", getActivity());
            } else if (String.valueOf(profileConfirmPasswordField.getText()).isEmpty() || String.valueOf(profileConfirmPasswordField.getText()) == null) {
                AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Please confirm your password", getActivity());
            } else {
                if (String.valueOf(profilePasswordField.getText()).equals(String.valueOf(profileConfirmPasswordField.getText()))) {
                    ServerProfileUpdate serverProfileUpdate = new ServerProfileUpdate(0, userId,
                            null,
                            null,
                            String.valueOf(profileFirstNameField.getText()),
                            String.valueOf(profileLastNameField.getText()),
                            user.get(SessionManager.KEY_LOCATION),
                            user.get(SessionManager.KEY_GENDER),
                            String.valueOf(profileUsernameField.getText()),
                            String.valueOf(profilePasswordField.getText()),
                            String.valueOf(dateOfBirth.getText()),
                            String.valueOf(profilePhoneNumberField.getText()),
                            String.valueOf(true),
                            String.valueOf(AppConstants.DEFAULT_DISTANCE_APART_IN_KILOMETRES));
                    if (new AppUtils().isNetworkAvailable(activity)) {
                        new WebService(getActivity(), systemProgressDialog, getActivity(), null, serverProfileUpdate, null, null).getAppUpdateProfileService();
                        if (profilePasswordField != null) profilePasswordField.setText("");
                        if (profileConfirmPasswordField != null)
                            profileConfirmPasswordField.setText("");
                    } else {
                        AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "You can only update your profile with a stable internet connection", activity);
                    }


                } else
                    AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Passwords don't match \nPlease make sure the provided passwords are the same", getActivity());
            }
        }


    }

}

