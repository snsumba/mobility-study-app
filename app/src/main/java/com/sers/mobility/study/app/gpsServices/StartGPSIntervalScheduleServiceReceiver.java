package com.sers.mobility.study.app.gpsServices;

/**
 * Created by kdeo on 4/14/2017.
 */
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartGPSIntervalScheduleServiceReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, LocationService.class);
        context.startService(service);
    }
}
