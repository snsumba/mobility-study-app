package com.sers.mobility.study.app.models.databases;


import org.sers.kpa.datasource.annotations.Column;
import org.sers.kpa.datasource.annotations.Entity;
import org.sers.kpa.datasource.annotations.Table;
import org.sers.kpa.datasource.datamanager.DatabaseJSONUtils;

@Entity
@Table(name = "time_diaries")
public class TimeDiary extends AppBaseEntity {


    @Column(name = "suggestion", nullable = true)
    public String suggestion;

    @Column(name = "suggestion_id", nullable = true)
    public String suggestionId;


    @Column(name = "start_time", nullable = true)
    public String startTime;

    @Column(name = "end_time", nullable = true)
    public String endTime;

    @Column(name = "ordering_parameter", nullable = true)
    public String orderingParameter;

    public TimeDiary() {
    }

    public TimeDiary(String suggestion) {
        this.suggestion = suggestion;
    }

    public TimeDiary(String date, String suggestion, String suggestionId, String startTime, String endTime) {
        super(date);
        this.suggestion = suggestion;
        this.suggestionId = suggestionId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.orderingParameter = date + " " + endTime;
    }

    public String getOrderingParameter() {
        return orderingParameter;
    }

    public void setOrderingParameter(String orderingParameter) {
        this.orderingParameter = orderingParameter;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public String getSuggestionId() {
        return suggestionId;
    }

    public void setSuggestionId(String suggestionId) {
        this.suggestionId = suggestionId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String toString() {
        try {
            return DatabaseJSONUtils.convertModelToJSON(this, TimeDiary.class).toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
