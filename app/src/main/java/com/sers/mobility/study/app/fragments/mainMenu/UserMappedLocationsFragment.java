package com.sers.mobility.study.app.fragments.mainMenu;


import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.melnykov.fab.FloatingActionButton;
import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.baseFiles.BaseMainFragment;
import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.databaseUtils.QueryDatabaseUtil;
import com.sers.mobility.study.app.gpsServices.GPSTracker;
import com.sers.mobility.study.app.models.databases.CapturedLocation;
import com.sers.mobility.study.app.sessionManager.SessionManager;

import java.util.HashMap;
import java.util.List;

/**
 * @see https://www.tutorialspoint.com/android/android_google_maps.htm
 * @see https://developer.android.com/training/maps/index.html
 * @see https://stackoverflow.com/questions/tagged/google-maps
 */
public class UserMappedLocationsFragment extends BaseMainFragment implements OnMapReadyCallback {

    private GoogleMap googleMap;
    private SupportMapFragment mapFragment = null;
    private GPSTracker gpsTracker = null;

    public UserMappedLocationsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_mapped_locations, container, false);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            System.out.println("Version :: " + android.os.Build.VERSION.SDK_INT);
            System.out.println("Version :: " + android.os.Build.VERSION.SDK_INT);
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, MY_PERMISSIONS_REQUEST_LOCATION_INITIAL);
            }
        }
        gpsTracker = new GPSTracker(getActivity());
        if (this.googleMap == null) {
            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gpsTracker = new GPSTracker(getActivity());
                if (googleMap == null) {
                    mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
                    mapFragment.getMapAsync(UserMappedLocationsFragment.this);
                }
            }
        });

        return rootView;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            System.out.println("Version :: " + android.os.Build.VERSION.SDK_INT);
            System.out.println("Version :: " + android.os.Build.VERSION.SDK_INT);
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, MY_PERMISSIONS_REQUEST_LOCATION_INITIAL);
            }
        }
        try {
            this.googleMap = googleMap;
            this.googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            this.googleMap.getUiSettings().setZoomGesturesEnabled(true);


            this.sessionManager = new SessionManager(getActivity());
            if (sessionManager != null) {
                HashMap<String, String> user = sessionManager.getUserDetails();
                userId = (user.get(SessionManager.KEY_USER_ID) != null ? user.get(SessionManager.KEY_USER_ID) : "n.a");
            }
            Location location = this.gpsTracker.getLocation();
            List<CapturedLocation> listOfLocations = new QueryDatabaseUtil().getListOfCapturedLocationsFromDatabase(userId);
            if (listOfLocations != null) {
                if (listOfLocations.size() != 0) {
                    for (CapturedLocation capturedLocation : listOfLocations) {
                        double latitude = Float.parseFloat(capturedLocation.latitude);
                        double longitude = Float.parseFloat(capturedLocation.longitude);

                        if (latitude == 0 || longitude == 0) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                        LatLng ur_location = new LatLng(latitude, longitude);
                        this.googleMap.addMarker(new MarkerOptions().position(ur_location).title(
                                (capturedLocation.placeName != null ? capturedLocation.placeName : AppConstants.LOCATION_NOT_FOUND))
                                .snippet(String.valueOf(("(" + latitude + "," + longitude + ")"))));
                        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ur_location, 16));
                    }
                } else {
                    double defaultLatitude = 0;
                    double defaultLongitude = 0;
                    if (gpsTracker != null) {
                        defaultLatitude = location.getLatitude();
                        defaultLongitude = location.getLongitude();
                    }
                    LatLng latLng = new LatLng(defaultLatitude, defaultLongitude);
                    this.googleMap.addMarker(new MarkerOptions().position(latLng).title(AppConstants.USER_LOCATION_TITLE).snippet("(" + defaultLatitude + "," + defaultLongitude + ")"));
                    this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
