package com.sers.mobility.study.app.notification;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.activities.LoginActivity;
import com.sers.mobility.study.app.activities.MainActivity;
import com.sers.mobility.study.app.sessionManager.SessionManager;

import java.util.HashMap;

public class BackGroundNotificationOperation {

    private Context context = null;

    public BackGroundNotificationOperation() {
    }

    public BackGroundNotificationOperation(Context context) {
        this.context = context;
    }

    public void showNotification() {
        String title = "Mobility study";
        String subject = "Notification";
        String body = "Please tell us where you are";

        SessionManager sessionManager = new SessionManager(this.context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        String isLoggedIn = user.get(SessionManager.KEY_IS_LOGGED_IN);

        Intent intent = new Intent(this.context, LoginActivity.class);
        if (isLoggedIn == "true") {
            intent = new Intent(this.context, MainActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this.context, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        Ringtone ringtone = RingtoneManager.getRingtone(context, notification);
        ringtone.play();
        android.support.v4.app.NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this.context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(notification)
                .setGroup(subject)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }


}
