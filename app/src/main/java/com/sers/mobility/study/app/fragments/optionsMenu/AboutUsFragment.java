package com.sers.mobility.study.app.fragments.optionsMenu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.baseFiles.BaseMainFragment;
import com.sers.mobility.study.app.sessionManager.SessionManager;

import java.util.HashMap;


public class AboutUsFragment extends BaseMainFragment {

    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about_us, container, false);
        TextView welcomeInfo = (TextView) rootView.findViewById(R.id.welcomeInfo);
        SessionManager sessionManager = new SessionManager(getActivity());
        HashMap<String, String> user = sessionManager.getUserDetails();
        String firstName = user.get(SessionManager.KEY_FIRST_NAME);
        String lastName = user.get(SessionManager.KEY_LAST_NAME);
        welcomeInfo.setText("Welcome " + (firstName != null ? firstName : lastName) + ",");
        return rootView;
    }

}
