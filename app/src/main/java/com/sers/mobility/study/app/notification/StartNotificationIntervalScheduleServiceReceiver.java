package com.sers.mobility.study.app.notification;

/**
 * Created by kdeo on 4/14/2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sers.mobility.study.app.gpsServices.LocationService;

public class StartNotificationIntervalScheduleServiceReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, NotificationService.class);
        context.startService(service);
    }
}
