package com.sers.mobility.study.app.utils;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.sers.mobility.study.app.constants.AppConstants;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.InetAddress;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class AppUtils {

    public static final long CONNECTION_TIME_OUT = 300;
    public static final String DEFAULT_DELAY_IN_MINUTES = "3";
    public static final String APP_VERSION = "11.0.0";
    public static int PERMITTED_DAYS_RANGE = 7;

    public static String selectedTime(int hour, int min) {
        String format = "";
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }
        return String.valueOf(new StringBuilder().append(hour).append(" : ").append(min)
                .append(" ").append(format));
    }

    public static String generateUserId() {
        return new BigInteger(130, new SecureRandom()).toString(32);
    }

    public static String getCurrentDate() {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
    }

    public static String getCurrentDateOnly() {
        return new SimpleDateFormat("yyyy/MM/dd").format(new Date());
    }

    public static String getCurrentTime() {
        return new SimpleDateFormat(" HH:mm:ss").format(new Date());
    }

    public static void showConfirmMessage(String message, Activity activity, DialogInterface.OnClickListener dialogClickListener) {
        DialogInterface.OnClickListener closeDialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pressed) {
                switch (pressed) {
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setTitle(AppConstants.CONFIRM_OPERATION);
        builder.setMessage(message).setNegativeButton("No", closeDialogClickListener)
                .setPositiveButton("Yes", dialogClickListener)
                .show();
    }

    public static int getDaysBetweenDates(String startDate, String endDate) {
        return Days.daysBetween(new LocalDate(new Date(startDate != null ? startDate : AppUtils.getCurrentDate()).getTime()),
                new LocalDate(new Date(endDate != null ? endDate : AppUtils.getCurrentDate()).getTime())).getDays();
    }

    public static String getTransactingAccountBalance() {
        return String.valueOf(0);
    }

    public static void setEditable(EditText editText, boolean isEditable) {
        editText.setCursorVisible(isEditable);
        editText.setPressed(isEditable);
        editText.setFocusable(isEditable);
    }

    public List<String> getSpinnerItems(List<?> listOfModels) {
        List<String> list = new ArrayList<String>();
        try {
            if (listOfModels != null) {
                if (listOfModels.size() > 0) {
                    String[] spinnerItemNames = new String[listOfModels.size()];
                    for (int i = 0; i < listOfModels.size(); i++) {
                        spinnerItemNames[i] = listOfModels.get(i).toString();
                    }
                    for (int x = 0; x < spinnerItemNames.length; x++) {
                        list.add(spinnerItemNames[x]);
                    }
                    Collections.sort(list);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static String encryptData(String data_to_encrypt) {
        // return DataProtector.encryptWithKeyAndSalt(data_to_encrypt, DataProtector.API_SALT, DataProtector.API_KEY);
        return data_to_encrypt;
    }

    public static String decryptData(String data_to_decrypt) {
        //  return DataProtector.decryptWithKeyAndSalt(data_to_decrypt, DataProtector.API_SALT, DataProtector.API_KEY);
        return data_to_decrypt;
    }

    public static void showSnackBarMessage(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    public boolean checkNetwork(Context context, Activity activity) {
        if (isNetworkAvailable(activity)) {
            return true;
        } else {
            noInternet(context);
        }
        return false;
    }

    public void noInternet(final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("No internet access. Please turn it on to use this Application")
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, final int id) {
                        Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(i);

                    }

                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        Log.d("LOG_TAG", "No network available!");
    }

    public void noInternet(final Context context, String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("No internet access. Please turn it on to continue with : " + message)
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, final int id) {
                        Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(i);

                    }

                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        Log.d("LOG_TAG", "No network available!");
    }

    public boolean isGoogleReachable() {
        return isNetAddressAvailable(AppConstants.DEFAULT_ADDRESS);
    }

    public boolean isNetAddressAvailable(String net_address) {
        try {
            boolean network = false;
            if (InetAddress.getByName(net_address).isReachable(4000) == true) {
                network = true;
            } else {
                network = false; // Ping doesnt work
            }
            return network;
        } catch (Exception exc) {
            System.out.println("exc : " + exc.getMessage());
            exc.printStackTrace();
            return false;
        }
    }

    public boolean isNetworkAvailable(Context context) {
        boolean haveConnectedWifi = Boolean.FALSE;
        boolean haveConnectedMobile = Boolean.FALSE;
        boolean isNetworkConnected = Boolean.FALSE;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();

        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected()) {
                    isNetworkConnected = ni.isAvailable();
                    haveConnectedWifi = true;
                }

            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected()) {
                    isNetworkConnected = ni.isAvailable();
                    haveConnectedMobile = true;
                }
        }

        System.out.println("***** Network type connected : " + isNetworkConnected);
        System.out.println("***** Network type wifi available : " + haveConnectedWifi);
        System.out.println("***** Network type data available : " + haveConnectedMobile);

        return ((haveConnectedWifi || haveConnectedMobile) && isNetworkConnected);
    }

    public static void showToastMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    /**
     * @param message
     * @param
     */
    public static void showMessage(boolean cancelable, String title, String message, Context context) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pressed) {
                switch (pressed) {
                    case DialogInterface.BUTTON_POSITIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(cancelable);
        builder.setTitle(title);
        builder.setMessage(message).setPositiveButton("Ok", dialogClickListener).show();
    }

    public static void showMessageWithOutButton(boolean cancelable, String title, String message, Context context) {
        /*DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pressed) {
                switch (pressed) {
                    case DialogInterface.BUTTON_POSITIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(cancelable);
        builder.setTitle(title);
        builder.setMessage(message).show();*/

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pressed) {
                switch (pressed) {
                    case DialogInterface.BUTTON_POSITIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(cancelable);
        builder.setTitle(title);
        builder.setMessage(message).setPositiveButton("Ok", dialogClickListener).show();
    }


    /**
     * @param message
     * @param activity
     * @param reflectiveMethod
     * @param showNoOption
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public static void showMessage(String title, String message, final Activity activity, final Method reflectiveMethod,
                                   boolean showNoOption) throws IllegalArgumentException, InvocationTargetException, IllegalAccessException {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pressed) {
                switch (pressed) {
                    case DialogInterface.BUTTON_POSITIVE:
                        if (reflectiveMethod != null)
                            try {
                                reflectiveMethod.invoke(activity);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setTitle(title);
        if (showNoOption) {
            builder.setMessage(message).setPositiveButton("Ok", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        } else {
            builder.setMessage(message).setPositiveButton("Ok", dialogClickListener).show();
        }
    }

    public static void showMessage(String title, String message, final Activity activity, final Method reflectiveMethod,
                                   final Object[] parameters, boolean showNoOption)
            throws IllegalArgumentException, InvocationTargetException, IllegalAccessException {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pressed) {
                switch (pressed) {
                    case DialogInterface.BUTTON_POSITIVE:
                        if (reflectiveMethod != null)
                            try {
                                reflectiveMethod.invoke(activity, parameters);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setTitle(title);
        if (showNoOption) {
            builder.setMessage(message).setPositiveButton("Ok", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        } else {
            builder.setMessage(message).setPositiveButton("Ok", dialogClickListener).show();
        }
    }

    /**
     * @param message
     * @param activity
     * @param staticReflectiveMethod
     * @param invokingReference
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public static void showMessage(String title, String message, Activity activity, final Method staticReflectiveMethod,
                                   final Object invokingReference)
            throws IllegalArgumentException, InvocationTargetException, IllegalAccessException {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pressed) {
                switch (pressed) {
                    case DialogInterface.BUTTON_POSITIVE:
                        if (staticReflectiveMethod != null)
                            try {
                                staticReflectiveMethod.invoke(invokingReference);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message).setPositiveButton("Ok", dialogClickListener).show();
    }

    /**
     * @param message
     * @param activity
     * @param reflectiveMethod
     * @param invokingReference
     * @param methodParameters
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public static void showMessage(String title, String message, Activity activity, final Method reflectiveMethod,
                                   final Object invokingReference, final Object[] methodParameters)
            throws IllegalArgumentException, InvocationTargetException, IllegalAccessException {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pressed) {
                switch (pressed) {
                    case DialogInterface.BUTTON_POSITIVE:
                        if (reflectiveMethod != null && invokingReference != null)
                            try {
                                reflectiveMethod.invoke(invokingReference, methodParameters);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setMessage(message).setPositiveButton("Ok", dialogClickListener).show();
    }

}
