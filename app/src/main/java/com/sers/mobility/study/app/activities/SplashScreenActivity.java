package com.sers.mobility.study.app.activities;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.baseFiles.BaseMainFragment;
import com.sers.mobility.study.app.custom.NavigationController;
import com.sers.mobility.study.app.sessionManager.SessionManager;


public class SplashScreenActivity extends Activity implements BaseMainFragment.OnFragmentInteractionListener {

    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        super.setTitle("");
        session = new SessionManager(getApplicationContext());
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(4000);// sleep for 3secs
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {

                    if (session.checkLogin()) {
                        NavigationController.navigateToActivity(SplashScreenActivity.this, getApplicationContext(), MainActivity.class);
                    } else {
                        NavigationController.navigateToActivity(SplashScreenActivity.this, getApplicationContext(), LoginActivity.class);
                    }
                }
            }
        };
        timer.start();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
