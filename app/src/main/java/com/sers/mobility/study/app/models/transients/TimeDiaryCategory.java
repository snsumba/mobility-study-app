package com.sers.mobility.study.app.models.transients;


public class TimeDiaryCategory extends ServerUpdate {
    private String category;
    private String recordId;

    public TimeDiaryCategory() {
    }

    public TimeDiaryCategory(String category, String recordId) {
        this.category = category;
        this.recordId = recordId;
    }

    public TimeDiaryCategory(int pk, String userId, String responseStatus, String responseMessage, String category, String recordId) {
        super(pk, userId, responseStatus, responseMessage);
        this.category = category;
        this.recordId = recordId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }
}
