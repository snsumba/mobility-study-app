package com.sers.mobility.study.app.utils;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.view.View;
import android.widget.TextView;

import com.sers.mobility.study.app.constants.AppMessages;

public class LocationUtils {
    private Context context = null;

    public LocationUtils() {

    }

    public LocationUtils(Context context) {
        this.context = context;
    }

    public void statusCheck(View view, boolean enable, String message) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (enable) {
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessagGPSSettings(message);
            } else AppUtils.showSnackBarMessage(view, AppMessages.GPS_ALREADY_ON);
        } else {
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessagGPSSettings(message);
            } else AppUtils.showSnackBarMessage(view, AppMessages.GPS_ALREADY_OFF);
        }
    }

    public boolean isGPSEnabled() {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            return true;
        else return false;
    }

    private void buildAlertMessagGPSSettings(final String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


}
