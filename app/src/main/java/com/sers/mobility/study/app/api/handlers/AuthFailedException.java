package com.sers.mobility.study.app.api.handlers;

import retrofit.RetrofitError;

public class AuthFailedException extends Throwable {
    public String getDetail() {
        return detail;
    }

    private final String detail;
    private RetrofitError error;

    public AuthFailedException(String s, RetrofitError error) {
        this.detail = s;
        this.error = error;
    }



    public RetrofitError getError() {
        return error;
    }

    public void setError(RetrofitError error) {
        this.error = error;
    }
}
