package com.sers.mobility.study.app.baseFiles;


import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.internal.MDButton;
import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.activities.MainActivity;
import com.sers.mobility.study.app.api.webserviceImpl.WebService;
import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.constants.MethodType;
import com.sers.mobility.study.app.custom.NavigationController;
import com.sers.mobility.study.app.custom.SystemProgressDialog;
import com.sers.mobility.study.app.fragments.MainApplicationFragment;
import com.sers.mobility.study.app.models.transients.AppAccessDetails;
import com.sers.mobility.study.app.sessionManager.AppTypeManager;
import com.sers.mobility.study.app.utils.AppUtils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.HashMap;

public abstract class BaseReportFragment extends BaseMainFragment
        implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, DatePickerDialog.OnDateSetListener {

    public RecyclerView recyclerView;
    public SwipeRefreshLayout swipeRefreshLayout;
    public SystemProgressDialog systemProgressDialog;
    public int VIEW_CHANGER = 0;
    public LinearLayout searchLinearLayout;
    public EditText searchStartDate, searchEndDate, dateOfBirth;
    public Button searchButton;
    public MDButton confirmButton;
    public WebService webService;
    public AppAccessDetails appAccessDetails;
    public ActionBar action;
    public MenuItem mSearchAction;
    public boolean isSearchOpened = false;
    public EditText edtSeach;

    public BaseReportFragment(Activity activity) {
        super(activity);
    }

    public BaseReportFragment() {

    }

    public void loadContent() {
        this.getContent(false);
    }

    @Override
    public void onRefresh() {
        this.getContent(true);
    }

    public abstract void getContent(boolean isRefreshing);

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        if (VIEW_CHANGER == 1) {
            int monthday_start = monthOfYear + 1;
            if (searchStartDate != null)
                searchStartDate.setText(year + "/" + monthday_start + "/" + dayOfMonth + AppUtils.getCurrentTime());
        } else if (VIEW_CHANGER == 2) {
            int monthday_end = monthOfYear + 1;
            if (searchEndDate != null)
                searchEndDate.setText(year + "/" + monthday_end + "/" + dayOfMonth + AppUtils.getCurrentTime());
        } else if (VIEW_CHANGER == 3) {
            int monthday_end = monthOfYear + 1;
            if (dateOfBirth != null)
                dateOfBirth.setText(year + "/" + monthday_end + "/" + dayOfMonth + AppUtils.getCurrentTime());
        }
    }


    public void initializeDatesPickers(final DatePickerDialog.OnDateSetListener onDateSetListener) {
        if (searchStartDate != null)
            searchStartDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == 0) {
                        VIEW_CHANGER = 1;
                        showDatePicker(onDateSetListener);
                    }
                    return false;
                }
            });

        if (searchEndDate != null)
            searchEndDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == 0) {
                        VIEW_CHANGER = 2;
                        showDatePicker(onDateSetListener);
                    }
                    return false;
                }
            });
        if (dateOfBirth != null)
            dateOfBirth.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == 0) {
                        VIEW_CHANGER = 3;
                        showDatePicker(onDateSetListener);
                    }
                    return false;
                }
            });


    }

    public void showDatePicker(DatePickerDialog.OnDateSetListener onDateSetListener) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                onDateSetListener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getActivity().getFragmentManager(), "Select Date");
        dpd.setAccentColor(getResources().getColor(R.color.colorPrimary));
    }


    public void reloadDisplay(Activity activity) {
       NavigationController.navigateToActivity(activity, activity.getApplicationContext(), MainActivity.class);
    }
}
