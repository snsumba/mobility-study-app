package com.sers.mobility.study.app.models.singletons;


public class SelectedTimeDiaryOption {

    private static SelectedTimeDiaryOption instance = null;

    private SelectedTimeDiaryOption() {
    }

    public static SelectedTimeDiaryOption getInstance() {
        if (instance == null) {
            instance = new SelectedTimeDiaryOption();
        }
        return instance;
    }

    public CategoryOption categoryOption = new CategoryOption();
}
