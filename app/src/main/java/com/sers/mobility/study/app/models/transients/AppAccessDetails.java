package com.sers.mobility.study.app.models.transients;


public class AppAccessDetails {
    private String userId = "";
    private String password;

    public AppAccessDetails() {
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
