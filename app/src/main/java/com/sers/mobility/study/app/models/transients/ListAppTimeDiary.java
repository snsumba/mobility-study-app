package com.sers.mobility.study.app.models.transients;

import java.io.Serializable;
import java.util.List;

public class ListAppTimeDiary implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String userId;
    private List<TimeDiaryRequest> appTimeDiaries;
    //private String apiToken;

    public ListAppTimeDiary() {
    }

    public ListAppTimeDiary(String apiToken, String userId, List<TimeDiaryRequest> appTimeDiaries) {
        this.userId = userId;
        //this.apiToken = apiToken;
        this.appTimeDiaries = appTimeDiaries;
    }

//    public String getApiToken() {
//        return apiToken;
//    }
//
//    public void setApiToken(String apiToken) {
//        this.apiToken = apiToken;
//    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<TimeDiaryRequest> getAppTimeDiaries() {
        return appTimeDiaries;
    }

    public void setAppTimeDiaries(List<TimeDiaryRequest> appTimeDiaries) {
        this.appTimeDiaries = appTimeDiaries;
    }
}
