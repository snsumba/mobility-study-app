package com.sers.mobility.study.app.models.transients;

import java.io.Serializable;
import java.util.List;

public class ListServerCapturedLocation implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String userId;
    //private String apiToken;
    private List<ServerCapturedLocation> serverCapturedLocations;

    public ListServerCapturedLocation() {
    }

    public ListServerCapturedLocation(String apiToken, String userId, List<ServerCapturedLocation> serverCapturedLocations) {
        this.userId = userId;
        //this.apiToken = apiToken;
        this.serverCapturedLocations = serverCapturedLocations;
    }

//    public String getApiToken() {
//        return apiToken;
//    }
//
//    public void setApiToken(String apiToken) {
//        this.apiToken = apiToken;
//    }

    /**
     * @return the serverCapturedLocations
     */
    public List<ServerCapturedLocation> getServerCapturedLocations() {
        return serverCapturedLocations;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @param serverCapturedLocations the serverCapturedLocations to set
     */
    public void setServerCapturedLocations(List<ServerCapturedLocation> serverCapturedLocations) {
        this.serverCapturedLocations = serverCapturedLocations;
    }

}
