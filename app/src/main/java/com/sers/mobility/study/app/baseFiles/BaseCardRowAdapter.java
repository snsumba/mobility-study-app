package com.sers.mobility.study.app.baseFiles;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.custom.SystemProgressDialog;


public abstract class BaseCardRowAdapter extends RecyclerView.Adapter<BaseCardRowAdapter.ViewHolder> {
    public Context context;
    public ColorGenerator generator = ColorGenerator.MATERIAL;
    public int rowLayout;
    public SystemProgressDialog systemProgressDialog;
    public FragmentManager fragmentManager;
    public Activity activity;
    public View.OnClickListener onClickListener=null;

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbnail;
        TextView recordId, leftCardElement1, leftCardElement2, leftCardElement3, leftCardElement4, rightCardElement1;

        public ViewHolder(View view) {
            super(view);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            recordId = (TextView) view.findViewById(R.id.recordId);
            leftCardElement1 = (TextView) view.findViewById(R.id.leftCardElement1);
            leftCardElement2 = (TextView) view.findViewById(R.id.leftCardElement2);
            leftCardElement3 = (TextView) view.findViewById(R.id.leftCardElement3);
            leftCardElement4 = (TextView) view.findViewById(R.id.leftCardElement4);
            rightCardElement1 = (TextView) view.findViewById(R.id.rightCardElement1);

        }
    }

    public BaseCardRowAdapter(int rowLayout, Context context, SystemProgressDialog systemProgressDialog, FragmentManager fragmentManager,
                              Activity activity, View.OnClickListener onClickListener) {
        this.rowLayout = rowLayout;
        this.context = context;
        this.activity = activity;
        this.fragmentManager = fragmentManager;
        this.systemProgressDialog = systemProgressDialog;
        this.onClickListener=onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }

    public abstract void onBindViewHolder(ViewHolder holder, final int position);

    public abstract int getItemCount();
}

