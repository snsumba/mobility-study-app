package com.sers.mobility.study.app.activities;

import android.app.Activity;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.api.webserviceImpl.WebService;
import com.sers.mobility.study.app.baseFiles.BaseMainFragment;
import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.constants.AppMessages;
import com.sers.mobility.study.app.custom.AppMaterialDialog;
import com.sers.mobility.study.app.custom.NavigationController;
import com.sers.mobility.study.app.custom.SystemProgressDialog;
import com.sers.mobility.study.app.databaseUtils.CustomDatabaseCreator;
import com.sers.mobility.study.app.databaseUtils.QueryDatabaseUtil;
import com.sers.mobility.study.app.models.databases.RegisteredAccount;
import com.sers.mobility.study.app.models.transients.AppAccessDetails;
import com.sers.mobility.study.app.models.transients.ServerProfileUpdate;
import com.sers.mobility.study.app.sessionManager.AccessTokenManager;
import com.sers.mobility.study.app.sessionManager.AppDurationManager;
import com.sers.mobility.study.app.sessionManager.AppTypeManager;
import com.sers.mobility.study.app.sessionManager.DomainManager;
import com.sers.mobility.study.app.sessionManager.NotificationDurationManager;
import com.sers.mobility.study.app.sessionManager.SessionManager;
import com.sers.mobility.study.app.utils.AppUtils;

import java.util.HashMap;

public class LoginActivity extends Activity implements View.OnClickListener, BaseMainFragment.OnFragmentInteractionListener {

    private EditText txtPassword, txtUsername;
    private AppCompatButton btnSign;
    private SystemProgressDialog systemProgressDialog;
    private SessionManager sessionManager;
    private String isLoggedIn;
    private TextView appDomainLink, registrationLink;
    private String applicationDomain, methodType, durationInSeconds;
    private AppAccessDetails appAccessDetails = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_login);
        txtPassword = (EditText) findViewById(R.id.password);
        txtUsername = (EditText) findViewById(R.id.username);
        btnSign = (AppCompatButton) findViewById(R.id.login);
        btnSign.setOnClickListener(this);

        systemProgressDialog = new SystemProgressDialog(this);

        new CustomDatabaseCreator(getApplicationContext());
        initilizeApplicationSessionInfo();

        appDomainLink = (TextView) findViewById(R.id.app_domain_link);
        appDomainLink.setOnClickListener(this);
        appDomainLink.setOnHoverListener(new View.OnHoverListener() {
            @Override
            public boolean onHover(View v, MotionEvent event) {
                appDomainLink.setPaintFlags(appDomainLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                return false;
            }
        });

        registrationLink = (TextView) findViewById(R.id.register_account_link);
        registrationLink.setOnClickListener(this);
        //registrationLink.setOnHoverListener(this);


        if (sessionManager != null) {
            if (sessionManager.checkLogin()) {
                NavigationController.navigateToActivity(LoginActivity.this, getApplicationContext(), MainActivity.class);
            }
        }

    }

    private void initilizeApplicationSessionInfo() {
        sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        isLoggedIn = user.get(SessionManager.KEY_IS_LOGGED_IN);

        DomainManager domainManager = new DomainManager(getApplicationContext());
        HashMap<String, String> domain = domainManager.getApplicationDomain();
        applicationDomain = domain.get(DomainManager.KEY_APPLICATION_DOMAIN);

        if (applicationDomain == null) {
            domainManager.clearApplicationDomain();
            domainManager.createAppDomain();
        }

        AppTypeManager appTypeManager = new AppTypeManager(LoginActivity.this);
        HashMap<String, String> appType = appTypeManager.getAppTypeSettings();
        methodType = appType.get(AppTypeManager.KEY_DESIRED_METHOD);
        if (methodType == null) {
            appTypeManager.clearAppTypeManager();
            appTypeManager.createAppPreference();
        }

        AppDurationManager appDurationManager = new AppDurationManager(LoginActivity.this);
        HashMap<String, String> appDuration = appDurationManager.getAppDurationSettings();
        durationInSeconds = appDuration.get(AppDurationManager.KEY_DURATION_IN_SECONDS);
        if (durationInSeconds == null) {
            appDurationManager.clearAppDurationManager();
            appDurationManager.createAppPreference();
        }

        AccessTokenManager accessTokenManager = new AccessTokenManager(LoginActivity.this);
        String accessToken = accessTokenManager.getAccessToken();
        if (accessToken == null) {
            accessTokenManager.clearAccessTokenManager();
            accessTokenManager.createAppPreference();
        }

        NotificationDurationManager notificationDurationManager = new NotificationDurationManager(LoginActivity.this);
        HashMap<String, String> notification = notificationDurationManager.getAppDurationSettings();
        String duration = notification.get(NotificationDurationManager.KEY_DURATION_IN_MINUTES);
        if (duration == null) {
            notificationDurationManager.clearAppDurationManager();
            notificationDurationManager.createAppPreference();
        }
    }

    private void applicationDomainDialog() {
        final MaterialDialog applicationDomainDialog = new AppMaterialDialog().appMaterialDialog(R.layout.popup_app_domain,
                LoginActivity.this, AppConstants.APPLICATION_DOMAIN, R.string.reset_text, R.string.negtive_dialog_text,
                R.string.save_text, Boolean.FALSE);
        View dialogView = applicationDomainDialog.getView();
        final EditText appDomainView = (EditText) dialogView.findViewById(R.id.app_domain);

        final DomainManager appDomainManager = new DomainManager(getApplicationContext());
        HashMap<String, String> domain = appDomainManager.getApplicationDomain();
        applicationDomain = domain.get(DomainManager.KEY_APPLICATION_DOMAIN);
        appDomainView.setText(applicationDomain);

        final MDButton save = (MDButton) applicationDomainDialog.getActionButton(DialogAction.POSITIVE);
        applicationDomainDialog.show();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (String.valueOf(appDomainView.getText()).length() > 14) {
                    appDomainManager.clearApplicationDomain();
                    appDomainManager.editAppDomain(String.valueOf(appDomainView.getText()));
                    applicationDomainDialog.dismiss();
                } else {
                    applicationDomainDialog.dismiss();
                    AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Invalid domain name, Please contact " + AppConstants.DOMAIN_CONTACT + " for the right domain", LoginActivity.this);
                }
            }
        });

        final MDButton reset = (MDButton) applicationDomainDialog.getActionButton(DialogAction.NEUTRAL);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appDomainManager.clearApplicationDomain();
                appDomainManager.createAppDomain();
                applicationDomainDialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.login:
                if (isLoggedIn == "true") {
                    NavigationController.navigateToActivity(this, getApplicationContext(), MainActivity.class);
                    AppUtils.showToastMessage(getApplicationContext(), "Successful Login");
                } else {

                    if (String.valueOf(txtUsername.getText()).equals("pass") && String.valueOf(txtPassword.getText()).equals("pass")) {
                        sessionManager.clearSessionManager();
                        sessionManager.createLoginSession(
                                new ServerProfileUpdate(new RegisteredAccount().pk, new RegisteredAccount().userId, AppMessages.SUCCESS, AppMessages.SUCCESSFUL_LOGIN,
                                        new RegisteredAccount().firstName, new RegisteredAccount().lastName, new RegisteredAccount().location, new RegisteredAccount().gender,
                                        new RegisteredAccount().username, new RegisteredAccount().password, String.valueOf(new RegisteredAccount().dateOfBirth),
                                        new RegisteredAccount().phoneNumber, String.valueOf(true), String.valueOf(AppConstants.DEFAULT_DISTANCE_APART_IN_KILOMETRES)), "");
                        NavigationController.navigateToActivity(this, getApplicationContext(), MainActivity.class);
                        systemProgressDialog.closeProgressDialog();
                    } else if (String.valueOf(txtUsername.getText()).isEmpty() || String.valueOf(txtPassword.getText()).isEmpty()) {
                        AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Invalid username or Password", this);
                    } else {
                        appAccessDetails = new AppAccessDetails();
                        appAccessDetails.setUserId(AppUtils.encryptData(String.valueOf(txtUsername.getText())));
                        appAccessDetails.setPassword(AppUtils.encryptData(String.valueOf(txtPassword.getText())));
                        if (new AppUtils().isNetworkAvailable(this)) {
                            new WebService(getApplicationContext(), systemProgressDialog, LoginActivity.this, appAccessDetails,
                                    null, null, null).getAppLoginService();
                        } else {
                            RegisteredAccount registeredAccount = new QueryDatabaseUtil().getRegisteredAccountFromDatabaseByUsernameAndPassword(String.valueOf(txtUsername.getText()), String.valueOf(txtPassword.getText()));
                            if (registeredAccount != null) {
                                NavigationController.navigateToActivity(LoginActivity.this, getApplicationContext(), MainActivity.class);
                                SessionManager sessionManager = new SessionManager(getApplicationContext());
                                sessionManager.clearSessionManager();
                                sessionManager.createLoginSession(
                                        new ServerProfileUpdate(registeredAccount.pk, registeredAccount.userId, AppMessages.SUCCESS, AppMessages.SUCCESSFUL_LOGIN,
                                                registeredAccount.firstName, registeredAccount.lastName, registeredAccount.location, registeredAccount.gender,
                                                registeredAccount.username, registeredAccount.password, String.valueOf(registeredAccount.dateOfBirth),
                                                registeredAccount.phoneNumber, String.valueOf(true), String.valueOf(AppConstants.DEFAULT_DISTANCE_APART_IN_KILOMETRES)), String.valueOf(txtPassword.getText()));
                                AppUtils.showToastMessage(getApplicationContext(), "Successful Login");
                            } else
                                AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Invalid username or Password", this);
                        }
                    }
                }
                break;
            case R.id.app_domain_link:
                applicationDomainDialog();
                break;

            case R.id.register_account_link:
                NavigationController.navigateToActivity(LoginActivity.this, getApplicationContext(), RegisterAccountActivity.class);
                break;

        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * Is called when the user receives an event like a call or a text message,
     * when onPause() is called the Activity may be partially or completely hidden.
     * You would want to save user data in onPause, in case he hits back button
     * without saving the data explicitly
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Is called when the user resumes his Activity which he left a while ago,
     * say he presses home button and then comes back to app, onResume() is called.
     * You can do the network related updates here or anything of this sort in onResume.
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Is called when the Activity is being destroyed either by the system, or by the user,
     * say by hitting back, until the app exits.
     * Its compulsory that you save any user data that you want to persist in onDestroy(),
     * because the system will not do it for you.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
