package com.sers.mobility.study.app.api.handlers;

public class ResourceNotFoundException extends Throwable {
    private String content;

    public String getContent() {
        return content;
    }

    public ResourceNotFoundException(String s) {
        this.content = s;
    }
}
