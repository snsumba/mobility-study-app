package com.sers.mobility.study.app.custom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class NavigationController {

    public static void navigateToActivity(Activity currentActivity, Context fromContext, Class<?> toActivity) {
        Intent intent = new Intent(fromContext, toActivity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        fromContext.startActivity(intent);
        currentActivity.finish();
    }
}
