package com.sers.mobility.study.app.fragments.mainMenu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.melnykov.fab.FloatingActionButton;
import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.adapters.listAdapters.DisplayUserLocationCardAdapter;
import com.sers.mobility.study.app.api.webserviceImpl.WebService;
import com.sers.mobility.study.app.baseFiles.BaseReportFragment;
import com.sers.mobility.study.app.constants.AppMessages;
import com.sers.mobility.study.app.constants.MethodType;
import com.sers.mobility.study.app.constants.RecordStatus;
import com.sers.mobility.study.app.custom.SystemProgressDialog;
import com.sers.mobility.study.app.databaseUtils.QueryDatabaseUtil;
import com.sers.mobility.study.app.databaseUtils.UpdateDatabaseUtil;
import com.sers.mobility.study.app.models.databases.CapturedLocation;
import com.sers.mobility.study.app.models.transients.ListServerCapturedLocation;
import com.sers.mobility.study.app.models.transients.ServerCapturedLocation;
import com.sers.mobility.study.app.sessionManager.AccessTokenManager;
import com.sers.mobility.study.app.sessionManager.SessionManager;
import com.sers.mobility.study.app.utils.AppUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class PlacesVisitedFragment extends BaseReportFragment {

    private List<CapturedLocation> capturedLocations = new ArrayList<CapturedLocation>();
    private AppCompatButton syncLocationsButton;

    public PlacesVisitedFragment() {
        super();
    }

    @SuppressLint("ValidFragment")
    public PlacesVisitedFragment(Activity activity) {
        super(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_places_visited, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.listRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplication().getApplicationContext()));
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.listSwipeView);
        swipeRefreshLayout.setOnRefreshListener(this);
        systemProgressDialog = new SystemProgressDialog(getActivity());

        syncLocationsButton = (AppCompatButton) rootView.findViewById(R.id.syncLocations);
        syncLocationsButton.setOnClickListener(this);

        sessionManager = new SessionManager(activity);
        if (sessionManager != null) {
            HashMap<String, String> user = sessionManager.getUserDetails();
            userId = (user.get(SessionManager.KEY_USER_ID) != null ? user.get(SessionManager.KEY_USER_ID) : "n.a");
        }

        loadContent();

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerView.setAdapter(new DisplayUserLocationCardAdapter(R.layout.card_row_layout, systemProgressDialog,
                        getActivity().getFragmentManager(), getActivity(),
                        new QueryDatabaseUtil().getListOfCapturedLocationsFromDatabase(userId), this));
            }
        });

        return rootView;
    }

    @Override
    public void getContent(boolean isRefreshing) {
        try {
            if (isRefreshing) {
                swipeRefreshLayout.setRefreshing(false);
                List<CapturedLocation> db = new QueryDatabaseUtil().getListOfCapturedLocationsFromDatabase(userId);
                if (db != null) {
                    if (db.size() > 0) {
                        systemProgressDialog.showProgressDialog("Synchronizing Data, Please Wait....");
                        capturedLocations = db;
                        recyclerView.setAdapter(new DisplayUserLocationCardAdapter(R.layout.card_row_layout, systemProgressDialog, getActivity().getFragmentManager(), getActivity(), capturedLocations, this));
                        systemProgressDialog.closeProgressDialog();
                    }
                }
            } else {
                List<CapturedLocation> db = new QueryDatabaseUtil().getListOfCapturedLocationsFromDatabase(userId);
                if (db != null) {
                    if (db.size() > 0) {
                        systemProgressDialog.showProgressDialog("Synchronizing Data, Please Wait....");
                        capturedLocations = db;
                        recyclerView.setAdapter(new DisplayUserLocationCardAdapter(R.layout.card_row_layout, systemProgressDialog, getActivity().getFragmentManager(), getActivity(), capturedLocations, this));
                        systemProgressDialog.closeProgressDialog();
                    }
                } else this.getContent(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.syncLocations:
                syncPlacesVisited();
                break;
        }
    }

    private void syncPlacesVisited() {
        try {
            if (new AppUtils().isNetworkAvailable(getActivity())) {
                systemProgressDialog.showProgressDialog("Synchronizing Captured Locations Data, Please Wait....");
                List<CapturedLocation> unSyncedLocations = new QueryDatabaseUtil().getListOfCapturedLocationsFromDatabase(userId, RecordStatus.UN_SYNCED.getStatus());
                if (unSyncedLocations != null) {
                    if (unSyncedLocations.size() > 0) {
                        List<ServerCapturedLocation> serverCapturedLocations = new ArrayList<ServerCapturedLocation>();
                        for (CapturedLocation capturedLocation : unSyncedLocations) {
                            if (capturedLocation != null) {
                                ServerCapturedLocation serverCapturedLocation = new ServerCapturedLocation();
                                serverCapturedLocation.setMethodType(AppUtils.encryptData((capturedLocation.getMethodType() != null ? capturedLocation.getMethodType() : MethodType.GPS_DETERMINED.getName())));
                                serverCapturedLocation.setUserId(AppUtils.encryptData(userId));
                                serverCapturedLocation.setRecordId(AppUtils.encryptData(capturedLocation.getRecordId()));
                                serverCapturedLocation.setStatus(AppUtils.encryptData(capturedLocation.getStatus()));
                                serverCapturedLocation.setPlaceName(AppUtils.encryptData(capturedLocation.getPlaceName()));
                                serverCapturedLocation.setLatitude(AppUtils.encryptData(capturedLocation.getLatitude()));
                                serverCapturedLocation.setLongitude(AppUtils.encryptData(capturedLocation.getLongitude()));
                                serverCapturedLocation.setAccuracy(AppUtils.encryptData(capturedLocation.getAccuracy()));
                                serverCapturedLocation.setDate(AppUtils.encryptData(capturedLocation.date));
                                serverCapturedLocations.add(serverCapturedLocation);
                                System.out.println("***** >>> RECORD_ID 1 : " + capturedLocation.getUserId()+" ---MM");
                                System.out.println("***** >>> RECORD_ID 2 : " + capturedLocation.getRecordId());
                                System.out.println("***** >>> RECORD_ID 3 : " + AppUtils.decryptData(serverCapturedLocation.getRecordId())+" ---YY");
                            }
                        }
                        System.out.println("***** >>> RECORD_ID 4 : " + userId);
                        System.out.println("***** >>> RECORD_ID 5 : " + userId);

                        String accessToken = new AccessTokenManager(getActivity()).getAccessToken();
                        ListServerCapturedLocation listServerCapturedLocation = new ListServerCapturedLocation(AppUtils.encryptData(accessToken),AppUtils.encryptData(userId), serverCapturedLocations);
                        new WebService(systemProgressDialog, getActivity(), null, listServerCapturedLocation).saveAppListOfCapturedLocationsService();

                        recyclerView.setAdapter(new DisplayUserLocationCardAdapter(R.layout.card_row_layout, systemProgressDialog,
                                getActivity().getFragmentManager(), getActivity(),
                                new QueryDatabaseUtil().getListOfCapturedLocationsFromDatabase(userId), this));
                    } else {
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessageWithOutButton(true, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE, "No captured locations to Sync", getActivity());
                    }
                }
            } else {
                new AppUtils().noInternet(getActivity(), "Location Data Sync");
            }
            new UpdateDatabaseUtil().clearSyncedCapturedLocations();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void syncPlacesVisitedBackup() {
        try {
            if (new AppUtils().isNetworkAvailable(getActivity())) {
                systemProgressDialog.showProgressDialog("Synchronizing Captured Locations Data, Please Wait....");
                List<CapturedLocation> unSyncedLocations = new QueryDatabaseUtil().getListOfCapturedLocationsFromDatabase(userId, RecordStatus.UN_SYNCED.getStatus());
                if (unSyncedLocations != null) {
                    if (unSyncedLocations.size() > 0) {
                        for (CapturedLocation capturedLocation : unSyncedLocations) {
                            if (capturedLocation != null) {
                                ServerCapturedLocation serverCapturedLocation = new ServerCapturedLocation();
                                serverCapturedLocation.setMethodType((capturedLocation.getMethodType() != null ? capturedLocation.getMethodType() : MethodType.GPS_DETERMINED.getName()));
                                serverCapturedLocation.setUserId(userId);
                                serverCapturedLocation.setRecordId(capturedLocation.getRecordId());
                                serverCapturedLocation.setStatus(capturedLocation.getStatus());
                                serverCapturedLocation.setPlaceName(capturedLocation.getPlaceName());
                                serverCapturedLocation.setLatitude(capturedLocation.getLatitude());
                                serverCapturedLocation.setLongitude(capturedLocation.getLongitude());
                                serverCapturedLocation.setAccuracy(capturedLocation.getAccuracy());
                                serverCapturedLocation.setDate(capturedLocation.date);
                                new WebService(getActivity(), systemProgressDialog, getActivity(), null, null, null, serverCapturedLocation).getAppCapturedLocationService();
                            }
                        }
                        recyclerView.setAdapter(new DisplayUserLocationCardAdapter(R.layout.card_row_layout, systemProgressDialog,
                                getActivity().getFragmentManager(), getActivity(),
                                new QueryDatabaseUtil().getListOfCapturedLocationsFromDatabase(userId), this));
                    } else {
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessageWithOutButton(true, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE, "No captured locations to Sync", getActivity());
                    }
                }
            } else {
                new AppUtils().noInternet(getActivity(), "Location Data Sync");
            }
            new UpdateDatabaseUtil().clearSyncedCapturedLocations();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
