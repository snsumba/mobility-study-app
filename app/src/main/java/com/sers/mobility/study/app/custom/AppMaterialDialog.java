package com.sers.mobility.study.app.custom;

import android.app.Activity;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.sers.mobility.study.app.R;

public class AppMaterialDialog {

    public MaterialDialog appMaterialDialog(int dialogLayout, Activity activity, String title, int neutralText, int negativeText, int positiveText, boolean cancelable) {
        final MaterialDialog dialog = new MaterialDialog.Builder(activity)
                .title(title)
                .customView(dialogLayout, Boolean.TRUE)
                .backgroundColorRes(R.color.list_item_title)
                .positiveColorRes(R.color.colorPrimary)
                .negativeColorRes(R.color.colorPrimary)
                .neutralColorRes(R.color.colorPrimary)
                .autoDismiss(false)
                .cancelable(cancelable)
                .neutralText(neutralText)
                .negativeText(negativeText)
                .positiveText(positiveText)
                .build();

        MDButton decline = (MDButton) dialog.getActionButton(DialogAction.NEGATIVE);
        decline.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }
}
