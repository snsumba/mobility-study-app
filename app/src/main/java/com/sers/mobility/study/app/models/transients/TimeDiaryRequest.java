package com.sers.mobility.study.app.models.transients;


import java.io.Serializable;

public class TimeDiaryRequest implements Serializable {
    private String date;
    private String userId;
    private String recordId;
    private String timeCategorySuggestionId;
    private String startTime;
    private String endTime;

    public TimeDiaryRequest() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getTimeCategorySuggestionId() {
        return timeCategorySuggestionId;
    }

    public void setTimeCategorySuggestionId(String timeCategorySuggestionId) {
        this.timeCategorySuggestionId = timeCategorySuggestionId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
