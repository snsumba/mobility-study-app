package com.sers.mobility.study.app.models.transients;


public class TimeDiaryCategorySuggestion {
    private String suggestion;
    private String suggestionId;

    public TimeDiaryCategorySuggestion() {
    }

    public TimeDiaryCategorySuggestion(String suggestion, String suggestionId) {
        this.suggestion = suggestion;
        this.suggestionId = suggestionId;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public String getSuggestionId() {
        return suggestionId;
    }

    public void setSuggestionId(String suggestionId) {
        this.suggestionId = suggestionId;
    }
}
