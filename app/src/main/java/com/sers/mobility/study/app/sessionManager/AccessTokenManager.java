package com.sers.mobility.study.app.sessionManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.constants.MethodType;

import java.util.HashMap;


public class AccessTokenManager {
    // Shared Preferences
    public SharedPreferences pref;
    // Editor for Shared preferences
    Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREF_NAME = "AccessTokenManager";

    /**
     * make variables public to access from outside
     */
    public static final String KEY_ACCESS_TOKEN = "desiredMethod";

    public AccessTokenManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createAppPreference() {
        editor.putString(KEY_ACCESS_TOKEN, "");
        editor.commit();
    }


    public void editAccessToken(String accessToken) {
        if (accessToken != null) {
            editor.putString(KEY_ACCESS_TOKEN, accessToken);
            editor.commit();
        }
    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getAccessTokenSettings() {
        HashMap<String, String> domain = new HashMap<String, String>();
        //  Put application domain details in the hash map
        domain.put(KEY_ACCESS_TOKEN, pref.getString(KEY_ACCESS_TOKEN, null));
        // return user
        return domain;
    }

    public String getAccessToken() {
        return pref.getString(KEY_ACCESS_TOKEN, null);
    }

    /**
     * Clear session details
     */
    public boolean clearAccessTokenManager() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        return true;

    }


}