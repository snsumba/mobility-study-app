package com.sers.mobility.study.app.models.transients;

public class AppTimeDiary extends ServerUpdate {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    public String diaryCategory = "n.a";
    public String selectedSuggestion;
    public String otherInfo;
    public String timeCategorySuggestionId;

    public String startTime;
    public String endTime;
    public String description = "n.a";
    public String recordId;
    public String date;
    public String status;

    /**
     *
     */
    public AppTimeDiary() {
        super();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getDiaryCategory() {
        return diaryCategory;
    }

    public void setDiaryCategory(String diaryCategory) {
        this.diaryCategory = diaryCategory;
    }

    public String getSelectedSuggestion() {
        return selectedSuggestion;
    }

    public void setSelectedSuggestion(String selectedSuggestion) {
        this.selectedSuggestion = selectedSuggestion;
    }

    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    public String getTimeCategorySuggestionId() {
        return timeCategorySuggestionId;
    }

    public void setTimeCategorySuggestionId(String timeCategorySuggestionId) {
        this.timeCategorySuggestionId = timeCategorySuggestionId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
