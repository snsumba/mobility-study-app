package com.sers.mobility.study.app.constants;

public enum RecordStatus {
    SYNCED("Synced"),
    UN_SYNCED("UnSynced");

    /**
     * Constructor with initial specified value
     *
     * @param status
     */
    RecordStatus(String status) {
        this.status = status;
    }

    private String status;

    /**
     * gets the title of the enumerated value
     *
     * @return
     */
    public String getStatus() {
        return this.status;
    }

    @Override
    public String toString() {
        return this.status;
    }
};