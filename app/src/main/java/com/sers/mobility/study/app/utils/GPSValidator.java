package com.sers.mobility.study.app.utils;

import android.content.Context;

import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.gpsServices.GPSTracker;

/**
 * Created by kdeo on 10/27/2016.
 */
public class GPSValidator {

    static final double RADIUS_OF_EARTH_IN_KM = 6378;


    public static boolean currentCoodinateShouldBeSaved(GPSTracker gpsLocation, double savedLatitude, double savedLongitude) {
        if (savedLatitude == 0 && savedLongitude == 0)
            return true;
        double radiusInKilometres = AppConstants.DEFAULT_DISTANCE_APART_IN_KILOMETRES / 1000;
        double maximumLongitude = savedLongitude + (radiusInKilometres / RADIUS_OF_EARTH_IN_KM) * (180 / Math.PI) / Math.cos(savedLatitude * Math.PI / 180);
        double maximumLatitude = savedLatitude + (radiusInKilometres / RADIUS_OF_EARTH_IN_KM) * (180 / Math.PI);
        double minimumLongitude = savedLongitude + (-radiusInKilometres / RADIUS_OF_EARTH_IN_KM) * (180 / Math.PI) / Math.cos(savedLatitude * Math.PI / 180);
        double minimumLatitude = savedLatitude + (-radiusInKilometres / RADIUS_OF_EARTH_IN_KM) * (180 / Math.PI);

        double currentLongitude = 0, currentLatitude = 0;
        if (gpsLocation != null) {
            if (gpsLocation.canGetLocation()) {
                currentLongitude = gpsLocation.getLongitude();
                currentLatitude = gpsLocation.getLatitude();
                //Determine whether current coordinate fits with in the boundaries of the saved coordinate
                if ((currentLongitude >= minimumLongitude && currentLongitude <= maximumLongitude)
                        && (currentLatitude >= minimumLatitude && currentLatitude <= maximumLatitude)) {
                    return false;
                }
            }
        }

        System.out.println("lookUp radiusInKilometres  : " + radiusInKilometres);
        System.out.println("lookUp maximumLongitude    : " + maximumLongitude);
        System.out.println("lookUp maximumLatitude     : " + maximumLatitude);
        System.out.println("lookUp minimumLongitude    : " + minimumLongitude);
        System.out.println("lookUp minimumLatitude     : " + minimumLatitude);

        System.out.println("lookUp currentLongitude     : " + currentLongitude);
        System.out.println("lookUp currentLatitude     : " + currentLatitude);


        return true;
    }


}
