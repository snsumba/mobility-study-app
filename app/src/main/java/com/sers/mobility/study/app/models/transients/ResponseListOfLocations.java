package com.sers.mobility.study.app.models.transients;


import java.io.Serializable;
import java.util.List;

public class ResponseListOfLocations implements Serializable {
    private String responseStatus;
    private String responseMessage;
    private List<ReturnedLocation> failedRecords;
    private List<ReturnedLocation> successfulRecords;

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public List<ReturnedLocation> getFailedRecords() {
        return failedRecords;
    }

    public void setFailedRecords(List<ReturnedLocation> failedRecords) {
        this.failedRecords = failedRecords;
    }

    public List<ReturnedLocation> getSuccessfulRecords() {
        return successfulRecords;
    }

    public void setSuccessfulRecords(List<ReturnedLocation> successfulRecords) {
        this.successfulRecords = successfulRecords;
    }
}
