package com.sers.mobility.study.app.models.transients;


import java.io.Serializable;
import java.util.List;

public class ResponseListOfTimeDiaries implements Serializable {
    private String responseStatus;
    private String responseMessage;
    private List<ReturnedTimeDiary> failedRecords;
    private List<ReturnedTimeDiary> successfulRecords;

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public List<ReturnedTimeDiary> getFailedRecords() {
        return failedRecords;
    }

    public void setFailedRecords(List<ReturnedTimeDiary> failedRecords) {
        this.failedRecords = failedRecords;
    }

    public List<ReturnedTimeDiary> getSuccessfulRecords() {
        return successfulRecords;
    }

    public void setSuccessfulRecords(List<ReturnedTimeDiary> successfulRecords) {
        this.successfulRecords = successfulRecords;
    }
}
