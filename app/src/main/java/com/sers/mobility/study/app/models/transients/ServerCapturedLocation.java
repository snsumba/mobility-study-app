package com.sers.mobility.study.app.models.transients;


public class ServerCapturedLocation extends ServerUpdate {
    private String recordId;
    private String status;
    private String date;
    private String placeName;
    private String longitude;
    private String latitude;
    private String accuracy;
    private String methodType;

    public ServerCapturedLocation() {
    }

    public ServerCapturedLocation(String recordId, String status, String date, String placeName,
                                  String longitude, String latitude, String accuracy) {
        super();
        this.recordId = recordId;
        this.status = status;
        this.date = date;
        this.placeName = placeName;
        this.longitude = longitude;
        this.latitude = latitude;
        this.accuracy = accuracy;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getMethodType() {
        return methodType;
    }

    public void setMethodType(String methodType) {
        this.methodType = methodType;
    }
}
