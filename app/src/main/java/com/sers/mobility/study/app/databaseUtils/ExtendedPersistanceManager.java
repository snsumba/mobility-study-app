package com.sers.mobility.study.app.databaseUtils;


import com.sers.mobility.study.app.constants.RecordStatus;

import org.sers.kpa.datasource.annotations.Table;
import org.sers.kpa.datasource.datamanager.CustomPersistanceManager;

public class ExtendedPersistanceManager<T> extends CustomPersistanceManager<T> {

    public ExtendedPersistanceManager(Class<T> klass) {
        super(klass);
    }

    public ExtendedPersistanceManager clearSyncedRecords() throws Exception {
        if (!this.isADatabaseModel()) {
            throw new Exception("Specified Object is not a database model");
        } else {
            this.open();
            this.database.execSQL("DELETE FROM " + ((Table) super.klass.getAnnotation(Table.class)).name() + " WHERE status = \"" + RecordStatus.SYNCED.getStatus() + "\"");
            this.close();
            return this;
        }
    }
}
