package com.sers.mobility.study.app.fragments.optionsMenu;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;

import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.api.webserviceImpl.WebService;
import com.sers.mobility.study.app.baseFiles.BaseReportFragment;
import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.constants.AppMessages;
import com.sers.mobility.study.app.constants.RecordStatus;
import com.sers.mobility.study.app.custom.SystemProgressDialog;
import com.sers.mobility.study.app.databaseUtils.QueryDatabaseUtil;
import com.sers.mobility.study.app.models.databases.RegisteredAccount;
import com.sers.mobility.study.app.models.transients.ServerRegisteredAccount;
import com.sers.mobility.study.app.sessionManager.SessionManager;
import com.sers.mobility.study.app.utils.AppUtils;

import java.util.Date;


public class RegisterAccountFragment extends BaseReportFragment {

    private EditText firstNameField, lastNameField, phoneNumberField, usernameField, passwordField, confirmPasswordField;
    private AppCompatButton accountButton = null;
    private RadioButton genderMale, genderFemale;
    private String selectedGender = AppConstants.UNKNOWN;

    public RegisterAccountFragment() {

    }

    @SuppressLint("ValidFragment")
    public RegisterAccountFragment(Activity activity) {
        super(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register_account, container, false);

        firstNameField = (EditText) rootView.findViewById(R.id.firstNameField);
        lastNameField = (EditText) rootView.findViewById(R.id.lastNameField);
        phoneNumberField = (EditText) rootView.findViewById(R.id.phoneNumberField);
        usernameField = (EditText) rootView.findViewById(R.id.usernameField);

        dateOfBirth = (EditText) rootView.findViewById(R.id.dateOfBirth);
        initializeDatesPickers(RegisterAccountFragment.this);

        passwordField = (EditText) rootView.findViewById(R.id.passwordField);
        confirmPasswordField = (EditText) rootView.findViewById(R.id.confirmPasswordField);
        genderFemale = (RadioButton) rootView.findViewById(R.id.genderFemale);
        genderMale = (RadioButton) rootView.findViewById(R.id.genderMale);
        accountButton = (AppCompatButton) rootView.findViewById(R.id.accountButton);
        accountButton.setOnClickListener(this);

        sessionManager = new SessionManager(activity);
        systemProgressDialog = new SystemProgressDialog(activity);

        genderFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = AppConstants.FEMALE;
            }
        });

        genderMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = AppConstants.MALE;
            }
        });


        return rootView;
    }

    @Override
    public void getContent(boolean isRefreshing) {

    }

    private void clearField() {
        if (firstNameField != null)
            firstNameField.setText("");
        if (lastNameField != null)
            lastNameField.setText("");
        if (phoneNumberField != null)
            phoneNumberField.setText("");
        if (usernameField != null)
            usernameField.setText("");
        if (dateOfBirth != null)
            dateOfBirth.setText("");
        if (passwordField != null)
            passwordField.setText("");
        if (confirmPasswordField != null)
            confirmPasswordField.setText("");
    }

    @Override
    public void onClick(View view) {
        if (view == accountButton) {
            if (String.valueOf(firstNameField.getText()).isEmpty() || String.valueOf(firstNameField.getText()) == null) {
                AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide a first name", getActivity());
            } else if (String.valueOf(lastNameField.getText()).isEmpty() || String.valueOf(lastNameField.getText()) == null) {
                AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide a last name", getActivity());
            } else if (String.valueOf(usernameField.getText()).isEmpty() || String.valueOf(usernameField.getText()) == null) {
                AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide a user name", getActivity());
            } else if (String.valueOf(passwordField.getText()).isEmpty() || String.valueOf(passwordField.getText()) == null) {
                AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide a password", getActivity());
            } else if (String.valueOf(confirmPasswordField.getText()).isEmpty() || String.valueOf(confirmPasswordField.getText()) == null) {
                AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Please confirm your password", getActivity());
            } else {
                if (String.valueOf(passwordField.getText()).equals(String.valueOf(confirmPasswordField.getText()))) {

                    ServerRegisteredAccount serverRegisteredAccount = new ServerRegisteredAccount(0,
                            AppUtils.encryptData(AppUtils.generateUserId()), null, null,
                            null, AppUtils.encryptData(RecordStatus.UN_SYNCED.getStatus()),
                            new Date(AppUtils.getCurrentDate()), AppUtils.encryptData(String.valueOf(firstNameField.getText())),
                            AppUtils.encryptData(String.valueOf(lastNameField.getText())),
                            AppUtils.encryptData(selectedGender),
                            null, AppUtils.encryptData(String.valueOf(phoneNumberField.getText())),
                            AppUtils.encryptData(String.valueOf(usernameField.getText())),
                            AppUtils.encryptData(String.valueOf(passwordField.getText())),
                            AppUtils.encryptData(java.lang.String.valueOf(dateOfBirth.getText())));

                    if (new AppUtils().isNetworkAvailable(activity)) {
                        new WebService(getActivity(), systemProgressDialog, getActivity(), null, null, serverRegisteredAccount, null).registraterAccountService();
                        clearField();
                    } else {
                        new AppUtils().noInternet(activity, "Account Registration");
                    }
                } else
                    AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Passwords don't match \nPlease make sure the provided passwords are the same", getActivity());
            }
        }
    }
}
