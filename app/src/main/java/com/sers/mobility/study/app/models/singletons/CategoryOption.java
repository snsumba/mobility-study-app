package com.sers.mobility.study.app.models.singletons;


public class CategoryOption {
    private String selectedOption;

    public CategoryOption() {
    }

    public CategoryOption(String selectedOption) {
        this.selectedOption = selectedOption;
    }

    public String getSelectedOption() {
        return selectedOption;
    }

    public void setSelectedOption(String selectedOption) {
        this.selectedOption = selectedOption;
    }
}
