package com.sers.mobility.study.app.activities;

import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.api.webserviceImpl.WebService;
import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.constants.AppMessages;
import com.sers.mobility.study.app.constants.AppToolBarTitles;
import com.sers.mobility.study.app.constants.EnableGpsTrackerStatus;
import com.sers.mobility.study.app.constants.MethodType;
import com.sers.mobility.study.app.constants.RecordStatus;
import com.sers.mobility.study.app.custom.NavigationController;
import com.sers.mobility.study.app.custom.SystemProgressDialog;
import com.sers.mobility.study.app.databaseUtils.QueryDatabaseUtil;
import com.sers.mobility.study.app.databaseUtils.UpdateDatabaseUtil;
import com.sers.mobility.study.app.gpsServices.GPSTracker;
import com.sers.mobility.study.app.models.databases.TimeDiary;
import com.sers.mobility.study.app.models.transients.ListAppTimeDiary;
import com.sers.mobility.study.app.models.transients.TimeDiaryRequest;
import com.sers.mobility.study.app.sessionManager.AccessTokenManager;
import com.sers.mobility.study.app.sessionManager.AppDurationManager;
import com.sers.mobility.study.app.sessionManager.AppTypeManager;
import com.sers.mobility.study.app.sessionManager.EnableGpsTrackerManager;
import com.sers.mobility.study.app.sessionManager.NotificationDurationManager;
import com.sers.mobility.study.app.sessionManager.SessionManager;
import com.sers.mobility.study.app.utils.AppUtils;
import com.sers.mobility.study.app.utils.LocationUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ApplicationSettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioButton manualDataInput, gpsDetermined, notificationDisEnabled, notificationEnabled;
    private EnableGpsTrackerManager gpsTrackerManager = null;
    private TextView gpsStatus, durationTitle, notificationTitle;
    private LocationUtils locationUtils = null;
    private GPSTracker gpsTracker;
    private AppCompatButton saveDuration, saveNotificationDuration, syncTimeDiaryButton;
    private SessionManager sessionManager = null;
    private SystemProgressDialog systemProgressDialog = null;
    private AppTypeManager appTypeManager = null;
    private EditText distanceInMetres, timeInSeconds, notificationTimeInMinutes;
    private AppDurationManager appDurationManager = null;
    private NotificationDurationManager notificationDurationManager = null;
    private String userId;
    private String selectedNotificationStatus = AppConstants.DEFAULT_APP_NOTIFICATION_ENABLED;
    private String selectedCollectionMethod = MethodType.MANUAL_INPUT.getName();
    private LinearLayout gpsTimeIntervalLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(AppToolBarTitles.SETTINGS.getName());

        gpsTimeIntervalLayout = (LinearLayout) findViewById(R.id.gpsTimeIntervalLayout);
        AppCompatButton appCompatButton = (AppCompatButton) findViewById(R.id.backButton);
        appCompatButton.setVisibility(View.VISIBLE);
        appCompatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationController.navigateToActivity(ApplicationSettingsActivity.this, getApplicationContext(), MainActivity.class);
            }
        });


        gpsStatus = (TextView) findViewById(R.id.gpsStatus);
        durationTitle = (TextView) findViewById(R.id.durationTitle);
        notificationTitle = (TextView) findViewById(R.id.notificationTitle);

        durationTitle.setPaintFlags(durationTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        notificationTitle.setPaintFlags(notificationTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        manualDataInput = (RadioButton) findViewById(R.id.manualDataInput);
        gpsDetermined = (RadioButton) findViewById(R.id.gpsDetermined);

        notificationDisEnabled = (RadioButton) findViewById(R.id.notificationDisEnabled);
        notificationEnabled = (RadioButton) findViewById(R.id.notificationEnabled);

        distanceInMetres = (EditText) findViewById(R.id.distanceInMetres);
        timeInSeconds = (EditText) findViewById(R.id.timeInSeconds);
        notificationTimeInMinutes = (EditText) findViewById(R.id.notificationTimeInSeconds);
        saveDuration = (AppCompatButton) findViewById(R.id.saveDuration);
        saveNotificationDuration = (AppCompatButton) findViewById(R.id.saveNotificationDuration);

        syncTimeDiaryButton = (AppCompatButton) findViewById(R.id.syncTimeDiary);
        syncTimeDiaryButton.setOnClickListener(this);

        systemProgressDialog = new SystemProgressDialog(this);
        sessionManager = new SessionManager(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        userId = user.get(SessionManager.KEY_USER_ID);

        notificationDurationManager = new NotificationDurationManager(ApplicationSettingsActivity.this);
        HashMap<String, String> notificationManager = notificationDurationManager.getAppDurationSettings();
        notificationTimeInMinutes.setText(notificationManager.get(NotificationDurationManager.KEY_DURATION_IN_MINUTES));
        String reminder = notificationManager.get(NotificationDurationManager.KEY_ENABLED);
        if (reminder.equalsIgnoreCase(AppConstants.DEFAULT_APP_NOTIFICATION_ENABLED)) {
            notificationEnabled(true);
        } else if (reminder.equalsIgnoreCase(AppConstants.DEFAULT_APP_NOTIFICATION_ENABLED_NO)) {
            notificationDisEnabled(true);
        }

        appTypeManager = new AppTypeManager(ApplicationSettingsActivity.this);
        HashMap<String, String> appType = appTypeManager.getAppTypeSettings();
        String methodType = appType.get(AppTypeManager.KEY_DESIRED_METHOD);
        if (methodType != null) {
            if (methodType.equalsIgnoreCase(MethodType.GPS_DETERMINED.getName()))
                gpsDeterminedInput(true);
            else if (methodType.equalsIgnoreCase(MethodType.MANUAL_INPUT.getName()))
                manualDataInput(true);
        } else
            manualDataInput(true);
        appDurationManager = new AppDurationManager(ApplicationSettingsActivity.this);
        HashMap<String, String> appDuration = appDurationManager.getAppDurationSettings();
        final String durationInSeconds = appDuration.get(AppDurationManager.KEY_DURATION_IN_SECONDS);
        String distance = appDuration.get(AppDurationManager.KEY_DISTANCE_IN_METRES);
        if (durationInSeconds != null) {
            timeInSeconds.setText(durationInSeconds);
        } else timeInSeconds.setText(AppConstants.DEFAULT_APP_DURATION_IN_SECONDS);
        if (distance != null) {
            distanceInMetres.setText(distance);
        } else distanceInMetres.setText(AppConstants.DEFAULT_APP_DISTANCE_IN_METRES);

        saveDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appDurationManager.createAppPreference();
                appDurationManager.editAppDuration(String.valueOf(timeInSeconds.getText()), String.valueOf(100));
                AppUtils.showMessage(false, AppMessages.REBOOT_TITLE, AppMessages.REBOOT_DEVICE, ApplicationSettingsActivity.this);
            }
        });

        saveNotificationDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (String.valueOf(notificationTimeInMinutes.getText()) == null || String.valueOf(notificationTimeInMinutes.getText()) == "0") {
                    AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide time duration in seconds and try again", ApplicationSettingsActivity.this);
                } else {
                    notificationDurationManager.clearAppDurationManager();
                    notificationDurationManager.editAppDuration(String.valueOf(notificationTimeInMinutes.getText()), selectedNotificationStatus);
                    if (selectedNotificationStatus.equalsIgnoreCase(AppConstants.DEFAULT_APP_NOTIFICATION_ENABLED)) {
                        notificationEnabled(true);
                    } else if (selectedNotificationStatus.equalsIgnoreCase(AppConstants.DEFAULT_APP_NOTIFICATION_ENABLED_NO)) {
                        notificationDisEnabled(true);
                    }
                    AppUtils.showMessage(false, AppMessages.REBOOT_TITLE, AppMessages.REBOOT_DEVICE, ApplicationSettingsActivity.this);
                }
            }
        });
        locationUtils = new LocationUtils(ApplicationSettingsActivity.this);
        gpsTracker = new GPSTracker(ApplicationSettingsActivity.this);
        gpsTrackerManager = new EnableGpsTrackerManager(ApplicationSettingsActivity.this);
        if (gpsTrackerManager != null) {
            if (locationUtils.isGPSEnabled()) {
                gpsStatus.setText(AppMessages.GPS_STATUS_INITIAL_TEXT + EnableGpsTrackerStatus.ON.getStatus());
            } else {
                gpsStatus.setText(AppMessages.GPS_STATUS_INITIAL_TEXT + EnableGpsTrackerStatus.OFF.getStatus());
            }
        }

        notificationEnabled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedNotificationStatus = AppConstants.DEFAULT_APP_NOTIFICATION_ENABLED;
                notificationEnabled(true);
            }
        });

        notificationDisEnabled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedNotificationStatus = AppConstants.DEFAULT_APP_NOTIFICATION_ENABLED_NO;
                notificationDisEnabled(true);
            }
        });

        manualDataInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedCollectionMethod = MethodType.MANUAL_INPUT.getName();
                appTypeManager.clearAppTypeManager();
                appTypeManager.editAppType(selectedCollectionMethod);
                manualDataInput(true);

                if (!locationUtils.isGPSEnabled())
                    gpsStatus.setText(AppMessages.GPS_STATUS_INITIAL_TEXT + EnableGpsTrackerStatus.OFF.getStatus());
                else
                    locationUtils.statusCheck(view, false, AppMessages.TURN_OFF_GPS);
            }
        });


        gpsDetermined.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedCollectionMethod = MethodType.GPS_DETERMINED.getName();
                appTypeManager.clearAppTypeManager();
                appTypeManager.editAppType(selectedCollectionMethod);
                gpsDeterminedInput(true);

                if (locationUtils.isGPSEnabled())
                    gpsStatus.setText(AppMessages.GPS_STATUS_INITIAL_TEXT + EnableGpsTrackerStatus.ON.getStatus());
                else
                    locationUtils.statusCheck(view, true, AppMessages.TURN_ON_GPS);
            }
        });

    }

    private void notificationEnabled(boolean enabled) {
        notificationDisEnabled.setChecked(!enabled);
        notificationDisEnabled.setEnabled(enabled);
        notificationEnabled.setChecked(enabled);
        notificationEnabled.setEnabled(!enabled);

    }

    private void notificationDisEnabled(boolean enabled) {
        notificationDisEnabled.setChecked(enabled);
        notificationDisEnabled.setEnabled(!enabled);
        notificationEnabled.setChecked(!enabled);
        notificationEnabled.setEnabled(enabled);
    }


    private void gpsDeterminedInput(boolean enabled) {
        manualDataInput.setChecked(!enabled);
        manualDataInput.setEnabled(enabled);
        gpsDetermined.setChecked(enabled);
        gpsDetermined.setEnabled(!enabled);
        gpsTimeIntervalLayout.setVisibility(View.VISIBLE);
    }

    private void manualDataInput(boolean enabled) {
        manualDataInput.setChecked(enabled);
        manualDataInput.setEnabled(!enabled);
        gpsDetermined.setChecked(!enabled);
        gpsDetermined.setEnabled(enabled);
        gpsTimeIntervalLayout.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        NavigationController.navigateToActivity(this, getApplicationContext(), MainActivity.class);
    }

    /**
     * Is called when the user receives an event like a call or a text message,
     * when onPause() is called the Activity may be partially or completely hidden.
     * You would want to save user data in onPause, in case he hits back button
     * without saving the data explicitly
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Is called when the user resumes his Activity which he left a while ago,
     * say he presses home button and then comes back to app, onResume() is called.
     * You can do the network related updates here or anything of this sort in onResume.
     */
    @Override
    protected void onResume() {
        super.onResume();
    }


    /**
     * Is called when the Activity is being destroyed either by the system, or by the user,
     * say by hitting back, until the app exits.
     * Its compulsory that you save any user data that you want to persist in onDestroy(),
     * because the system will not do it for you.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.syncTimeDiary:
                syncTimeDiary();
                break;
        }
    }

    private void clearSyncedTimeDiaries() {
        AppUtils.showConfirmMessage("Are you sure you want to clear Synced Time diaries?", ApplicationSettingsActivity.this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int pressed) {
                switch (pressed) {
                    case DialogInterface.BUTTON_POSITIVE:
                        systemProgressDialog.showProgressDialog("Clearing Synced Time Diary Data, Please Wait....");
                        new UpdateDatabaseUtil().clearSyncedTimeDiaries();
                        systemProgressDialog.closeProgressDialog();
                        break;
                }
            }
        });
    }

    private void syncTimeDiary() {
        try {
            if (new AppUtils().isNetworkAvailable(ApplicationSettingsActivity.this)) {
                systemProgressDialog.showProgressDialog("Synchronizing Time Diary Data, Please Wait....");
                List<TimeDiary> unSyncedTimeDiaries = new QueryDatabaseUtil().getListOfTimeDiariesFromDatabase(userId, RecordStatus.UN_SYNCED.getStatus());
                if (unSyncedTimeDiaries != null) {
                    if (unSyncedTimeDiaries.size() > 0) {
                        List<TimeDiaryRequest> appTimeDiaries = new ArrayList<TimeDiaryRequest>();
                        for (TimeDiary timeDiary : unSyncedTimeDiaries) {
                            if (timeDiary != null) {
                                TimeDiaryRequest timeDiaryRequest = new TimeDiaryRequest();
                                timeDiaryRequest.setDate(AppUtils.encryptData(timeDiary.getDate()));
                                timeDiaryRequest.setStartTime(AppUtils.encryptData(timeDiary.getStartTime()));
                                timeDiaryRequest.setEndTime(AppUtils.encryptData(timeDiary.getEndTime()));
                                timeDiaryRequest.setUserId(AppUtils.encryptData(userId));
                                timeDiaryRequest.setRecordId(AppUtils.encryptData(timeDiary.recordId));
                                timeDiaryRequest.setTimeCategorySuggestionId(AppUtils.encryptData(timeDiary.getSuggestionId()));
                                appTimeDiaries.add(timeDiaryRequest);
                            }
                        }
                        String accessToken = new AccessTokenManager(this).getAccessToken();
                        ListAppTimeDiary listAppTimeDiary = new ListAppTimeDiary(AppUtils.encryptData(accessToken), AppUtils.encryptData(userId), appTimeDiaries);
                        new WebService(systemProgressDialog, ApplicationSettingsActivity.this, listAppTimeDiary, null).saveAppListOfTimeDiariesService();

                    } else {
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessageWithOutButton(true, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE, "No time diaries to Sync", ApplicationSettingsActivity.this);
                    }
                }
            } else {
                new AppUtils().noInternet(ApplicationSettingsActivity.this, "Time diary Data Sync");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
