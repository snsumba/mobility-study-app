package com.sers.mobility.study.app.models.transients;

import java.io.Serializable;

public class ServerUpdate implements Serializable {
    public int pk;
    public String userId;
    public String appType;
    public String responseStatus;
    public String responseMessage;
    public String durationInMinutes;
    public String distanceApart;
    public String apiToken;

    public ServerUpdate() {
    }

    public ServerUpdate(int pk, String userId, String responseStatus, String responseMessage) {
        this.pk = pk;
        this.userId = userId;
        this.responseStatus = responseStatus;
        this.responseMessage = responseMessage;
    }

    public String getAppType() {

        return appType;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getDurationInMinutes() {
        return durationInMinutes;
    }

    public String getDistanceApart() {
        return distanceApart;
    }

    public String getUserId() {
        return userId;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setDurationInMinutes(String durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }

    public void setDistanceApart(String distanceApart) {
        this.distanceApart = distanceApart;
    }

    public int getPk() {
        return pk;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}