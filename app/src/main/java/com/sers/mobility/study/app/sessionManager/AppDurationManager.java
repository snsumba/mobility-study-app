package com.sers.mobility.study.app.sessionManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.sers.mobility.study.app.constants.AppConstants;

import java.util.HashMap;


public class AppDurationManager {
    // Shared Preferences
    public SharedPreferences pref;
    // Editor for Shared preferences
    Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREF_NAME = "AppDurationManager";


    /**
     * make variables public to access from outside
     */
    public static final String KEY_DURATION_IN_SECONDS = "durationInSeconds";
    public static final String KEY_DISTANCE_IN_METRES = "distanceInMetres";

    public AppDurationManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createAppPreference() {
        editor.putString(KEY_DURATION_IN_SECONDS, AppConstants.DEFAULT_APP_DURATION_IN_SECONDS);
        editor.putString(KEY_DISTANCE_IN_METRES, AppConstants.DEFAULT_APP_DISTANCE_IN_METRES);
        editor.commit();
    }


    public void editAppDuration(String durationInSeconds, String distanceInMetres) {
        if (durationInSeconds != null) {
            editor.putString(KEY_DURATION_IN_SECONDS, durationInSeconds);
            editor.putString(KEY_DISTANCE_IN_METRES, distanceInMetres);
            editor.commit();
            System.out.println("GPS Commit Successful");
        }
    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getAppDurationSettings() {
        HashMap<String, String> domain = new HashMap<String, String>();
        //  Put application domain details in the hash map
        domain.put(KEY_DURATION_IN_SECONDS, pref.getString(KEY_DURATION_IN_SECONDS, null));
        domain.put(KEY_DISTANCE_IN_METRES, pref.getString(KEY_DISTANCE_IN_METRES, null));
        // return user
        return domain;
    }

    /**
     * Clear session details
     */
    public boolean clearAppDurationManager() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        return true;
    }


}