package com.sers.mobility.study.app.notification;

/**
 * Created by kdeo on 4/14/2017.
 */

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.gpsServices.StartGPSIntervalScheduleServiceReceiver;
import com.sers.mobility.study.app.sessionManager.AppDurationManager;
import com.sers.mobility.study.app.sessionManager.NotificationDurationManager;

import java.util.Calendar;
import java.util.HashMap;

public class NotificationIntervalScheduleReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmManager service = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intervalIntent = new Intent(context, StartNotificationIntervalScheduleServiceReceiver.class);
        PendingIntent pending = PendingIntent.getBroadcast(context, 0, intervalIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        Calendar cal = Calendar.getInstance();

        cal.add(Calendar.SECOND, 10);
        NotificationDurationManager appDurationManager = new NotificationDurationManager(context);
        HashMap<String, String> domain = appDurationManager.getAppDurationSettings();
        String durationInMinutes = domain.get(NotificationDurationManager.KEY_DURATION_IN_MINUTES);
        String enabled = domain.get(NotificationDurationManager.KEY_ENABLED);
        long longDurationInMinutes = 15;

        if (enabled.equalsIgnoreCase(AppConstants.DEFAULT_APP_NOTIFICATION_ENABLED)) {
            if (durationInMinutes != null) {
                if (durationInMinutes.matches("[0-9]+")) {
                    longDurationInMinutes = new Long(durationInMinutes);
                }
            }
            long REPEAT_TIME = 1000 * 60 * longDurationInMinutes;

            service.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), REPEAT_TIME, pending);
            service.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), REPEAT_TIME, pending);
        }
    }
}
