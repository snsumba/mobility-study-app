package com.sers.mobility.study.app.sessionManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.sers.mobility.study.app.constants.EnableGpsTrackerStatus;

import java.util.HashMap;


public class EnableGpsTrackerManager {
    // Shared Preferences
    public SharedPreferences pref;
    // Editor for Shared preferences
    Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREF_NAME = "EnableGpsTrackerManager";
    private static final String DEFAULT_GPS_DESIRED_STATE = EnableGpsTrackerStatus.OFF.getStatus();

    /**
     * make variables public to access from outside
     */
    public static final String KEY_DESIRED_STATE = "desiredState";

    public EnableGpsTrackerManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void createAppPreference() {
        editor.putString(KEY_DESIRED_STATE, DEFAULT_GPS_DESIRED_STATE);
        // commit changes
        editor.commit();
    }

    /**
     * Create login session
     */
    public void editAppGpsStatte(String gpsState) {
        if (gpsState != null) {
            editor.putString(KEY_DESIRED_STATE, gpsState);
            editor.commit();
            System.out.println("GPS Commit Successful");
        }
    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getGPSTrackerSettings() {
        HashMap<String, String> domain = new HashMap<String, String>();
        //  Put application domain details in the hash map
        domain.put(KEY_DESIRED_STATE, pref.getString(KEY_DESIRED_STATE, null));
        // return user
        return domain;
    }

    /**
     * Clear session details
     */
    public boolean clearAppGpsManager() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        return true;

    }


}