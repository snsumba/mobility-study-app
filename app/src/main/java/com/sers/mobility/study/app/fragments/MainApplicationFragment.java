package com.sers.mobility.study.app.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.adapters.tabAdapters.MobilityViewPagerAdapter;
import com.sers.mobility.study.app.baseFiles.BaseMainFragment;
import com.sers.mobility.study.app.custom.SlidingTabLayout;


public class MainApplicationFragment extends BaseMainFragment {

    private Activity activity;
    private MobilityViewPagerAdapter viewPagerAdapter = null;

    public MainApplicationFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public MainApplicationFragment(FragmentManager compactibleFragmentManager, CharSequence[] titles, int tabCount, Activity activity) {
        super(compactibleFragmentManager, titles, tabCount);
        this.activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_application, container, false);
        try {
            // Assigning ViewPager View and setting the adapter
            pager = (ViewPager) rootView.findViewById(R.id.pager);
            viewPagerAdapter = new MobilityViewPagerAdapter(getActivity().getSupportFragmentManager(), titles, tabCount, activity);
            pager.setAdapter(viewPagerAdapter);
            tabLayout = (SlidingTabLayout) rootView.findViewById(R.id.tabLayout);
            tabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            tabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.white));
            //To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
            tabLayout.setDistributeEvenly(true);
            // Setting Custom Color for the Scroll bar indicator of the Tab View
            tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
                @Override
                public int getIndicatorColor(int position) {
                    return getResources().getColor(R.color.white);
                }
            });
            tabLayout.setViewPager(pager);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rootView;
    }
}
