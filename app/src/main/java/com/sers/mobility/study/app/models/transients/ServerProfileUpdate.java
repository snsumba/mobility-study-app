package com.sers.mobility.study.app.models.transients;


import java.util.List;

public class ServerProfileUpdate extends ServerUpdate {
    private String firstName;
    private String lastName;
    private String location;
    private String gender;
    private String username;
    private String password;
    private String dateOfBirth;
    private String phoneNumber;
    private String isSessionAvailable;
    private List<TimeDiaryCategorySuggestion> categorySuggestions;

    public ServerProfileUpdate() {
    }

    public ServerProfileUpdate(String firstName, String lastName, String location, String gender,
                               String username, String password,
                               String dateOfBirth, String phoneNumber, String isSessionAvailable) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.location = location;
        this.gender = gender;
        this.username = username;
        this.password = password;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
        this.isSessionAvailable = isSessionAvailable;
    }

    public ServerProfileUpdate(int pk, String userId, String responseStatus, String responseMessage, String firstName, String lastName, String location, String gender,
                               String username, String password, String dateOfBirth, String phoneNumber, String isSessionAvailable, String distanceApart) {
        super(pk, userId, responseStatus, responseMessage);
        this.firstName = firstName;
        this.lastName = lastName;
        this.location = location;
        this.gender = gender;
        this.username = username;
        this.password = password;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
        this.isSessionAvailable = isSessionAvailable;

    }

    public List<TimeDiaryCategorySuggestion> getCategorySuggestions() {
        return categorySuggestions;
    }

    public void setCategorySuggestions(List<TimeDiaryCategorySuggestion> categorySuggestions) {
        this.categorySuggestions = categorySuggestions;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIsSessionAvailable() {
        return isSessionAvailable;
    }

    public void setIsSessionAvailable(String isSessionAvailable) {
        this.isSessionAvailable = isSessionAvailable;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
