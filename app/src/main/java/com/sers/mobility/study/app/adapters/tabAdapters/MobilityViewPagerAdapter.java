package com.sers.mobility.study.app.adapters.tabAdapters;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.sers.mobility.study.app.baseFiles.BaseViewPagerAdapter;
import com.sers.mobility.study.app.fragments.mainMenu.PickUserLocationsFragment;
import com.sers.mobility.study.app.fragments.mainMenu.PlacesVisitedFragment;
import com.sers.mobility.study.app.fragments.mainMenu.TimeDiaryFragment;
import com.sers.mobility.study.app.fragments.mainMenu.UserMappedLocationsFragment;
import com.sers.mobility.study.app.fragments.optionsMenu.AboutUsFragment;

public class MobilityViewPagerAdapter extends BaseViewPagerAdapter {

    private Activity activity;

    public MobilityViewPagerAdapter(FragmentManager fm, CharSequence[] titles, int tabCount, Activity activity) {
        super(fm, titles, tabCount);
        this.activity = activity;
    }

    /**
     * This method return the fragment for the every position in the View Pager
     */
    @Override
    public Fragment getItem(int position) {

        if (tabCount > 0) {
           /* if (position == 0) {
                return new AboutUsFragment();
            } else if (position == 1) {
                return new TimeDiaryFragment(activity);
            } else if (position == 2) {
                return new PlacesVisitedFragment(activity);
            } else if (position == 3) {
                return new UserMappedLocationsFragment();
            } else if (position == 4) {
                return new PickUserLocationsFragment(activity);
            }*/
            if (position == 0) {
                return new TimeDiaryFragment(activity);
            } else if (position == 1) {
                return new PlacesVisitedFragment(activity);
            } else if (position == 2) {
                return new UserMappedLocationsFragment();
            } else if (position == 3) {
                return new PickUserLocationsFragment(activity);
            }

        }
        return null;
    }

}
