package com.sers.mobility.study.app.notification;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.sers.mobility.study.app.gpsServices.BackGroundOperation;

public class NotificationService extends Service {

    private final IBinder mBinder = new NotificationService.MyBinder();

    public NotificationService() {
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new BackGroundNotificationOperation(getApplicationContext()).showNotification();
        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }

    public class MyBinder extends Binder {
        public NotificationService getService() {
            return NotificationService.this;
        }
    }
}
