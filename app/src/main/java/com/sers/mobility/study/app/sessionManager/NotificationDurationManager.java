package com.sers.mobility.study.app.sessionManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.sers.mobility.study.app.constants.AppConstants;

import java.util.HashMap;


public class NotificationDurationManager {
    // Shared Preferences
    public SharedPreferences pref;
    // Editor for Shared preferences
    Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREF_NAME = "NotificationDurationManager";


    /**
     * make variables public to access from outside
     */
    public static final String KEY_DURATION_IN_MINUTES = "durationInMinutes";
    public static final String KEY_ENABLED = "enabled";

    public NotificationDurationManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createAppPreference() {
        editor.putString(KEY_DURATION_IN_MINUTES, AppConstants.DEFAULT_APP_NOTIFICATION_DURATION_IN_MINUTES);
        editor.putString(KEY_ENABLED, AppConstants.DEFAULT_APP_NOTIFICATION_ENABLED_NO);
        editor.commit();
    }


    public void editAppDuration(String durationInSeconds, String enabled) {
        if (durationInSeconds != null) {
            editor.putString(KEY_DURATION_IN_MINUTES, durationInSeconds);
            editor.putString(KEY_ENABLED, enabled);
            editor.commit();
        }
    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getAppDurationSettings() {
        HashMap<String, String> domain = new HashMap<String, String>();
        //  Put application domain details in the hash map
        domain.put(KEY_DURATION_IN_MINUTES, pref.getString(KEY_DURATION_IN_MINUTES, null));
        domain.put(KEY_ENABLED, pref.getString(KEY_ENABLED, null));
        return domain;
    }

    /**
     * Clear session details
     */
    public boolean clearAppDurationManager() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        return true;
    }


}