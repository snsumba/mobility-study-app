package com.sers.mobility.study.app.custom;

import android.app.Activity;
import android.app.ProgressDialog;


public class SystemProgressDialog {
    private ProgressDialog progressDialog;
    private Activity activity;

    public SystemProgressDialog(Activity activity) {
        this.activity = activity;
    }

    public void showProgressDialog(String message) {
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(message);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void closeProgressDialog() {
        progressDialog.dismiss();
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }
}
