package com.sers.mobility.study.app.databaseUtils;


import com.sers.mobility.study.app.models.databases.AppTimeDiaryCategory;
import com.sers.mobility.study.app.models.databases.AppTimeDiaryCategorySuggestion;
import com.sers.mobility.study.app.models.databases.CapturedLocation;
import com.sers.mobility.study.app.models.databases.RegisteredAccount;
import com.sers.mobility.study.app.models.databases.TimeDiary;
import com.sers.mobility.study.app.models.transients.ServerCapturedLocation;
import com.sers.mobility.study.app.models.transients.ServerRegisteredAccount;

import org.sers.kpa.datasource.datamanager.CustomDataAccessManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QueryDatabaseUtil implements Serializable {
    public QueryDatabaseUtil() {
    }

    public List<TimeDiary> getListOfTimeDiariesFromDatabase(String userId) {
        try {
            System.out.println("lookUp userId        : z " + userId);
            List<TimeDiary> timeDiaries = new CustomDataAccessManager(TimeDiary.class)
                    .addFilterEqualTo("userId", userId)
                    .sortDesc("orderingParameter")
                    .searchDatabase();
            return timeDiaries;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<CapturedLocation> getListOfCapturedLocationsFromDatabase(String userId) {
        try {
            System.out.println("lookUp userId        : z " + userId);
            List<CapturedLocation> capturedLocations = new CustomDataAccessManager(CapturedLocation.class)
                    .addFilterEqualTo("userId", userId)
                    .sortDesc("date")
                    .searchDatabase();
            Collections.sort(capturedLocations, Collections.reverseOrder());
            return capturedLocations;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<AppTimeDiaryCategorySuggestion> getAppTimeDiaryCategorySuggestionSpinnerItems(String defualtText) {
        List<AppTimeDiaryCategorySuggestion> list = new ArrayList<AppTimeDiaryCategorySuggestion>();
        List<AppTimeDiaryCategorySuggestion> dbList = getListOfAppTimeDiaryCategorySuggestionsFromDatabase();
        if (dbList != null) {
            System.out.println("lookUp category      : 11 size " + dbList.size());
            if (dbList.size() != 0) {
                list.add(new AppTimeDiaryCategorySuggestion(defualtText));
                for (AppTimeDiaryCategorySuggestion categorySuggestion : dbList) {
                    list.add(categorySuggestion);
                }
            } else list.add(new AppTimeDiaryCategorySuggestion(defualtText));
        } else list.add(new AppTimeDiaryCategorySuggestion(defualtText));
        return list;
    }

    public List<AppTimeDiaryCategory> getListOfAppTimeDiaryCategoryFromDatabase() {
        try {
            return new CustomDataAccessManager(AppTimeDiaryCategory.class).searchDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<AppTimeDiaryCategorySuggestion> getListOfAppTimeDiaryCategorySuggestionsFromDatabase() {
        try {
            System.out.println("lookUp category        : 11 default");
            return new CustomDataAccessManager(AppTimeDiaryCategorySuggestion.class)
                    .searchDatabase();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public AppTimeDiaryCategorySuggestion getAppTimeDiaryCategorySuggestionFromDatabaseBySuggestionAndCategory(String suggestion) {
        try {
            System.out.println("lookUp category        : 12 " + suggestion);
            return (AppTimeDiaryCategorySuggestion) new CustomDataAccessManager(AppTimeDiaryCategorySuggestion.class)
                    .addFilterEqualTo("suggestion", suggestion)
                    .searchUnique();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public CapturedLocation getCapturedLocationFromDatabase(ServerCapturedLocation serverCapturedLocation) {
        try {
            return (CapturedLocation) new CustomDataAccessManager(CapturedLocation.class)
                    .addFilterEqualTo("userId", serverCapturedLocation.getUserId())
                    .addFilterEqualTo("recordId", serverCapturedLocation.getRecordId())
                    .searchUnique();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public CapturedLocation getCapturedLocationFromDatabase(String userId, String recordId) {
        try {
            System.out.println("userid : captured location : " + userId);
            System.out.println("recordId : captured location : " + recordId);
            return (CapturedLocation) new CustomDataAccessManager(CapturedLocation.class)
                    .addFilterEqualTo("userId", userId)
                    .addFilterEqualTo("recordId", recordId)
                    .searchUnique();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public TimeDiary getTimeDiaryFromDatabase(String userId, String recordId) {
        try {
            System.out.println("userid : time diary : " + userId);
            System.out.println("recordId : time diary : " + recordId);
            return (TimeDiary) new CustomDataAccessManager(TimeDiary.class)
                    .addFilterEqualTo("userId", userId)
                    .addFilterEqualTo("recordId", recordId)
                    .searchUnique();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public CapturedLocation getCapturedLocationFromDatabaseByUserId(String userId) {
        try {
            System.out.println("lookUp userId        : x " + userId);

            return (CapturedLocation) new CustomDataAccessManager(CapturedLocation.class)
                    .addFilterEqualTo("userId", userId)
                    .searchUnique();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public CapturedLocation getCapturedLocationFromDatabaseByRecordId(String recordId) {
        try {
            System.out.println("lookUp recordId        : x " + recordId);

            return (CapturedLocation) new CustomDataAccessManager(CapturedLocation.class)
                    .addFilterEqualTo("recordId", recordId)
                    .searchUnique();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public TimeDiary getTimeDiaryFromDatabaseByRecordId(String recordId) {
        try {
            System.out.println("lookUp recordId        : x " + recordId);

            return (TimeDiary) new CustomDataAccessManager(TimeDiary.class)
                    .addFilterEqualTo("recordId", recordId)
                    .searchUnique();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<AppTimeDiaryCategory> getListOfAppTimeDiaryCategoriesFromDatabase() {
        try {
            return new CustomDataAccessManager(AppTimeDiaryCategory.class).searchDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<CapturedLocation> getListOfCapturedLocationsFromDatabase(String userId, String status) {
        try {
            System.out.println("lookUp userId        : z " + userId);
            return new CustomDataAccessManager(CapturedLocation.class)
                    .addFilterEqualTo("userId", userId)
                    .addFilterEqualTo("status", status)
                    .searchDatabase();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<TimeDiary> getListOfTimeDiariesFromDatabase(String userId, String status) {
        try {
            System.out.println("lookUp userId        : z " + userId);
            System.out.println("lookUp status        : z " + status);
            return new CustomDataAccessManager(TimeDiary.class)
                    .addFilterEqualTo("userId", userId)
                    .addFilterEqualTo("status", status)
                    .searchDatabase();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<CapturedLocation> getListOfCapturedLocationsFromDatabase() {
        try {
            return new CustomDataAccessManager(CapturedLocation.class)
                    .searchDatabase();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public RegisteredAccount getRegisteredAccountFromDatabase(ServerRegisteredAccount serverRegisteredAccount) {
        try {
            return (RegisteredAccount) new CustomDataAccessManager(RegisteredAccount.class)
                    .addFilterEqualTo("userId", serverRegisteredAccount.getUserId())
                    .addFilterEqualTo("pk", serverRegisteredAccount.getPk())
                    .searchUnique();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public RegisteredAccount getRegisteredAccountFromDatabaseByUsername(String username) {
        try {
            return (RegisteredAccount) new CustomDataAccessManager(RegisteredAccount.class)
                    .addFilterEqualTo("username", username)
                    .searchUnique();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public RegisteredAccount getRegisteredAccountFromDatabaseByUserId(String userId) {
        try {
            return (RegisteredAccount) new CustomDataAccessManager(RegisteredAccount.class)
                    .addFilterEqualTo("userId", userId)
                    .searchUnique();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public RegisteredAccount getRegisteredAccountFromDatabaseByUsernameAndPassword(String username, String password) {
        try {
            System.out.println("username : " + username);
            System.out.println("password : " + password);
            return (RegisteredAccount) new CustomDataAccessManager(RegisteredAccount.class)
                    .addFilterEqualTo("username", username)
                    .addFilterEqualTo("password", password)
                    .searchUnique();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<RegisteredAccount> getListOfRegisteredAccountsFromDatabase(String userId) {
        try {
            return new CustomDataAccessManager(RegisteredAccount.class)
                    .addFilterEqualTo("userId", userId)
                    .searchDatabase();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<RegisteredAccount> getListOfRegisteredAccountsFromDatabase() {
        try {
            return new CustomDataAccessManager(RegisteredAccount.class).searchDatabase();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
