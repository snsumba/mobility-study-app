package com.sers.mobility.study.app.sessionManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.sers.mobility.study.app.models.transients.ServerProfileUpdate;
import com.sers.mobility.study.app.utils.AppUtils;

import java.util.HashMap;


public class SessionManager {
    // Shared Preferences
    public SharedPreferences pref;
    // Editor for Shared preferences
    Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREF_NAME = "SessionPreferences";

    /**
     * make all Shared Preferences Keys variables public to access from outside
     */
    public static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_IS_LOGGED_IN = "isSessionAvailable";
    //public static final String KEY_APP_METHOD_TYPE = "methodType";
    public static final String KEY_USER_ID = "userId";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_FIRST_NAME = "firstName";
    public static final String KEY_LAST_NAME = "lastName";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_PHONE_NUMBER = "phoneNumber";
    public static final String KEY_LOCATION = "location";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_DATE_OF_BIRTH = "dateOfBirth";
    //public static final String KEY_DISTANCE_APART_IN_KILOMETRES = "distanceApart";
    //public static final String KEY_DURATION_IN_MINUTES = "durationInMinutes";

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();

    }

    /**
     * Create login session
     */

    public void createLoginSession(ServerProfileUpdate serverProfileUpdate, String password) {
        if (serverProfileUpdate != null) {
            editor.putBoolean(IS_LOGIN, true);
            // editor.putString(KEY_APP_METHOD_TYPE, serverProfileUpdate.getAppType());
            editor.putString(KEY_IS_LOGGED_IN, serverProfileUpdate.getIsSessionAvailable());
            editor.putString(KEY_USER_ID, AppUtils.decryptData(serverProfileUpdate.getUserId()));
            editor.putString(KEY_USERNAME, AppUtils.decryptData(serverProfileUpdate.getUsername()));
            editor.putString(KEY_FIRST_NAME, AppUtils.decryptData(serverProfileUpdate.getFirstName()));
            editor.putString(KEY_LAST_NAME, AppUtils.decryptData(serverProfileUpdate.getLastName()));
            editor.putString(KEY_GENDER, AppUtils.decryptData(serverProfileUpdate.getGender()));
            editor.putString(KEY_PHONE_NUMBER, AppUtils.decryptData(serverProfileUpdate.getPhoneNumber()));
            editor.putString(KEY_LOCATION, AppUtils.decryptData(serverProfileUpdate.getLocation()));
            editor.putString(KEY_PASSWORD, password);
            editor.putString(KEY_DATE_OF_BIRTH, AppUtils.decryptData(serverProfileUpdate.getDateOfBirth()));
            //   editor.putString(KEY_DISTANCE_APART_IN_KILOMETRES, serverProfileUpdate.getDistanceApart());
            //    editor.putString(KEY_DURATION_IN_MINUTES, serverProfileUpdate.getDurationInMinutes());
            editor.commit();
        }
    }


    /**
     * Check login method wil check user login status If false it will redirect
     * user to login page Else won't do anything
     */
    public boolean checkLogin() {
        // Check login status
        return this.isLoggedIn();
    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        // Put user details in the hash map
        user.put(KEY_IS_LOGGED_IN, pref.getString(KEY_IS_LOGGED_IN, "false"));
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
        user.put(KEY_USER_ID, pref.getString(KEY_USER_ID, null));
        user.put(KEY_FIRST_NAME, pref.getString(KEY_FIRST_NAME, null));
        user.put(KEY_LAST_NAME, pref.getString(KEY_LAST_NAME, null));
        user.put(KEY_GENDER, pref.getString(KEY_GENDER, null));
        user.put(KEY_PHONE_NUMBER, pref.getString(KEY_PHONE_NUMBER, null));
        user.put(KEY_LOCATION, pref.getString(KEY_LOCATION, null));
        //   user.put(KEY_APP_METHOD_TYPE, pref.getString(KEY_APP_METHOD_TYPE, null));
        user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, null));
        //   user.put(KEY_DISTANCE_APART_IN_KILOMETRES, pref.getString(KEY_DISTANCE_APART_IN_KILOMETRES, null));
        user.put(KEY_DATE_OF_BIRTH, pref.getString(KEY_DATE_OF_BIRTH, null));
        //   user.put(KEY_DURATION_IN_MINUTES, pref.getString(KEY_DURATION_IN_MINUTES, null));
        // return user
        return user;
    }

    /**
     * Clear session details
     */
    public boolean clearSessionManager() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        return true;

    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

}