package com.sers.mobility.study.app.models.databases;


import org.sers.kpa.datasource.annotations.Column;
import org.sers.kpa.datasource.annotations.Entity;
import org.sers.kpa.datasource.annotations.Table;
import org.sers.kpa.datasource.datamanager.DatabaseJSONUtils;

import java.util.Date;

@Entity
@Table(name = "capturedlocations")
public class CapturedLocation extends AppBaseEntity {

    @Column(name = "place_name", nullable = false)
    public String placeName;

    @Column(name = "longitude", nullable = false)
    public String longitude;

    @Column(name = "latitude", nullable = false)
    public String latitude;

    @Column(name = "accuracy", nullable = false)
    public String accuracy;

    private String responseStatus;
    private String responseMessage;
    private String durationInMinutes;
    private String dateCreated;

    public CapturedLocation() {
    }

    public CapturedLocation(String userId, String placeName, String longitude, String latitude, String accuracy) {
        this.userId = userId;
        this.placeName = placeName;
        this.longitude = longitude;
        this.latitude = latitude;
        this.accuracy = accuracy;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getDurationInMinutes() {
        return durationInMinutes;
    }

    public void setDurationInMinutes(String durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }

    public String toString() {
        try {
            return DatabaseJSONUtils.convertModelToJSON(this, CapturedLocation.class).toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
