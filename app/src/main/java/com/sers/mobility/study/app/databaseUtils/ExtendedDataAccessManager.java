package com.sers.mobility.study.app.databaseUtils;


import org.sers.kpa.datasource.annotations.Column;
import org.sers.kpa.datasource.annotations.Entity;
import org.sers.kpa.datasource.annotations.PK;
import org.sers.kpa.datasource.annotations.Table;
import org.sers.kpa.datasource.datamanager.CustomDataAccessManager;
import org.sers.kpa.datasource.datamanager.DatabaseUtils;

import java.lang.reflect.Field;
import java.util.Date;

public class ExtendedDataAccessManager<T> extends CustomDataAccessManager<T> {

    public ExtendedDataAccessManager(Class<T> klass) {
        super(klass);
    }

    public CustomDataAccessManager<T> addFilterOrderByDesc2(String property) throws Exception {
        if(super.klass == null) {
            throw new Exception("Class Type not Instantiated properly");
        } else {
            Field field = this.klass.getField(property);
            if(field == null) {
                throw new Exception("No field with name " + property + " has been found on the specified class");
            } else if(!field.isAnnotationPresent(Column.class)) {
                throw new Exception("The specified field " + property + " is not a column on the underlying database structure");
            } else {
                this.conditions.append(" order by " + ((Column)field.getAnnotation(Column.class)).name() + " desc");
               return this;
            }
        }
    }

    public CustomDataAccessManager<T> addFilterOrderByAsc(String property) throws Exception {
        if(super.klass == null) {
            throw new Exception("Class Type not Instantiated properly");
        } else {
            Field field = this.klass.getField(property);
            if(field == null) {
                throw new Exception("No field with name " + property + " has been found on the specified class");
            } else if(!field.isAnnotationPresent(Column.class)) {
                throw new Exception("The specified field " + property + " is not a column on the underlying database structure");
            } else {
                this.conditions.append(" order by " + ((Column)field.getAnnotation(Column.class)).name() + " asc");
                return this;
            }
        }
    }
}
