package com.sers.mobility.study.app.models.databases;


import android.support.annotation.NonNull;

import com.sers.mobility.study.app.constants.RecordStatus;
import com.sers.mobility.study.app.utils.AppUtils;

import org.sers.kpa.datasource.annotations.Column;
import org.sers.kpa.datasource.annotations.MappedSupperClass;
import org.sers.kpa.datasource.annotations.PK;

import java.io.Serializable;
import java.util.Date;


@MappedSupperClass
public class AppBaseEntity implements Serializable, Comparable {

    @PK
    @Column(name = "local_id", nullable = false)
    public int pk = 0;

    @Column(name = "record_id", nullable = true)
    public String recordId = AppUtils.generateUserId();

    @Column(name = "status", nullable = true)
    public String status = RecordStatus.UN_SYNCED.getStatus();

    @Column(name = "date", nullable = true)
    public String date = AppUtils.getCurrentDate();

    @Column(name = "user_id", nullable = true)
    public String userId = AppUtils.generateUserId();

    @Column(name = "method_type", nullable = true)
    public String methodType;

    public AppBaseEntity() {
    }

    public AppBaseEntity(String date) {
        this.date = date;
    }

    public AppBaseEntity(int pk) {
        this.pk = pk;
    }

    public String getMethodType() {
        return methodType;
    }

    public void setMethodType(String methodType) {
        this.methodType = methodType;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public int compareTo(@NonNull Object another) {
        return 0;
    }
}
