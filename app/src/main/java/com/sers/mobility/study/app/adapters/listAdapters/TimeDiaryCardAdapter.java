package com.sers.mobility.study.app.adapters.listAdapters;


import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.constants.AppMessages;
import com.sers.mobility.study.app.constants.RecordStatus;
import com.sers.mobility.study.app.custom.AppMaterialDialog;
import com.sers.mobility.study.app.custom.SystemProgressDialog;
import com.sers.mobility.study.app.databaseUtils.QueryDatabaseUtil;
import com.sers.mobility.study.app.databaseUtils.UpdateDatabaseUtil;
import com.sers.mobility.study.app.models.databases.AppTimeDiaryCategorySuggestion;
import com.sers.mobility.study.app.models.databases.TimeDiary;
import com.sers.mobility.study.app.utils.AppUtils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.List;

public class TimeDiaryCardAdapter extends RecyclerView.Adapter<TimeDiaryCardAdapter.ViewHolder>
        implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private List<TimeDiary> listOfRecords = null;
    public Context context;
    public ColorGenerator generator = ColorGenerator.MATERIAL;
    public int rowLayout;
    public SystemProgressDialog systemProgressDialog;
    public FragmentManager fragmentManager;
    public Activity activity;
    public View.OnClickListener onClickListener = null;
    public TextView categoryTextView, suggestionTextView;
    public EditText leftCardElement3, otherInfoAnswer, activityField, startDateTimeField, endDateTimeField;

    private EditText startDateField, startTimeField, endTimeField;
    public Spinner componentSpinner;
    private AppTimeDiaryCategorySuggestion appTimeDiaryCategorySuggestion = null;
    private String selectedSuggestion = AppConstants.DEFAULT_DROP_DOWN_TEXT;
    public int VIEW_CHANGER = 0;
    public int TIME_VIEW_CHANGER = 0;
    public RecyclerView recyclerView;

    public TimeDiaryCardAdapter(int rowLayout, SystemProgressDialog systemProgressDialog, FragmentManager fragmentManager, Activity activity,
                                List<TimeDiary> listOfRecords, View.OnClickListener onClickListener, RecyclerView recyclerView) {
        this.rowLayout = rowLayout;
        this.context = activity.getApplication().getApplicationContext();
        this.activity = activity;
        this.fragmentManager = fragmentManager;
        this.systemProgressDialog = systemProgressDialog;
        this.onClickListener = onClickListener;
        this.listOfRecords = listOfRecords;
        this.recyclerView = recyclerView;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbnail;
        TextView recordId, activityField, endDateTimeField, dateField, startDateTimeField, editButton, viewButton, statusTextView;

        public ViewHolder(View view) {
            super(view);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            dateField = (TextView) view.findViewById(R.id.dateField);
            startDateTimeField = (TextView) view.findViewById(R.id.startDateTimeField);
            activityField = (TextView) view.findViewById(R.id.activityField);
            endDateTimeField = (TextView) view.findViewById(R.id.endDateTimeField);
            statusTextView = (TextView) view.findViewById(R.id.statusTextView);
            editButton = (TextView) view.findViewById(R.id.editButton);
            viewButton = (TextView) view.findViewById(R.id.viewButton);
        }
    }

    @Override
    public TimeDiaryCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new TimeDiaryCardAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if (listOfRecords != null) {
            if (listOfRecords.size() == 0) holder.dateField.setText(AppConstants.NO_RECORDS);
            else {
                //String date, String suggestion, String startTime, String endTime
                String recordId = listOfRecords.get(position).recordId;
                String suggestion = (String.valueOf(listOfRecords.get(position).suggestion).length() > 0 ? String.valueOf(listOfRecords.get(position).suggestion) : "Not Found");

                TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(suggestion.charAt(0)).toUpperCase(), generator.getRandomColor());
                holder.thumbnail.setImageDrawable(drawable);
                holder.dateField.setText(listOfRecords.get(position).date);
                holder.startDateTimeField.setText(listOfRecords.get(position).startTime);
                holder.activityField.setText(String.valueOf(listOfRecords.get(position).suggestion));
                holder.endDateTimeField.setText(String.valueOf(listOfRecords.get(position).endTime));
                //holder.rightCardElement1.setVisibility(View.GONE);
                holder.statusTextView.setText(listOfRecords.get(position).status);
                holder.editButton.setVisibility(View.GONE);
                holder.viewButton.setVisibility(View.GONE);

                if (String.valueOf(listOfRecords.get(position).status).equalsIgnoreCase(RecordStatus.UN_SYNCED.getStatus())) {
                    holder.editButton.setVisibility(View.VISIBLE);
                    holder.editButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSelectedTimeDiaryInfo(
                                    listOfRecords.get(position).recordId,
                                    listOfRecords.get(position).suggestion,
                                    listOfRecords.get(position).date,
                                    listOfRecords.get(position).startTime,
                                    listOfRecords.get(position).endTime);
                        }
                    });
                }

            }
        } else holder.dateField.setText(AppConstants.NO_RECORDS);
    }

    @Override
    public int getItemCount() {
        return listOfRecords.size();
    }

    private void getSelectedTimeDiaryInfo(final String timeDiaryRecord, final String suggestion, final String startDate,
                                          final String startTime, final String endTime) {
        final MaterialDialog applicationDomainDialog = new AppMaterialDialog().appMaterialDialog(R.layout.popup_edit_time_diary_layout,
                activity, AppConstants.TIME_DIARY_TITLE, R.string.default_text, R.string.negtive_dialog_text,
                R.string.save_text, Boolean.FALSE);

        View dialogView = applicationDomainDialog.getView();

        startDateField = (EditText) dialogView.findViewById(R.id.dateField);
        startTimeField = (EditText) dialogView.findViewById(R.id.startTimeField);
        endTimeField = (EditText) dialogView.findViewById(R.id.endTimeField);

        startTimeField = (EditText) dialogView.findViewById(R.id.startTimeField);
        startTimeField.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    TIME_VIEW_CHANGER = 1;
                    showTimePicker();
                }
                return false;
            }
        });

        endTimeField = (EditText) dialogView.findViewById(R.id.endTimeField);
        endTimeField.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    TIME_VIEW_CHANGER = 2;
                    showTimePicker();
                }
                return false;
            }
        });
        initializeStartAndEndDatesPickers();
        startDateField.setText(startDate);
        startTimeField.setText(startTime);
        endTimeField.setText(endTime);
        componentSpinner = (Spinner) dialogView.findViewById(R.id.componentSpinner);

        List<String> suggestionsList = new AppUtils().getSpinnerItems(new QueryDatabaseUtil().getAppTimeDiaryCategorySuggestionSpinnerItems(AppConstants.DEFAULT_DROP_DOWN_TEXT));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity.getApplicationContext(), R.layout.custom_spinner_content, suggestionsList);
        dataAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown);
        componentSpinner.setAdapter(dataAdapter);
        componentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedSuggestion = String.valueOf(componentSpinner.getSelectedItem());
                if (selectedSuggestion != null) {
                    ((TextView) adapterView.getChildAt(0)).setTextColor(Color.BLACK);
                    if (!selectedSuggestion.equalsIgnoreCase(AppConstants.DEFAULT_DROP_DOWN_TEXT)) {
                        appTimeDiaryCategorySuggestion = new QueryDatabaseUtil().getAppTimeDiaryCategorySuggestionFromDatabaseBySuggestionAndCategory(selectedSuggestion);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        if (suggestionsList != null)
            componentSpinner.setSelection(suggestionsList.indexOf(suggestion));

        final MDButton save = (MDButton) applicationDomainDialog.getActionButton(DialogAction.POSITIVE);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimeDiary timeDiary = new QueryDatabaseUtil().getTimeDiaryFromDatabaseByRecordId(timeDiaryRecord);
                if (timeDiary != null) {
                    String startDate = ((startDateField.getText() != null ? String.valueOf(startDateField.getText()) : AppUtils.getCurrentDateOnly()));
                    String startTime = String.valueOf(startTimeField.getText());
                    String endTime = String.valueOf(endTimeField.getText());

                    timeDiary.setDate(startDate);
                    timeDiary.setStartTime(startTime);
                    timeDiary.setEndTime(endTime);
                    timeDiary.setOrderingParameter(startDate + " " + endTime);
                    timeDiary.setRecordId(timeDiaryRecord);
                    timeDiary.setSuggestionId((appTimeDiaryCategorySuggestion != null ? appTimeDiaryCategorySuggestion.getSuggestionId() : ""));
                    timeDiary.setSuggestion(selectedSuggestion);

                    TimeDiary savedTimeDiary = new UpdateDatabaseUtil().saveOrUpdateTimeDiary(timeDiary);
                    if (savedTimeDiary != null) {
                        AppUtils.showMessageWithOutButton(true, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE, "Successfully updated Time diary", activity);
                        applicationDomainDialog.hide();

                        recyclerView.setAdapter(new TimeDiaryCardAdapter(R.layout.card_row_time_diary_layout,
                                systemProgressDialog, activity.getFragmentManager(), activity,
                                new QueryDatabaseUtil().getListOfTimeDiariesFromDatabase(timeDiary.userId), this, recyclerView));
                    } else {
                        AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Failed to update Time diary", activity);
                        applicationDomainDialog.hide();
                    }
                } else
                    AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Time diary no longer exists", activity);
            }
        });
        applicationDomainDialog.show();
    }


    public void initializeStartAndEndDatesPickers() {
        if (startDateField != null)
            startDateField.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == 0) {
                        VIEW_CHANGER = 1;
                        showDatePicker(TimeDiaryCardAdapter.this);
                    }
                    return false;
                }
            });

    }


    public void showDatePicker(DatePickerDialog.OnDateSetListener onDateSetListener) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                onDateSetListener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(activity.getFragmentManager(), "Select Date");
        dpd.setAccentColor(activity.getResources().getColor(R.color.colorPrimary));
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        if (VIEW_CHANGER == 1) {
            int monthday_start = monthOfYear + 1;
            if (startDateField != null)
                startDateField.setText(year + "/" + monthday_start + "/" + dayOfMonth);
        }
    }

    public void showTimePicker() {
        Calendar calendar = Calendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        boolean is24HourMode = DateFormat.is24HourFormat(activity);
        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(TimeDiaryCardAdapter.this,
                hourOfDay, minute, is24HourMode);
        timePickerDialog.show(activity.getFragmentManager(), "Select Time");
        timePickerDialog.setAccentColor(activity.getResources().getColor(R.color.colorPrimary));
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        if (TIME_VIEW_CHANGER == 1) {
            if (startTimeField != null)
                startTimeField.setText(AppUtils.selectedTime(hourOfDay, minute));
        }
        if (TIME_VIEW_CHANGER == 2) {
            if (endTimeField != null)
                endTimeField.setText(AppUtils.selectedTime(hourOfDay, minute));
        }
    }

}
