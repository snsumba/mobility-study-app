package com.sers.mobility.study.app.api.webserviceImpl;


import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.activities.MainActivity;
import com.sers.mobility.study.app.adapters.listAdapters.DisplayUserLocationCardAdapter;
import com.sers.mobility.study.app.adapters.listAdapters.TimeDiaryCardAdapter;
import com.sers.mobility.study.app.api.services.ApiClient;
import com.sers.mobility.study.app.api.services.AppServices;
import com.sers.mobility.study.app.constants.AppMessages;
import com.sers.mobility.study.app.constants.RecordStatus;
import com.sers.mobility.study.app.custom.NavigationController;
import com.sers.mobility.study.app.custom.SystemProgressDialog;
import com.sers.mobility.study.app.databaseUtils.QueryDatabaseUtil;
import com.sers.mobility.study.app.databaseUtils.UpdateDatabaseUtil;
import com.sers.mobility.study.app.models.databases.AppTimeDiaryCategorySuggestion;
import com.sers.mobility.study.app.models.databases.CapturedLocation;
import com.sers.mobility.study.app.models.databases.RegisteredAccount;
import com.sers.mobility.study.app.models.databases.TimeDiary;
import com.sers.mobility.study.app.models.transients.AppAccessDetails;
import com.sers.mobility.study.app.models.transients.AppTimeDiary;
import com.sers.mobility.study.app.models.transients.ListAppTimeDiary;
import com.sers.mobility.study.app.models.transients.ListServerCapturedLocation;
import com.sers.mobility.study.app.models.transients.ResponseListOfLocations;
import com.sers.mobility.study.app.models.transients.ResponseListOfTimeDiaries;
import com.sers.mobility.study.app.models.transients.ReturnedLocation;
import com.sers.mobility.study.app.models.transients.ReturnedTimeDiary;
import com.sers.mobility.study.app.models.transients.ServerCapturedLocation;
import com.sers.mobility.study.app.models.transients.ServerProfileUpdate;
import com.sers.mobility.study.app.models.transients.ServerRegisteredAccount;
import com.sers.mobility.study.app.models.transients.TimeDiaryCategorySuggestion;
import com.sers.mobility.study.app.models.transients.TimeDiaryRequest;
import com.sers.mobility.study.app.sessionManager.AccessTokenManager;
import com.sers.mobility.study.app.sessionManager.SessionManager;
import com.sers.mobility.study.app.utils.AppUtils;

import org.sers.kpa.datasource.datamanager.CustomPersistanceManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class WebService {

    private AppServices appServices = null;
    private SystemProgressDialog systemProgressDialog = null;
    private SwipeRefreshLayout swipeRefreshLayout = null;
    private RecyclerView recyclerView = null;
    private View.OnClickListener onClickListener = null;
    private FragmentManager fragmentManager = null;
    private Activity activity = null;
    private Context context = null;
    private AppAccessDetails appAccessDetails = null;
    private ServerProfileUpdate serverProfileUpdate = null;
    private ServerRegisteredAccount serverRegisteredAccount = null;
    private ServerCapturedLocation serverCapturedLocation = null;
    private TimeDiaryRequest timeDiaryRequest = null;

    private ListServerCapturedLocation listServerCapturedLocation;
    private ListAppTimeDiary listAppTimeDiary;

    public WebService() {
    }

    public WebService(SystemProgressDialog systemProgressDialog, Activity activity, ListAppTimeDiary listAppTimeDiary,
                      ListServerCapturedLocation listServerCapturedLocation) {
        this.systemProgressDialog = systemProgressDialog;
        this.activity = activity;
        this.context = activity;
        this.appServices = ApiClient.buildAPIService(this.context);
        this.listServerCapturedLocation = listServerCapturedLocation;
        this.listAppTimeDiary = listAppTimeDiary;
    }

    public WebService(Context context, SystemProgressDialog systemProgressDialog, Activity activity, AppAccessDetails appAccessDetails,
                      ServerProfileUpdate serverProfileUpdate, ServerRegisteredAccount serverRegisteredAccount,
                      ServerCapturedLocation serverCapturedLocation) {
        this.appServices = ApiClient.buildAPIService(context);
        this.systemProgressDialog = systemProgressDialog;
        this.activity = activity;
        this.context = context;
        this.appAccessDetails = appAccessDetails;
        this.serverProfileUpdate = serverProfileUpdate;
        this.serverRegisteredAccount = serverRegisteredAccount;
        this.serverCapturedLocation = serverCapturedLocation;
    }

    public WebService(Context context, SystemProgressDialog systemProgressDialog, Activity activity, AppAccessDetails appAccessDetails,
                      ServerProfileUpdate serverProfileUpdate, ServerRegisteredAccount serverRegisteredAccount,
                      ServerCapturedLocation serverCapturedLocation, TimeDiaryRequest timeDiaryRequest) {
        this.appServices = ApiClient.buildAPIService(context);
        this.systemProgressDialog = systemProgressDialog;
        this.activity = activity;
        this.context = context;
        this.appAccessDetails = appAccessDetails;
        this.serverProfileUpdate = serverProfileUpdate;
        this.serverRegisteredAccount = serverRegisteredAccount;
        this.serverCapturedLocation = serverCapturedLocation;
        this.timeDiaryRequest = timeDiaryRequest;
    }


    public void getAppLoginService() {
        systemProgressDialog.showProgressDialog("Authenticating user details, Please Wait....");
        appServices.getAppLoginService(appAccessDetails, new Callback<ServerProfileUpdate>() {
            @Override
            public void success(ServerProfileUpdate serverProfileUpdate, Response response) {

                try {
                    if (serverProfileUpdate != null) {
                        String status = AppUtils.decryptData(serverProfileUpdate.getResponseStatus());
                        if (status.equalsIgnoreCase(AppMessages.SUCCESS)) {
                            System.out.println("User : is successfully logged in");
                            System.out.println("User : is successfully logged in : " + AppUtils.decryptData(serverProfileUpdate.getUserId()));
                            systemProgressDialog.closeProgressDialog();
                            NavigationController.navigateToActivity(activity, activity.getApplicationContext(), MainActivity.class);
                            serverProfileUpdate.setIsSessionAvailable("true");
                            SessionManager sessionManager = new SessionManager(activity.getApplicationContext());
                            sessionManager.clearSessionManager();
                            sessionManager.createLoginSession(serverProfileUpdate, AppUtils.decryptData(appAccessDetails.getPassword()));
                            AccessTokenManager accessTokenManager = new AccessTokenManager(activity);
                            accessTokenManager.clearAccessTokenManager();
                            accessTokenManager.editAccessToken(AppUtils.decryptData(serverProfileUpdate.getApiToken()));
                            RegisteredAccount registeredAccount = new QueryDatabaseUtil().getRegisteredAccountFromDatabaseByUserId(String.valueOf(AppUtils.decryptData(serverProfileUpdate.getUserId())));
                            if (registeredAccount != null) {
                                System.out.println("User : is old record");
                                serverProfileUpdate.setPassword(appAccessDetails.getPassword());
                                new UpdateDatabaseUtil().saveOrUpdateRegisteredAccount(serverProfileUpdate);
                            } else {
                                System.out.println("User : is new record");
                                RegisteredAccount registeredAccount1 = new RegisteredAccount();
                                registeredAccount1.setStatus(RecordStatus.SYNCED.getStatus());
                                registeredAccount1.setDate(AppUtils.getCurrentDate());
                                registeredAccount1.setUserId(AppUtils.decryptData(serverProfileUpdate.getUserId()));
                                registeredAccount1.firstName = AppUtils.decryptData(serverProfileUpdate.getFirstName());
                                registeredAccount1.lastName = AppUtils.decryptData(serverProfileUpdate.getLastName());
                                registeredAccount1.location = AppUtils.decryptData(serverProfileUpdate.getLocation());
                                registeredAccount1.username = AppUtils.decryptData(serverProfileUpdate.getUsername());
                                registeredAccount1.phoneNumber = AppUtils.decryptData(serverProfileUpdate.getPhoneNumber());
                                registeredAccount1.password = serverProfileUpdate.getPassword();
                                new UpdateDatabaseUtil().saveOrUpdateRegisteredAccount(registeredAccount1);
                            }
                            List<RegisteredAccount> list = new QueryDatabaseUtil().getListOfRegisteredAccountsFromDatabase();
                            if (list != null) {
                                if (list.size() > 0) {
                                    for (RegisteredAccount account : list) {
                                        System.out.println("User : " + account.password + " : " + account.username + " : " + account.phoneNumber);
                                    }
                                } else System.out.println("User : is empty");
                            } else System.out.println("User : is null");

                            List<TimeDiaryCategorySuggestion> timeDiaryCategorySuggestions = serverProfileUpdate.getCategorySuggestions();
                            if (timeDiaryCategorySuggestions != null) {
                                if (timeDiaryCategorySuggestions.size() > 0) {
                                    new UpdateDatabaseUtil().emptyDatabaseTableAppTimeDiaryCategorySuggestion();
                                    for (TimeDiaryCategorySuggestion timeDiaryCategorySuggestion : timeDiaryCategorySuggestions) {
                                        System.out.println("lookUp list size  : " + timeDiaryCategorySuggestion);
                                        AppTimeDiaryCategorySuggestion appTimeDiaryCategorySuggestion = new AppTimeDiaryCategorySuggestion();
                                        appTimeDiaryCategorySuggestion.setSuggestion(AppUtils.decryptData(timeDiaryCategorySuggestion.getSuggestion()));
                                        appTimeDiaryCategorySuggestion.setSuggestionId(AppUtils.decryptData(timeDiaryCategorySuggestion.getSuggestionId()));
                                        appTimeDiaryCategorySuggestion.setStatus(RecordStatus.SYNCED.getStatus());
                                        appTimeDiaryCategorySuggestion.setDate(AppUtils.getCurrentDate());
                                        try {
                                            new CustomPersistanceManager<AppTimeDiaryCategorySuggestion>(AppTimeDiaryCategorySuggestion.class).saveOrUpdateAndReturnModel(appTimeDiaryCategorySuggestion);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                            AppUtils.showToastMessage(activity.getApplicationContext(), "Successful Login");
                        } else if (status.equalsIgnoreCase(AppMessages.FAILED)) {
                            systemProgressDialog.closeProgressDialog();
                            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, AppUtils.decryptData(serverProfileUpdate.getResponseMessage()), activity);
                        } else {
                            systemProgressDialog.closeProgressDialog();
                            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Login failed, Unknown response status", activity);
                        }
                    } else {
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "No response from the server, " +
                                "Please check your internet connection and try again", activity);
                    }

                } catch (Exception exe) {
                    exe.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                systemProgressDialog.closeProgressDialog();
                error.printStackTrace();
                AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Failed to connect to : " + error.getUrl()
                        + " , either server is unreached or URL is Invalid", activity);
            }
        });
    }

    public void registraterAccountService() {
        systemProgressDialog.showProgressDialog("Recording account details, Please Wait....");
        try {
            appServices.registerAccountService(serverRegisteredAccount, new Callback<ServerRegisteredAccount>() {
                @Override
                public void success(ServerRegisteredAccount serverRegisteredAccount, Response response) {
                    try {
                        if (serverRegisteredAccount != null) {
                            String status = AppUtils.decryptData(serverRegisteredAccount.getResponseStatus());
                            if (status.equalsIgnoreCase(AppMessages.SUCCESS)) {
                                serverRegisteredAccount.setStatus(RecordStatus.SYNCED.getStatus());
                                RegisteredAccount update = new UpdateDatabaseUtil().saveOrUpdateRegisteredAccount(serverRegisteredAccount);
                                if (update != null) {
                                    systemProgressDialog.closeProgressDialog();
                                    AppUtils.showMessage(true, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE, "Account successfully registered", activity);
                                } else {
                                    systemProgressDialog.closeProgressDialog();
                                    AppUtils.showMessage(true, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE, "Account successfully registered", activity);
                                }
                            } else if (status.equalsIgnoreCase(AppMessages.FAILED)) {
                                AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, AppUtils.decryptData(serverRegisteredAccount.getResponseMessage()), activity);
                            }
                        }
                    } catch (Exception exe) {
                        exe.printStackTrace();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    systemProgressDialog.closeProgressDialog();
                    error.printStackTrace();
                    AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Failed to connect to : " + error.getUrl()
                            + " , either server is unreached or URL is Invalid", activity);
                }
            });
        } catch (Exception e) {
            systemProgressDialog.closeProgressDialog();
            e.printStackTrace();
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, e.getMessage(), activity);
        }
    }

    public void getAppUpdateProfileService() {
        systemProgressDialog.showProgressDialog("Updating user profile, Please Wait....");
        appServices.updateProfileService(serverProfileUpdate, new Callback<ServerProfileUpdate>() {
            @Override
            public void success(ServerProfileUpdate serverProfileUpdate, Response response) {

                try {
                    if (serverProfileUpdate != null) {
                        String status = AppUtils.decryptData(serverProfileUpdate.getResponseStatus());
                        if (status.equalsIgnoreCase(AppMessages.SUCCESS)) {
                            systemProgressDialog.closeProgressDialog();
                            AppUtils.showToastMessage(activity.getApplicationContext(), "Successful profile update");
                            serverProfileUpdate.setIsSessionAvailable("true");
                            SessionManager sessionManager = new SessionManager(activity.getApplicationContext());
                            sessionManager.clearSessionManager();
                            sessionManager.createLoginSession(serverProfileUpdate, AppUtils.decryptData(serverProfileUpdate.getPassword()));
                            RegisteredAccount registeredAccount = new QueryDatabaseUtil().getRegisteredAccountFromDatabaseByUserId(AppUtils.decryptData(serverProfileUpdate.getUserId()));
                            if (registeredAccount != null) {
                                RegisteredAccount saveAccount = new UpdateDatabaseUtil().saveOrUpdateRegisteredAccount(registeredAccount);
                                if (saveAccount != null) {
                                    systemProgressDialog.closeProgressDialog();
                                    AppUtils.showMessage(true, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE, "Account successfully updated", activity);
                                } else {
                                    systemProgressDialog.closeProgressDialog();
                                    AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Failed to update account details,\n" +
                                            " Please try again", activity);
                                }
                            }
                        } else if (status.equalsIgnoreCase(AppMessages.FAILED)) {
                            systemProgressDialog.closeProgressDialog();
                            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, AppUtils.decryptData(serverProfileUpdate.getResponseMessage()), activity);
                        } else {
                            systemProgressDialog.closeProgressDialog();
                            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Profile update failed, Unknown response status", activity);
                        }
                    } else {
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "No response from the server, " +
                                "Please check your internet connection and try again", activity);
                    }

                } catch (Exception exe) {
                    exe.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                systemProgressDialog.closeProgressDialog();
                error.printStackTrace();
                AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Failed to connect to : " + error.getUrl()
                        + " , either server is unreached or URL is Invalid", activity);
            }
        });
    }

    public void saveAppListOfCapturedLocationsService() {
        appServices.appListOfCapturedLocationsService(listServerCapturedLocation, new Callback<ResponseListOfLocations>() {
            @Override
            public void success(ResponseListOfLocations responseListOfLocations, Response response) {
                try {
                    if (responseListOfLocations != null) {
                        System.out.println("***** >>> RECORD_ID 6 : ");
                        String status = AppUtils.decryptData(responseListOfLocations.getResponseStatus());
                        System.out.println("***** >>> RECORD_ID 7 : ");
                        if (status.equalsIgnoreCase(AppMessages.SUCCESS)) {
                            System.out.println("***** >>> RECORD_ID 8 : ");
                            List<ReturnedLocation> failedRecords = responseListOfLocations.getFailedRecords();
                            List<ReturnedLocation> successfulRecords = responseListOfLocations.getSuccessfulRecords();
                            System.out.println("***** >>> RECORD_ID 9 : ");
                            int failedNumbers = 0;
                            int successfulNumbers = 0;
                            System.out.println("***** >>> RECORD_ID : " + failedRecords.size());
                            System.out.println("***** >>> RECORD_ID : " + successfulRecords.size());
                            System.out.println("***** >>> RECORD_ID 10 : ");
                            if (failedRecords != null) failedNumbers = failedRecords.size();
                            if (successfulRecords != null) {
                                System.out.println("***** >>> RECORD_ID 11 : ");
                                successfulNumbers = successfulRecords.size();
                                System.out.println("***** >>> RECORD_ID : " + failedNumbers);
                                System.out.println("***** >>> RECORD_ID : " + successfulRecords);
                                System.out.println("***** >>> RECORD_ID 12 : ");
                                for (ReturnedLocation returnedLocation : successfulRecords) {
                                    String userId = AppUtils.decryptData(listServerCapturedLocation.getUserId());
                                    String recordId = AppUtils.decryptData(returnedLocation.getRecordId());
                                    System.out.println("***** >>> RECORD_ID 13 : ");
                                    System.out.println("***** >>> RECORD_ID : " + userId);
                                    System.out.println("***** >>> RECORD_ID : " + returnedLocation.getRecordId());
                                    System.out.println("***** >>> RECORD_ID : " + recordId);
                                    System.out.println("***** >>> RECORD_ID 14 : ");
                                    CapturedLocation capturedLocation = new QueryDatabaseUtil().getCapturedLocationFromDatabase(userId, recordId);
                                    if (capturedLocation != null) {
                                        System.out.println("***** >>> RECORD_ID 15 : ");
                                        System.out.println("Not Null captured location");
                                        capturedLocation.status = RecordStatus.SYNCED.getStatus();
                                        CapturedLocation update = new UpdateDatabaseUtil().saveOrUpdateCapturedLocation(capturedLocation);
                                        if (update != null) {
                                            System.out.println("Successful Update : captured location");
                                        } else {
                                            System.out.println("Failed Update : captured location");
                                        }
                                    }
                                    System.out.println("***** >>> RECORD_ID 16 : ");
                                }
                            }
                            System.out.println("***** >>> RECORD_ID 17 : ");
                            systemProgressDialog.closeProgressDialog();
                            AppUtils.showMessage(Boolean.TRUE, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE,
                                    "Successfully synced : " + successfulNumbers + " records \nFailed to Sync : " + failedNumbers + " records", activity);

                        } else if (status.equalsIgnoreCase(AppMessages.FAILED)) {
                            systemProgressDialog.closeProgressDialog();
                            AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, AppUtils.decryptData(responseListOfLocations.getResponseMessage()), activity);
                        }
                    }
                } catch (Exception exe) {
                    exe.printStackTrace();
                    systemProgressDialog.closeProgressDialog();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                systemProgressDialog.closeProgressDialog();
            }
        });

    }

    public void saveAppListOfTimeDiariesService() {
        appServices.appListOfTimeDiariesService(listAppTimeDiary, new Callback<ResponseListOfTimeDiaries>() {
            @Override
            public void success(ResponseListOfTimeDiaries responseListOfLocations, Response response) {
                try {
                    if (responseListOfLocations != null) {
                        String status = AppUtils.decryptData(responseListOfLocations.getResponseStatus());
                        if (status.equalsIgnoreCase(AppMessages.SUCCESS)) {
                            List<ReturnedTimeDiary> failedRecords = responseListOfLocations.getFailedRecords();
                            List<ReturnedTimeDiary> successfulRecords = responseListOfLocations.getSuccessfulRecords();
                            int failedNumbers = 0;
                            int successfulNumbers = 0;
                            if (failedRecords != null) failedNumbers = failedRecords.size();
                            if (successfulRecords != null) {
                                successfulNumbers = successfulRecords.size();
                                for (ReturnedTimeDiary returnedTimeDiary : successfulRecords) {
                                    String userId = AppUtils.decryptData(listAppTimeDiary.getUserId());
                                    String recordId = AppUtils.decryptData(returnedTimeDiary.getRecordId());
                                    TimeDiary timeDiary = new QueryDatabaseUtil().getTimeDiaryFromDatabase(userId, recordId);
                                    if (timeDiary != null) {
                                        System.out.println("Not Null time diary");
                                        timeDiary.status = RecordStatus.SYNCED.getStatus();
                                        TimeDiary update = new UpdateDatabaseUtil().saveOrUpdateTimeDiary(timeDiary);
                                        if (update != null) {
                                            System.out.println("Successful Update : time diary");
                                        } else {
                                            System.out.println("Failed Update : time diary");
                                        }
                                    } else {
                                        System.out.println("Null : time diary");
                                    }
                                }
                            }
                            systemProgressDialog.closeProgressDialog();
                            AppUtils.showMessage(Boolean.TRUE, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE,
                                    "Successfully synced : " + successfulNumbers + " records \nFailed to Sync : " + failedNumbers + " records", activity);

                        } else if (status.equalsIgnoreCase(AppMessages.FAILED)) {
                            systemProgressDialog.closeProgressDialog();
                            AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, AppUtils.decryptData(responseListOfLocations.getResponseMessage()), activity);
                        }
                    }
                } catch (Exception exe) {
                    exe.printStackTrace();
                    systemProgressDialog.closeProgressDialog();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                systemProgressDialog.closeProgressDialog();
            }
        });

    }

    public void getAppCapturedLocationService() {
        appServices.getAppCapturedLocationService(serverCapturedLocation, new Callback<ServerCapturedLocation>() {
            @Override
            public void success(ServerCapturedLocation serverCapturedLocation, Response response) {
                try {
                    if (serverCapturedLocation != null) {
                        String status = AppUtils.decryptData(serverCapturedLocation.getResponseStatus());
                        if (status.equalsIgnoreCase(AppMessages.SUCCESS)) {
                            String userId = AppUtils.decryptData(serverCapturedLocation.getUserId());
                            String recordId = AppUtils.decryptData(serverCapturedLocation.getRecordId());
                            CapturedLocation capturedLocation = new QueryDatabaseUtil().getCapturedLocationFromDatabase(userId, recordId);
                            if (capturedLocation != null) {
                                System.out.println("Not Null captured location");
                                capturedLocation.status = RecordStatus.SYNCED.getStatus();
                                CapturedLocation update = new UpdateDatabaseUtil().saveOrUpdateCapturedLocation(capturedLocation);
                                if (update != null) {
                                    System.out.println("Successful Update : captured location");
                                    systemProgressDialog.closeProgressDialog();
                                } else {
                                    System.out.println("Failed Update : captured location");
                                    systemProgressDialog.closeProgressDialog();
                                }
                            } else {
                                System.out.println("Null : captured location");
                                systemProgressDialog.closeProgressDialog();
                            }

                        } else if (status.equalsIgnoreCase(AppMessages.FAILED)) {
                            systemProgressDialog.closeProgressDialog();
                            AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, AppUtils.decryptData(serverCapturedLocation.getResponseMessage()), activity);
                        }
                    }
                } catch (Exception exe) {
                    exe.printStackTrace();
                    systemProgressDialog.closeProgressDialog();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                systemProgressDialog.closeProgressDialog();
            }
        });

    }
/*
    public void saveBackgroundTimeDiary() {
        try {

            appServices.saveAppTimeDiaryService(timeDiaryRequest, new Callback<AppTimeDiary>() {
                @Override
                public void success(AppTimeDiary appTimeDiary, Response response) {
                    try {
                        if (appTimeDiary != null) {
                            String status = AppUtils.decryptData(appTimeDiary.getResponseStatus());
                            if (status.equalsIgnoreCase(AppMessages.SUCCESS)) {
                                String userId = AppUtils.decryptData(appTimeDiary.getUserId());
                                String recordId = AppUtils.decryptData(appTimeDiary.getRecordId());
                                TimeDiary timeDiary = new QueryDatabaseUtil().getTimeDiaryFromDatabase(userId, recordId);
                                if (timeDiary != null) {
                                    System.out.println("Not Null time diary");
                                    timeDiary.status = RecordStatus.SYNCED.getStatus();
                                    TimeDiary update = new UpdateDatabaseUtil().saveOrUpdateTimeDiary(timeDiary);
                                    if (update != null) {
                                        System.out.println("Successful Update : time diary");
                                    } else {
                                        System.out.println("Failed Update : time diary");
                                    }
                                } else {
                                    System.out.println("Null : time diary");
                                }
                                systemProgressDialog.closeProgressDialog();

                            } else if (status.equalsIgnoreCase(AppMessages.FAILED)) {
                                systemProgressDialog.closeProgressDialog();
                                AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, AppUtils.decryptData(appTimeDiary.getResponseMessage()), activity);
                            }
                        }
                    } catch (Exception exe) {
                        exe.printStackTrace();
                        systemProgressDialog.closeProgressDialog();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    error.printStackTrace();
                    systemProgressDialog.closeProgressDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            systemProgressDialog.closeProgressDialog();
        }
    }

    public void saveTimeDiary() {
        systemProgressDialog.showProgressDialog("Recording Time Diary, Please Wait....");
        try {
            appServices.saveAppTimeDiaryService(timeDiaryRequest, new Callback<AppTimeDiary>() {
                @Override
                public void success(AppTimeDiary appTimeDiary, Response response) {
                    try {
                        if (appTimeDiary != null) {
                            if (appTimeDiary.getResponseStatus().equalsIgnoreCase(AppMessages.SUCCESS)) {
                                TimeDiary timeDiary = new QueryDatabaseUtil().getTimeDiaryFromDatabase(appTimeDiary.getUserId(), appTimeDiary.getRecordId());
                                TimeDiary update = null;
                                if (timeDiary != null) {
                                    timeDiary.setStatus(RecordStatus.SYNCED.getStatus());
                                    update = new UpdateDatabaseUtil().saveOrUpdateTimeDiary(timeDiary);
                                } else {
                                    update = new UpdateDatabaseUtil().saveOrUpdateTimeDiary(appTimeDiary);
                                }
                                if (update != null) {
                                    systemProgressDialog.closeProgressDialog();
                                    AppUtils.showMessage(true, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE, "Time diary successfully registered", activity);
                                } else {
                                    systemProgressDialog.closeProgressDialog();
                                    AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Failed to record Time diary", activity);
                                }

                            } else if (serverRegisteredAccount.getResponseStatus().equalsIgnoreCase(AppMessages.FAILED)) {
                                systemProgressDialog.closeProgressDialog();
                                AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, serverRegisteredAccount.getResponseMessage(), activity);
                            }
                        }
                    } catch (Exception exe) {
                        exe.printStackTrace();
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, exe.getMessage(), activity);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    systemProgressDialog.closeProgressDialog();
                    error.printStackTrace();
                    AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Failed to connect to : " + error.getUrl()
                            + " , either server is unreached or URL is Invalid", activity);
                }
            });
        } catch (Exception e) {
            systemProgressDialog.closeProgressDialog();
            e.printStackTrace();
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, e.getMessage(), activity);
        }
    }

    public void manuallySaveUserLocationService() {
        systemProgressDialog.showProgressDialog("Recording user location, Please Wait....");
        appServices.getAppCapturedLocationService(serverCapturedLocation, new Callback<ServerCapturedLocation>() {
            @Override
            public void success(ServerCapturedLocation serverCapturedLocation, Response response) {

                try {
                    if (serverCapturedLocation != null) {
                        if (serverCapturedLocation.getResponseStatus().equalsIgnoreCase(AppMessages.SUCCESS)) {
                            systemProgressDialog.closeProgressDialog();
                            CapturedLocation capturedLocation = new QueryDatabaseUtil().getCapturedLocationFromDatabase(serverCapturedLocation);
                            if (capturedLocation != null) {
                                capturedLocation.status = RecordStatus.SYNCED.getStatus();
                                new UpdateDatabaseUtil().saveOrUpdateCapturedLocation(capturedLocation);
                            } else {
                                CapturedLocation newCapturedLocation = new CapturedLocation(serverCapturedLocation.getUserId(), serverCapturedLocation.getPlaceName(), serverCapturedLocation.getLongitude(), serverCapturedLocation.getLatitude(), serverCapturedLocation.getAccuracy());
                                newCapturedLocation.setMethodType(serverCapturedLocation.getMethodType());
                                newCapturedLocation.status = RecordStatus.SYNCED.getStatus();
                                new UpdateDatabaseUtil().saveOrUpdateCapturedLocation(capturedLocation);
                            }
                            AppUtils.showToastMessage(activity.getApplicationContext(), "Successfully saved user location");
                        } else if (serverCapturedLocation.getResponseStatus().equalsIgnoreCase(AppMessages.FAILED)) {
                            systemProgressDialog.closeProgressDialog();
                            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, serverProfileUpdate.getResponseMessage(), activity);
                        } else {
                            systemProgressDialog.closeProgressDialog();
                            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "User location saving failed, Unknown response status", activity);
                        }
                    } else {
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "No response from the server, " +
                                "Please check your internet connection and try again", activity);
                    }

                } catch (Exception exe) {
                    systemProgressDialog.closeProgressDialog();
                    exe.printStackTrace();
                    AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, exe.getMessage(), activity);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                systemProgressDialog.closeProgressDialog();
                error.printStackTrace();
                AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Failed to connect to : " + error.getUrl()
                        + " , either server is unreached or URL is Invalid", activity);
            }
        });
    }

    public void loadCapturedLocationsFromServerService() {
        systemProgressDialog.showProgressDialog("Downloading visited places, Please Wait....");
        appServices.loadCapturedLocationsFromServerService(appAccessDetails, new Callback<List<CapturedLocation>>() {
            @Override
            public void success(List<CapturedLocation> capturedLocations, Response response) {
                if (capturedLocations != null) {
                    if (capturedLocations.size() > 0) {
                        for (CapturedLocation location : capturedLocations) {
                            CapturedLocation check = new QueryDatabaseUtil().getCapturedLocationFromDatabaseByRecordId(location.recordId);
                            if (check != null) {
                                check.setStatus(RecordStatus.SYNCED.getStatus());
                                new UpdateDatabaseUtil().saveOrUpdateCapturedLocation(check);
                            } else {
                                new UpdateDatabaseUtil().saveOrUpdateCapturedLocation(location);
                            }
                        }
                    }
                }
                SessionManager sessionManager = new SessionManager(context);
                String userId = "";
                if (sessionManager != null) {
                    HashMap<String, String> user = sessionManager.getUserDetails();
                    userId = (user.get(SessionManager.KEY_USER_ID) != null ? user.get(SessionManager.KEY_USER_ID) : "n.a");
                }
                List<CapturedLocation> dbList = new QueryDatabaseUtil().getListOfCapturedLocationsFromDatabase(userId);
                if (dbList == null) {
                    recyclerView.setAdapter(new DisplayUserLocationCardAdapter(R.layout.card_row_layout, systemProgressDialog, fragmentManager, activity, capturedLocations, onClickListener));
                    systemProgressDialog.closeProgressDialog();
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    recyclerView.setAdapter(new DisplayUserLocationCardAdapter(R.layout.card_row_layout, systemProgressDialog, fragmentManager, activity, dbList, onClickListener));
                    systemProgressDialog.closeProgressDialog();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                systemProgressDialog.closeProgressDialog();
                error.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void loadTimeDiariesFromServerService() {
        systemProgressDialog.showProgressDialog("Downloading time diaries, Please Wait....");
        appServices.loadTimeDiariesFromServerService(appAccessDetails, new Callback<List<AppTimeDiary>>() {
            @Override
            public void success(List<AppTimeDiary> appTimeDiaries, Response response) {
                List<TimeDiary> dbList = new ArrayList<TimeDiary>();
                if (appTimeDiaries != null) {
                    if (appTimeDiaries.size() > 0) {

                        new UpdateDatabaseUtil().emptyDatabaseTableAppTimeDiary();
                        for (AppTimeDiary appTimeDiary : appTimeDiaries) {
                            TimeDiary check = new QueryDatabaseUtil().getTimeDiaryFromDatabaseByRecordId(appTimeDiary.getRecordId());
                            if (check != null) {
                                check.setStatus(RecordStatus.SYNCED.getStatus());
                                new UpdateDatabaseUtil().saveOrUpdateTimeDiary(appTimeDiary);
                            } else {
                                new UpdateDatabaseUtil().saveOrUpdateTimeDiary(appTimeDiary);
                            }

                        }
                    }
                    SessionManager sessionManager = new SessionManager(context);
                    String userId = "";
                    if (sessionManager != null) {
                        HashMap<String, String> user = sessionManager.getUserDetails();
                        userId = (user.get(SessionManager.KEY_USER_ID) != null ? user.get(SessionManager.KEY_USER_ID) : "n.a");
                    }
                    dbList = new QueryDatabaseUtil().getListOfTimeDiariesFromDatabase(userId);
                }
                recyclerView.setAdapter(new TimeDiaryCardAdapter(R.layout.card_row_time_diary_layout, systemProgressDialog, fragmentManager, activity, dbList, onClickListener, recyclerView));
                systemProgressDialog.closeProgressDialog();
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void failure(RetrofitError error) {
                systemProgressDialog.closeProgressDialog();
                error.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
*/

}
