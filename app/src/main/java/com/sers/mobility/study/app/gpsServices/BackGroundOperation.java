package com.sers.mobility.study.app.gpsServices;


import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import com.sers.mobility.study.app.constants.MethodType;
import com.sers.mobility.study.app.constants.RecordStatus;
import com.sers.mobility.study.app.databaseUtils.UpdateDatabaseUtil;
import com.sers.mobility.study.app.models.databases.CapturedLocation;
import com.sers.mobility.study.app.sessionManager.AppTypeManager;
import com.sers.mobility.study.app.sessionManager.SessionManager;
import com.sers.mobility.study.app.utils.AppUtils;
import com.sers.mobility.study.app.utils.LocationUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class BackGroundOperation {

    private Context context = null;
    private LocationUtils locationUtils = null;
    private double latitude;
    private double longitude;
    private double accuracy;
    private String locationName;
    private CapturedLocation capturedLocation;

    public BackGroundOperation() {
    }

    public BackGroundOperation(Context context) {
        this.context = context;
        this.locationUtils = new LocationUtils(this.context);
    }

    public void lookUpForNewCoordinates() {
        System.out.println("lookUp running ....");
        GPSTracker gpsTracker = new GPSTracker(this.context);
        SessionManager sessionManager = new SessionManager(this.context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        String userId = user.get(SessionManager.KEY_USER_ID);

        AppTypeManager appTypeManager = new AppTypeManager(this.context);
        HashMap<String, String> appType = appTypeManager.getAppTypeSettings();
        String methodType = appType.get(AppTypeManager.KEY_DESIRED_METHOD);

        if (user != null) {
            if (methodType != null) {
                if (methodType.equalsIgnoreCase(MethodType.GPS_DETERMINED.getName())) {
                    if (this.locationUtils != null) {
                        if (this.locationUtils.isGPSEnabled()) {
                            try {
                                if (gpsTracker.canGetLocation()) {
                                    Location location = null;
                                    try {
                                        location = gpsTracker.getLocation();
                                        setLocationCoordinates(location);
                                        logTrace(userId);
                                        capturedLocation = new CapturedLocation(String.valueOf(userId), String.valueOf(locationName), String.valueOf(longitude), String.valueOf(latitude), String.valueOf(accuracy));
                                        capturedLocation.setStatus(RecordStatus.UN_SYNCED.getStatus());
                                        capturedLocation.setDate(AppUtils.getCurrentDate());
                                        capturedLocation.setRecordId(AppUtils.generateUserId());
                                        capturedLocation.setMethodType(MethodType.GPS_DETERMINED.getName());
                                        new UpdateDatabaseUtil().saveOrUpdateCapturedLocation(capturedLocation);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else System.out.println("lookUp GPS is OFF");
                    } else System.out.println("lookUp GPS is OFF");
                }
            }
        }

    }

    private void setLocationCoordinates(Location location) {
        try {
            if (location != null) {
                this.latitude = location.getLatitude();
                this.longitude = location.getLongitude();
                this.accuracy = location.getAccuracy();

                Geocoder geocoder = new Geocoder(this.context, Locale.getDefault());
                List<Address> listAddresses = geocoder.getFromLocation(this.latitude, this.longitude, 1);
                if (null != listAddresses && listAddresses.size() > 0) {
                    this.locationName = (listAddresses.get(0).getAddressLine(0) != null ? listAddresses.get(0).getAddressLine(0) : "Not found");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void logTrace(String userId) {
        System.out.println("lookUp userId        : " + userId);
        System.out.println("lookUp latitude      : " + latitude);
        System.out.println("lookUp longitude     : " + longitude);
        System.out.println("lookUp accuracy      : " + accuracy);
        System.out.println("lookUp locationName  : " + locationName);
    }
}
