package com.sers.mobility.study.app.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.baseFiles.BaseMainFragment;
import com.sers.mobility.study.app.constants.AppToolBarTitles;
import com.sers.mobility.study.app.custom.NavigationController;
import com.sers.mobility.study.app.fragments.optionsMenu.AboutUsFragment;

public class AboutUsActivity extends AppCompatActivity implements BaseMainFragment.OnFragmentInteractionListener {

    private Toolbar toolbar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(AppToolBarTitles.ABOUT_US.getName());

        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new AboutUsFragment()).commit();

    }

    @Override
    public void onBackPressed() {
        NavigationController.navigateToActivity(this, getApplicationContext(), MainActivity.class);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * Is called when the user receives an event like a call or a text message,
     * when onPause() is called the Activity may be partially or completely hidden.
     * You would want to save user data in onPause, in case he hits back button
     * without saving the data explicitly
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Is called when the user resumes his Activity which he left a while ago,
     * say he presses home button and then comes back to app, onResume() is called.
     * You can do the network related updates here or anything of this sort in onResume.
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Is called when the Activity is being destroyed either by the system, or by the user,
     * say by hitting back, until the app exits.
     * Its compulsory that you save any user data that you want to persist in onDestroy(),
     * because the system will not do it for you.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
