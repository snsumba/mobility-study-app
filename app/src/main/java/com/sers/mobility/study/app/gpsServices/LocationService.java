package com.sers.mobility.study.app.gpsServices;

/**
 * Created by kdeo on 4/14/2017.
 */

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

/**
 * http://stackoverflow.com/questions/2618182/how-to-play-ringtone-alarm-sound-in-android
 */
public class LocationService extends Service {
    private final IBinder mBinder = new MyBinder();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new BackGroundOperation(getApplicationContext()).lookUpForNewCoordinates();
        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }

    public class MyBinder extends Binder {
        public LocationService getService() {
            return LocationService.this;
        }
    }

}
