package com.sers.mobility.study.app.adapters.listAdapters;


import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.custom.SystemProgressDialog;
import com.sers.mobility.study.app.models.databases.CapturedLocation;

import java.util.List;

public class DisplayUserLocationCardAdapter extends RecyclerView.Adapter<DisplayUserLocationCardAdapter.ViewHolder> {

    private List<CapturedLocation> listOfRecords = null;
    public Context context;
    public ColorGenerator generator = ColorGenerator.MATERIAL;
    public int rowLayout;
    public SystemProgressDialog systemProgressDialog;
    public FragmentManager fragmentManager;
    public Activity activity;
    public View.OnClickListener onClickListener = null;

    public DisplayUserLocationCardAdapter(int rowLayout, SystemProgressDialog systemProgressDialog, FragmentManager fragmentManager, Activity activity,
                                          List<CapturedLocation> listOfRecords, View.OnClickListener onClickListener) {
        this.rowLayout = rowLayout;
        this.context = activity.getApplication().getApplicationContext();
        this.activity = activity;
        this.fragmentManager = fragmentManager;
        this.systemProgressDialog = systemProgressDialog;
        this.onClickListener = onClickListener;
        this.listOfRecords = listOfRecords;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbnail;
        TextView recordId, dateCardElement, leftCardElement1, leftCardElement2, leftCardElement3, leftCardElement4, rightCardElement1, statusTextView;

        public ViewHolder(View view) {
            super(view);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            recordId = (TextView) view.findViewById(R.id.recordId);
            leftCardElement1 = (TextView) view.findViewById(R.id.leftCardElement1);
            leftCardElement2 = (TextView) view.findViewById(R.id.leftCardElement2);
            leftCardElement3 = (TextView) view.findViewById(R.id.leftCardElement3);
            leftCardElement4 = (TextView) view.findViewById(R.id.leftCardElement4);
            rightCardElement1 = (TextView) view.findViewById(R.id.rightCardElement1);
            dateCardElement = (TextView) view.findViewById(R.id.dateCardElement);
            statusTextView = (TextView) view.findViewById(R.id.statusTextView);
        }
    }

    @Override
    public DisplayUserLocationCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new DisplayUserLocationCardAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (listOfRecords != null) {
            if (listOfRecords.size() == 0) holder.leftCardElement2.setText(AppConstants.NO_RECORDS);
            else {
                String recordId = listOfRecords.get(position).recordId;
                String placeName = (String.valueOf(listOfRecords.get(position).placeName) != null ? String.valueOf(listOfRecords.get(position).placeName) : "Not Found");
                TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(placeName.charAt(0)).toUpperCase(), generator.getRandomColor());
                holder.recordId.setText(recordId);
                holder.thumbnail.setImageDrawable(drawable);
                holder.leftCardElement1.setText(listOfRecords.get(position).placeName);
                holder.leftCardElement2.setText(listOfRecords.get(position).latitude);
                holder.leftCardElement3.setText(listOfRecords.get(position).longitude);
                holder.leftCardElement4.setText(listOfRecords.get(position).accuracy);
                holder.rightCardElement1.setText(String.valueOf(listOfRecords.get(position).date));
                holder.rightCardElement1.setVisibility(View.GONE);
                holder.dateCardElement.setText(String.valueOf(listOfRecords.get(position).date));
                holder.statusTextView.setText(listOfRecords.get(position).status);
                if (onClickListener != null)
                    holder.itemView.setOnClickListener(onClickListener);
            }
        } else holder.leftCardElement2.setText(AppConstants.NO_RECORDS);
    }

    @Override
    public int getItemCount() {
        return listOfRecords.size();
    }
}
