package com.sers.mobility.study.app.fragments.mainMenu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.melnykov.fab.FloatingActionButton;
import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.adapters.listAdapters.TimeDiaryCardAdapter;
import com.sers.mobility.study.app.api.webserviceImpl.WebService;
import com.sers.mobility.study.app.baseFiles.BaseReportFragment;
import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.constants.AppMessages;
import com.sers.mobility.study.app.constants.RecordStatus;
import com.sers.mobility.study.app.custom.AppMaterialDialog;
import com.sers.mobility.study.app.custom.SystemProgressDialog;
import com.sers.mobility.study.app.databaseUtils.QueryDatabaseUtil;
import com.sers.mobility.study.app.databaseUtils.UpdateDatabaseUtil;
import com.sers.mobility.study.app.models.databases.AppTimeDiaryCategorySuggestion;
import com.sers.mobility.study.app.models.databases.TimeDiary;
import com.sers.mobility.study.app.models.transients.ListAppTimeDiary;
import com.sers.mobility.study.app.models.transients.TimeDiaryRequest;
import com.sers.mobility.study.app.sessionManager.AccessTokenManager;
import com.sers.mobility.study.app.sessionManager.SessionManager;
import com.sers.mobility.study.app.utils.AppUtils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


public class TimeDiaryFragment extends BaseReportFragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private List<TimeDiary> timeDiaries = new ArrayList<TimeDiary>();
    public int VIEW_CHANGER = 0;
    public int TIME_VIEW_CHANGER = 0;
    private List<AppTimeDiaryCategorySuggestion> selectedCategorySuggestion = new ArrayList<AppTimeDiaryCategorySuggestion>();
    private LinearLayout leftSuggestionDisplay, rightSuggestionDisplay;
    private EditText startDateField, startTimeField, endTimeField;
    private AppCompatButton syncTimeDiaryButton;

    public TimeDiaryFragment() {
        super();
    }

    @SuppressLint("ValidFragment")
    public TimeDiaryFragment(Activity activity) {
        super(activity);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_time_diary, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.listRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplication().getApplicationContext()));
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.listSwipeView);
        swipeRefreshLayout.setOnRefreshListener(this);
        systemProgressDialog = new SystemProgressDialog(getActivity());

        syncTimeDiaryButton = (AppCompatButton) rootView.findViewById(R.id.syncTimeDiary);
        syncTimeDiaryButton.setOnClickListener(this);

        sessionManager = new SessionManager(getActivity());
        if (sessionManager != null) {
            HashMap<String, String> user = sessionManager.getUserDetails();
            userId = (user.get(SessionManager.KEY_USER_ID) != null ? user.get(SessionManager.KEY_USER_ID) : "n.a");
        }
        loadContent();
        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSelectedTimeCategory();
            }
        });

        return rootView;
    }


    @Override
    public void getContent(boolean isRefreshing) {
        try {
            if (isRefreshing) {
                swipeRefreshLayout.setRefreshing(false);
                List<TimeDiary> db = new QueryDatabaseUtil().getListOfTimeDiariesFromDatabase(userId);
                if (db != null) {
                    if (db.size() > 0) {
                        systemProgressDialog.showProgressDialog("Synchronizing Data, Please Wait....");
                        timeDiaries = db;
                        recyclerView.setAdapter(new TimeDiaryCardAdapter(R.layout.card_row_time_diary_layout, systemProgressDialog,
                                getActivity().getFragmentManager(), getActivity(), timeDiaries, this, recyclerView));
                        systemProgressDialog.closeProgressDialog();
                    }
                }
            } else {
                List<TimeDiary> db = new QueryDatabaseUtil().getListOfTimeDiariesFromDatabase(userId);
                if (db != null) {
                    if (db.size() > 0) {
                        systemProgressDialog.showProgressDialog("Synchronizing Data, Please Wait....");
                        timeDiaries = db;
                        recyclerView.setAdapter(new TimeDiaryCardAdapter(R.layout.card_row_time_diary_layout, systemProgressDialog,
                                getActivity().getFragmentManager(), getActivity(), timeDiaries, this, recyclerView));
                        systemProgressDialog.closeProgressDialog();
                    }
                } else this.getContent(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.syncTimeDiary:
                syncTimeDiary();
                break;
        }
    }

    private void syncTimeDiary() {
        try {
            if (new AppUtils().isNetworkAvailable(getActivity())) {
                systemProgressDialog.showProgressDialog("Synchronizing Time Diary Data, Please Wait....");
                List<TimeDiary> unSyncedTimeDiaries = new QueryDatabaseUtil().getListOfTimeDiariesFromDatabase(userId, RecordStatus.UN_SYNCED.getStatus());
                if (unSyncedTimeDiaries != null) {
                    if (unSyncedTimeDiaries.size() > 0) {
                        List<TimeDiaryRequest> appTimeDiaries = new ArrayList<TimeDiaryRequest>();
                        for (TimeDiary timeDiary : unSyncedTimeDiaries) {
                            if (timeDiary != null) {
                                TimeDiaryRequest timeDiaryRequest = new TimeDiaryRequest();
                                timeDiaryRequest.setDate(AppUtils.encryptData(timeDiary.getDate()));
                                timeDiaryRequest.setStartTime(AppUtils.encryptData(timeDiary.getStartTime()));
                                timeDiaryRequest.setEndTime(AppUtils.encryptData(timeDiary.getEndTime()));
                                timeDiaryRequest.setUserId(AppUtils.encryptData(userId));
                                timeDiaryRequest.setRecordId(AppUtils.encryptData(timeDiary.recordId));
                                timeDiaryRequest.setTimeCategorySuggestionId(AppUtils.encryptData(timeDiary.getSuggestionId()));
                                appTimeDiaries.add(timeDiaryRequest);
                            }
                        }
                        String accessToken = new AccessTokenManager(getActivity()).getAccessToken();
                        ListAppTimeDiary listAppTimeDiary = new ListAppTimeDiary(AppUtils.encryptData(accessToken), AppUtils.encryptData(userId), appTimeDiaries);
                        new WebService(systemProgressDialog, getActivity(), listAppTimeDiary, null).saveAppListOfTimeDiariesService();

                    } else {
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessageWithOutButton(true, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE, "No time diaries to Sync", getActivity());
                    }
                }
            } else {
                new AppUtils().noInternet(getActivity(), "Time diary Data Sync");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getSelectedTimeCategory() {
        final MaterialDialog applicationDomainDialog = new AppMaterialDialog().appMaterialDialog(R.layout.pop_up_time_diary,
                getActivity(), AppConstants.TIME_DIARY_TITLE, R.string.default_text, R.string.negtive_dialog_text,
                R.string.save_text, Boolean.FALSE);

        View dialogView = applicationDomainDialog.getView();
        startDateField = (EditText) dialogView.findViewById(R.id.dateField);
        leftSuggestionDisplay = (LinearLayout) dialogView.findViewById(R.id.leftSuggestionDisplay);
        rightSuggestionDisplay = (LinearLayout) dialogView.findViewById(R.id.rightSuggestionDisplay);
        startDateField.setText(AppUtils.getCurrentDateOnly());

        startTimeField = (EditText) dialogView.findViewById(R.id.startTimeField);
        startTimeField.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    TIME_VIEW_CHANGER = 1;
                    showTimePicker();
                }
                return false;
            }
        });

        endTimeField = (EditText) dialogView.findViewById(R.id.endTimeField);
        endTimeField.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    TIME_VIEW_CHANGER = 2;
                    showTimePicker();
                }
                return false;
            }
        });
        initializeStartAndEndDatesPickers();

        List<AppTimeDiaryCategorySuggestion> dbList = new QueryDatabaseUtil().getListOfAppTimeDiaryCategorySuggestionsFromDatabase();
        int listSize = (dbList != null ? dbList.size() : 0);
        List<AppTimeDiaryCategorySuggestion> leftListItems = dbList.subList(0, listSize / 2);
        List<AppTimeDiaryCategorySuggestion> rightListItems = dbList.subList(listSize / 2, (listSize));

        for (AppTimeDiaryCategorySuggestion categorySuggestion : leftListItems) {
            final CheckBox checkBox = new CheckBox(getActivity());
            setLayoutCheckBoxList(checkBox, categorySuggestion);
            leftSuggestionDisplay.addView(checkBox);
        }

        for (AppTimeDiaryCategorySuggestion categorySuggestion : rightListItems) {
            final CheckBox checkBox = new CheckBox(getActivity());
            setLayoutCheckBoxList(checkBox, categorySuggestion);
            rightSuggestionDisplay.addView(checkBox);
        }

        final MDButton save = (MDButton) applicationDomainDialog.getActionButton(DialogAction.POSITIVE);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userId = "";
                SessionManager sessionManager = new SessionManager(getActivity());
                if (sessionManager != null) {
                    HashMap<String, String> user = sessionManager.getUserDetails();
                    userId = (user.get(SessionManager.KEY_USER_ID) != null ? user.get(SessionManager.KEY_USER_ID) : "n.a");
                }
                if (String.valueOf(startDateField.getText()).isEmpty() || String.valueOf(startDateField.getText()) == null) {
                    AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide a Date and try again or Close", getActivity());
                } else if (String.valueOf(startTimeField.getText()).isEmpty() || String.valueOf(startTimeField.getText()) == null) {
                    AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide a Start time and try again or Close", getActivity());
                } else if (String.valueOf(endTimeField.getText()).isEmpty() || String.valueOf(endTimeField.getText()) == null) {
                    AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide an End time and try again or Close", getActivity());
                } else if (selectedCategorySuggestion == null || selectedCategorySuggestion.size() == 0) {
                    AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Please select an Activity", getActivity());
                } else {
                    startDate = ((startDateField.getText() != null ? String.valueOf(startDateField.getText()) : AppUtils.getCurrentDateOnly()));
                    String startTime = String.valueOf(startTimeField.getText());
                    String endTime = String.valueOf(endTimeField.getText());

                    for (AppTimeDiaryCategorySuggestion categorySuggestion : selectedCategorySuggestion) {
                        TimeDiary timeDiary = new TimeDiary();
                        timeDiary.setDate(startDate);
                        timeDiary.setStartTime(startTime);
                        timeDiary.setEndTime(endTime);
                        timeDiary.setOrderingParameter(startDate + " " + endTime);
                        timeDiary.setUserId(userId);
                        timeDiary.setStatus(RecordStatus.UN_SYNCED.getStatus());
                        timeDiary.setRecordId(AppUtils.generateUserId());
                        timeDiary.setSuggestionId((categorySuggestion != null ? categorySuggestion.getSuggestionId() : ""));
                        timeDiary.setSuggestion(categorySuggestion.suggestion);
                        new UpdateDatabaseUtil().saveOrUpdateTimeDiary(timeDiary);
                    }
                    //AppUtils.showMessageWithOutButton(true, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE, "Successfully recorded Time diary", getActivity());
                    applicationDomainDialog.hide();
                    selectedCategorySuggestion = new ArrayList<AppTimeDiaryCategorySuggestion>();

                    recyclerView.setAdapter(new TimeDiaryCardAdapter(R.layout.card_row_time_diary_layout,
                            systemProgressDialog, getActivity().getFragmentManager(), getActivity(),
                            new QueryDatabaseUtil().getListOfTimeDiariesFromDatabase(userId), this, recyclerView));
                }
            }
        });
        applicationDomainDialog.show();
    }


    private void setLayoutCheckBoxList(CheckBox checkBox, AppTimeDiaryCategorySuggestion categorySuggestion) {
        final AppTimeDiaryCategorySuggestion suggestion = new AppTimeDiaryCategorySuggestion(categorySuggestion.pk, categorySuggestion.suggestion, categorySuggestion.suggestionId);
        checkBox.setId(suggestion.pk);
        checkBox.setText(suggestion.suggestion);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) selectedCategorySuggestion.add(suggestion);
                else selectedCategorySuggestion.remove(suggestion);
            }
        });
    }


    public void initializeStartAndEndDatesPickers() {
        if (startDateField != null)
            startDateField.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == 0) {
                        VIEW_CHANGER = 1;
                        showDatePicker(TimeDiaryFragment.this);
                    }
                    return false;
                }
            });
    }

    public void showDatePicker(DatePickerDialog.OnDateSetListener onDateSetListener) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                onDateSetListener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getActivity().getFragmentManager(), "Select Date");
        dpd.setAccentColor(getResources().getColor(R.color.colorPrimary));
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        if (VIEW_CHANGER == 1) {
            int monthday_start = monthOfYear + 1;
            if (startDateField != null)
                startDateField.setText(year + "/" + monthday_start + "/" + dayOfMonth);
        }
    }


    public void showTimePicker() {
        Calendar calendar = Calendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        boolean is24HourMode = DateFormat.is24HourFormat(getActivity());
        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(TimeDiaryFragment.this,
                hourOfDay, minute, is24HourMode);
        timePickerDialog.show(getActivity().getFragmentManager(), "Select Time");
        timePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimary));
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        if (TIME_VIEW_CHANGER == 1) {
            if (startTimeField != null)
                startTimeField.setText(AppUtils.selectedTime(hourOfDay, minute));
        }
        if (TIME_VIEW_CHANGER == 2) {
            if (endTimeField != null)
                endTimeField.setText(AppUtils.selectedTime(hourOfDay, minute));
        }
    }
}
