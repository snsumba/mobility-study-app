package com.sers.mobility.study.app.api.services;


import com.sers.mobility.study.app.models.databases.CapturedLocation;
import com.sers.mobility.study.app.models.transients.AppAccessDetails;
import com.sers.mobility.study.app.models.transients.AppTimeDiary;
import com.sers.mobility.study.app.models.transients.ListAppTimeDiary;
import com.sers.mobility.study.app.models.transients.ListServerCapturedLocation;
import com.sers.mobility.study.app.models.transients.ResponseListOfLocations;
import com.sers.mobility.study.app.models.transients.ResponseListOfTimeDiaries;
import com.sers.mobility.study.app.models.transients.ServerCapturedLocation;
import com.sers.mobility.study.app.models.transients.ServerProfileUpdate;
import com.sers.mobility.study.app.models.transients.ServerRegisteredAccount;
import com.sers.mobility.study.app.models.transients.TimeDiaryCategory;
import com.sers.mobility.study.app.models.transients.TimeDiaryCategorySuggestion;
import com.sers.mobility.study.app.models.transients.TimeDiaryRequest;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

public interface AppServices {

    @POST("/api/AppLoginService")
    void getAppLoginService(@Body AppAccessDetails appAccessDetails, Callback<ServerProfileUpdate> cb);

    @POST("/api/RecordAppTimeDiaryService")
    void saveAppTimeDiaryService(@Body TimeDiaryRequest appTimeDiary, Callback<AppTimeDiary> cb);

    @POST("/api/AppUpdateProfileService")
    void updateProfileService(@Body ServerProfileUpdate profileUpdate, Callback<ServerProfileUpdate> cb);

    @POST("/api/AppRegisterAccountService")
    void registerAccountService(@Body ServerRegisteredAccount registeredAccount, Callback<ServerRegisteredAccount> cb);

    @POST("/api/AppCapturedLocationService")
    void getAppCapturedLocationService(@Body ServerCapturedLocation capturedLocation, Callback<ServerCapturedLocation> cb);

    @POST("/api/LoadAppCapturedLocationsService")
    void loadCapturedLocationsFromServerService(@Body AppAccessDetails appAccessDetails, Callback<List<CapturedLocation>> cb);

    @POST("/api/LoadAppTimeDiariesService")
    void loadTimeDiariesFromServerService(@Body AppAccessDetails appAccessDetails, Callback<List<AppTimeDiary>> cb);

    @POST("/api/LoadAppTimeDiaryCategoriesService")
    void LoadAppTimeDiaryCategoriesService(@Body AppAccessDetails appAccessDetails, Callback<List<TimeDiaryCategory>> cb);

    @POST("/api/LoadAppTimeDiaryCategorySuggestionsService")
    void LoadAppTimeDiaryCategorySuggestionsService(@Body AppAccessDetails appAccessDetails, Callback<List<TimeDiaryCategorySuggestion>> cb);


    @POST("/api/RecordAppListOfCapturedLocationsService")
    void appListOfCapturedLocationsService(@Body ListServerCapturedLocation listServerCapturedLocation, Callback<ResponseListOfLocations> cb);

    @POST("/api/RecordAppListOfTimeDiariesService")
    void appListOfTimeDiariesService(@Body ListAppTimeDiary listAppTimeDiary, Callback<ResponseListOfTimeDiaries> cb);


}
