package com.sers.mobility.study.app.databaseUtils;


import com.sers.mobility.study.app.constants.RecordStatus;
import com.sers.mobility.study.app.models.databases.AppBaseEntity;
import com.sers.mobility.study.app.models.databases.AppTimeDiaryCategory;
import com.sers.mobility.study.app.models.databases.AppTimeDiaryCategorySuggestion;
import com.sers.mobility.study.app.models.databases.CapturedLocation;
import com.sers.mobility.study.app.models.databases.RegisteredAccount;
import com.sers.mobility.study.app.models.databases.TimeDiary;
import com.sers.mobility.study.app.models.transients.AppTimeDiary;
import com.sers.mobility.study.app.models.transients.ServerProfileUpdate;
import com.sers.mobility.study.app.models.transients.ServerRegisteredAccount;
import com.sers.mobility.study.app.utils.AppUtils;

import org.sers.kpa.datasource.datamanager.CustomPersistanceManager;

import java.io.Serializable;
import java.util.Date;

public class UpdateDatabaseUtil implements Serializable {

    public void emptyDatabaseTableAppTimeDiaryCategory() {
        try {
            new CustomPersistanceManager<AppTimeDiaryCategory>(AppTimeDiaryCategory.class).emptyTable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearSyncedTimeDiaries() {
        try {
            new ExtendedPersistanceManager<TimeDiary>(TimeDiary.class).clearSyncedRecords();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearSyncedCapturedLocations() {
        try {
            new ExtendedPersistanceManager<CapturedLocation>(CapturedLocation.class).clearSyncedRecords();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void emptyDatabaseTableAppTimeDiaryCategorySuggestion() {
        try {
            new CustomPersistanceManager<AppTimeDiaryCategorySuggestion>(AppTimeDiaryCategorySuggestion.class).emptyTable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void emptyDatabaseTableAppTimeDiary() {
        try {
            new CustomPersistanceManager<TimeDiary>(TimeDiary.class).emptyTable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CapturedLocation saveOrUpdateCapturedLocation(CapturedLocation capturedLocation) {
        System.out.println("Saved is called ...");
        try {
            CapturedLocation saved = new CustomPersistanceManager<CapturedLocation>(CapturedLocation.class).saveOrUpdateAndReturnModel(capturedLocation);
            if (saved != null) {
                System.out.println("Saved : " + saved);
            } else System.out.println("Saved is null ");

            return saved;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public RegisteredAccount saveOrUpdateRegisteredAccount(RegisteredAccount registeredAccount) {
        try {
            return new CustomPersistanceManager<RegisteredAccount>(RegisteredAccount.class).saveOrUpdateAndReturnModel(registeredAccount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public RegisteredAccount saveOrUpdateRegisteredAccount(ServerRegisteredAccount serverRegisteredAccount) {
        try {
            RegisteredAccount registeredAccount = new RegisteredAccount(serverRegisteredAccount.getFirstName(), serverRegisteredAccount.getLastName(), serverRegisteredAccount.getGender(), serverRegisteredAccount.getLocation(),
                    serverRegisteredAccount.getPhoneNumber(), serverRegisteredAccount.getUsername(), serverRegisteredAccount.getPassword(), new Date());
            registeredAccount.status = serverRegisteredAccount.getStatus();
            registeredAccount.date = AppUtils.getCurrentDate();
            registeredAccount.userId = AppUtils.generateUserId();
            return new CustomPersistanceManager<RegisteredAccount>(RegisteredAccount.class).saveOrUpdateAndReturnModel(registeredAccount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public RegisteredAccount saveOrUpdateRegisteredAccount(ServerProfileUpdate serverProfileUpdate) {
        try {
            RegisteredAccount registeredAccount = new RegisteredAccount(serverProfileUpdate.getFirstName(), serverProfileUpdate.getLastName(), serverProfileUpdate.getGender(), serverProfileUpdate.getLocation(),
                    serverProfileUpdate.getPhoneNumber(), serverProfileUpdate.getUsername(), serverProfileUpdate.getPassword(), new Date(serverProfileUpdate.getDateOfBirth()));
            registeredAccount.status = RecordStatus.SYNCED.getStatus();
            registeredAccount.date = AppUtils.getCurrentDate();
            registeredAccount.userId = AppUtils.generateUserId();
            return new CustomPersistanceManager<RegisteredAccount>(RegisteredAccount.class).saveOrUpdateAndReturnModel(registeredAccount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public TimeDiary saveOrUpdateTimeDiary(AppTimeDiary appTimeDiary) {
        try {
            TimeDiary timeDiary = new TimeDiary(appTimeDiary.getDescription());
            timeDiary.setStatus(RecordStatus.SYNCED.getStatus());
            timeDiary.setDate(AppUtils.getCurrentDate());
            timeDiary.setUserId(appTimeDiary.getUserId());

            return new CustomPersistanceManager<TimeDiary>(TimeDiary.class).saveOrUpdateAndReturnModel(timeDiary);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public TimeDiary saveOrUpdateTimeDiary(TimeDiary timeDiary) {
        try {
            return new CustomPersistanceManager<TimeDiary>(TimeDiary.class).saveOrUpdateAndReturnModel(timeDiary);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
