package com.sers.mobility.study.app.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormatter {

	public static String doubleNumberFormatter(String number) {
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(true);
		return numberFormat.format(new Double(number)).toString();
	}
	
	public static String decimalNumberFormatter(String number){
		DecimalFormat decimalFormat = new DecimalFormat("#.0");
		decimalFormat.setGroupingUsed(true);
		decimalFormat.setGroupingSize(3);
		return decimalFormat.format(new Double(number)).toString();
	}

	public static String integerNumberFormatter(String number){
		DecimalFormat decimalFormat = new DecimalFormat("#");
		decimalFormat.setGroupingUsed(true);
		decimalFormat.setGroupingSize(3);
		return decimalFormat.format(new Double(number)).toString();
	}

	
	public static String germanNumberFormatter(String number) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.GERMAN);
		DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
		decimalFormat.applyPattern("#.0");
		decimalFormat.setGroupingUsed(true);
		decimalFormat.setGroupingSize(3);
		return decimalFormat.format(new Double(number)).toString();
	}
	
	public static String usaNumberFormatter(String number) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
		decimalFormat.applyPattern("#.0");
		decimalFormat.setGroupingUsed(true);
		decimalFormat.setGroupingSize(3);
		return decimalFormat.format(new Double(number)).toString();
	}
	public static void main(String[] args) {
		String[] numbers = { "11220.00", "232323232.24", "121211.55","102.121212" };
		
		for (String d : numbers) {
			System.out.println(doubleNumberFormatter(d));
		}
		System.out.println();

		for (String d : numbers) {
			System.out.println(decimalNumberFormatter(d));
		}
		System.out.println();
		
		for (String d : numbers) {
			System.out.println(germanNumberFormatter(d));
		}
		System.out.println();
		
		for (String d : numbers) {
			System.out.println(usaNumberFormatter(d));
		}
		System.out.println();

		for (String d : numbers) {
			System.out.println(integerNumberFormatter(d));
		}
		System.out.println();
	}
}