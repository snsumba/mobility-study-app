package com.sers.mobility.study.app.constants;

public enum MethodType {
    GPS_DETERMINED("GPS Determined"),
	MANUAL_INPUT("Manual Input"),
	UNKNOWN("Unknown");

	private String name;

	MethodType(String name) {
		this.name = name;
	}

	/**
	 * This method gets the name or title of the enumerator element
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
