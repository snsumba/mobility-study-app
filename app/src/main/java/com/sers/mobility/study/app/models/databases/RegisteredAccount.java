package com.sers.mobility.study.app.models.databases;


import org.sers.kpa.datasource.annotations.Column;
import org.sers.kpa.datasource.annotations.Entity;
import org.sers.kpa.datasource.annotations.Table;
import org.sers.kpa.datasource.datamanager.DatabaseJSONUtils;

import java.util.Date;

@Entity
@Table(name = "registeredaccounts")
public class RegisteredAccount extends AppBaseEntity {

    @Column(name = "first_name", nullable = true)
    public String firstName = "Default";

    @Column(name = "last_name", nullable = true)
    public String lastName = "Default";

    @Column(name = "gender", nullable = true)
    public String gender = "male";

    @Column(name = "location", nullable = true)
    public String location = "Kampala";

    @Column(name = "phone_number", nullable = true)
    public String phoneNumber = "0700000000";

    @Column(name = "username", nullable = false)
    public String username = "Default";

    @Column(name = "password", nullable = false)
    public String password = "n.a";

    @Column(name = "date_of_birth", nullable = false)
    public Date dateOfBirth;

    public RegisteredAccount() {
    }


    public RegisteredAccount(String firstName, String lastName, String gender, String location,
                             String phoneNumber, String username, String password, Date dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.location = location;
        this.phoneNumber = phoneNumber;
        this.username = username;
        this.password = password;
        this.dateOfBirth = dateOfBirth;
    }

    public String toString() {
        try {
            return DatabaseJSONUtils.convertModelToJSON(this, RegisteredAccount.class).toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
