package com.sers.mobility.study.app.baseFiles;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sers.mobility.study.app.custom.SlidingTabLayout;
import com.sers.mobility.study.app.sessionManager.SessionManager;
import com.sers.mobility.study.app.utils.AppUtils;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BaseMainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */

public abstract class BaseMainFragment extends Fragment {
    public SlidingTabLayout tabLayout;
    public android.support.v4.app.FragmentManager compactibleFragmentManager;
    public OnFragmentInteractionListener mListener;
    public ViewPager pager;
    public CharSequence titles[];
    public int tabCount;
    public String userId;
    public String password;
    public String startDate = AppUtils.getCurrentDate();
    public String endDate = AppUtils.getCurrentDate();
    public Activity activity;
    public SessionManager sessionManager = null;
    public final int MY_PERMISSIONS_REQUEST_LOCATION = 1;
    public final int MY_PERMISSIONS_REQUEST_LOCATION_INITIAL = 2;
    public final int MY_PERMISSIONS_REQUEST_LOCATION_REFRESH = 3;


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


    public BaseMainFragment() {

    }

    public BaseMainFragment(Activity activity) {
        this.activity = activity;
        sessionManager = new SessionManager(activity);
        if (sessionManager != null) {
            HashMap<String, String> user = sessionManager.getUserDetails();
            userId = (user.get(SessionManager.KEY_USERNAME) != null ? user.get(SessionManager.KEY_USERNAME) : "n.a");
            password = (user.get(SessionManager.KEY_PASSWORD) != null ? user.get(SessionManager.KEY_PASSWORD) : "n.a");
        }
    }

    public BaseMainFragment(android.support.v4.app.FragmentManager compactibleFragmentManager, CharSequence titles[], int tabCount) {
        this.compactibleFragmentManager = compactibleFragmentManager;
        this.titles = titles;
        this.tabCount = tabCount;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public abstract View onCreateView(LayoutInflater inflater, ViewGroup container,
                                      Bundle savedInstanceState);

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void requestPermission(String permission, int request_code) {
        if (ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
            Log.e("Location Request", "Asking for permissions");
            ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, request_code);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
