package com.sers.mobility.study.app.models.databases;


import com.sers.mobility.study.app.constants.AppConstants;

import org.sers.kpa.datasource.annotations.Column;
import org.sers.kpa.datasource.annotations.Entity;
import org.sers.kpa.datasource.annotations.Table;

@Entity
@Table(name = "time_diary_category_suggestions")
public class AppTimeDiaryCategorySuggestion extends AppBaseEntity {


    @Column(name = "suggestion", nullable = false)
    public String suggestion;

    @Column(name = "suggestion_id", nullable = false)
    public String suggestionId;

    public AppTimeDiaryCategorySuggestion() {
    }

    public AppTimeDiaryCategorySuggestion(int pk, String suggestion) {
        super(pk);
        this.suggestion = suggestion;
    }

    public AppTimeDiaryCategorySuggestion(int pk, String suggestion, String suggestionId) {
        super(pk);
        this.suggestion = suggestion;
        this.suggestionId = suggestionId;
    }

    public AppTimeDiaryCategorySuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public AppTimeDiaryCategorySuggestion(String suggestion, String suggestionId) {
        this.suggestion = suggestion;
        this.suggestionId = suggestionId;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public String getSuggestionId() {
        return suggestionId;
    }

    public void setSuggestionId(String suggestionId) {
        this.suggestionId = suggestionId;
    }

    @Override
    public String toString() {
        return (suggestion != null ? suggestion : AppConstants.DEFAULT_DROP_DOWN_TEXT);
    }
}
