package com.sers.mobility.study.app.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.baseFiles.BaseMainFragment;
import com.sers.mobility.study.app.constants.AppToolBarTitles;
import com.sers.mobility.study.app.custom.NavigationController;
import com.sers.mobility.study.app.fragments.optionsMenu.ProfileFragment;

public class ProfileActivity extends AppCompatActivity implements BaseMainFragment.OnFragmentInteractionListener {

    private Toolbar toolbar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        AppCompatButton appCompatButton = (AppCompatButton) findViewById(R.id.backButton);
        appCompatButton.setVisibility(View.VISIBLE);
        appCompatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationController.navigateToActivity(ProfileActivity.this, getApplicationContext(), MainActivity.class);
            }
        });

        setSupportActionBar(toolbar);
        toolbar.setTitle(AppToolBarTitles.USER_PROFILE.getName());
        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new ProfileFragment(ProfileActivity.this)).commit();

    }

    @Override
    public void onBackPressed() {
        NavigationController.navigateToActivity(this, getApplicationContext(), MainActivity.class);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
