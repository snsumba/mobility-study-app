package com.sers.mobility.study.app.constants;


public interface AppConstants {
    String DOMAIN_CONTACT = "+256 703 819 876";
    String APPLICATION_DOMAIN = "Application Domain";
    /**
     * MENU TAB ENGLISH
     */
//    String TAB_HOME = "Home";
//    String TAB_TIME_DIARY = "Time Diary";
//    String TAB_PLACES_VISITED = "Places Visted";
//    String TAB_MAPPED_LOCATIONS = "Mapped Locations";
//    String TAB_CLICKABLE_MAP = "Pick Location";

    /**
     * MENU TAB SPANISH
     */
    String TAB_TIME_DIARY = "Diario";
    String TAB_PLACES_VISITED = "Lugares";
    String TAB_MAPPED_LOCATIONS = "Mapa";
    String TAB_CLICKABLE_MAP = "localización de la selección";

    String NO_RECORDS = "No records";
    String CONFIRM_OPERATION = "Confirm Message";
    String FEMALE = "Female";
    String MALE = "Male";
    String UNKNOWN = "Unknown";
    String LOCATION_NOT_FOUND = "Unidentified Location";
    double DEFAULT_DISTANCE_APART_IN_KILOMETRES = 0.001;
    String DEFAULT_ADDRESS = "88.99.94.208";
    String DEFUALT_LOCATION = "Defualt";
    //String USER_LOCATION_TITLE = "Current Location";
    String USER_LOCATION_TITLE = "Localización actual";
    String TIME_DIARY_TITLE = "Diario";
    //String TIME_DIARY_TITLE = "Time Diary";

    String DEFAULT_DROP_DOWN_TEXT = "None of this";

    String DEFAULT_APP_DURATION_IN_SECONDS = "30";
    String DEFAULT_APP_DISTANCE_IN_METRES = "100";

    String DEFAULT_APP_NOTIFICATION_DURATION_IN_MINUTES = "15";
    String DEFAULT_APP_NOTIFICATION_ENABLED = "yes";
    String DEFAULT_APP_NOTIFICATION_ENABLED_NO = "no";
}
