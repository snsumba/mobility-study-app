package com.sers.mobility.study.app.fragments.mainMenu;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.melnykov.fab.FloatingActionButton;
import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.baseFiles.BaseMainFragment;
import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.constants.AppMessages;
import com.sers.mobility.study.app.constants.MethodType;
import com.sers.mobility.study.app.constants.RecordStatus;
import com.sers.mobility.study.app.custom.AppMaterialDialog;
import com.sers.mobility.study.app.databaseUtils.UpdateDatabaseUtil;
import com.sers.mobility.study.app.gpsServices.GPSTracker;
import com.sers.mobility.study.app.models.databases.CapturedLocation;
import com.sers.mobility.study.app.sessionManager.SessionManager;
import com.sers.mobility.study.app.utils.AppUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class PickUserLocationsFragment extends BaseMainFragment implements OnMapReadyCallback {

    private GoogleMap googleMap;
    private SupportMapFragment mapFragment = null;
    private GPSTracker gpsTracker = null;
    private double latitude = 0;
    private double longitude = 0;
    private double accuracy = 0;
    private String locationName = AppConstants.DEFUALT_LOCATION;
    private TextView placeNameField, longitudeField, latitudeField, accuracyField;
    private Activity activity;
    private ProgressDialog progressDialog;
    private Geocoder geocoder = null;

    public PickUserLocationsFragment() {
    }

    @SuppressLint("ValidFragment")
    public PickUserLocationsFragment(Activity activity) {
        this.activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_mapped_locations, container, false);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            System.out.println("Version :: " + android.os.Build.VERSION.SDK_INT);
            System.out.println("Version :: " + android.os.Build.VERSION.SDK_INT);
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, MY_PERMISSIONS_REQUEST_LOCATION_INITIAL);
            }
        }

        progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);

        sessionManager = new SessionManager(activity);
        if (sessionManager != null) {
            HashMap<String, String> user = sessionManager.getUserDetails();
            userId = (user.get(SessionManager.KEY_USER_ID) != null ? user.get(SessionManager.KEY_USER_ID) : "n.a");
        }

        gpsTracker = new GPSTracker(activity);
        if (this.googleMap == null) {
            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gpsTracker = new GPSTracker(activity);
                if (googleMap == null) {
                    mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
                    mapFragment.getMapAsync(PickUserLocationsFragment.this);
                }
            }
        });

        return rootView;
    }


    private void setPopUpFields() {
        if (this.placeNameField != null)
            this.placeNameField.setText(String.valueOf(this.locationName));
        if (this.longitudeField != null)
            this.longitudeField.setText(String.valueOf(this.longitude));
        if (this.latitudeField != null)
            this.latitudeField.setText(String.valueOf(this.latitude));
        if (this.accuracyField != null)
            this.accuracyField.setText(String.valueOf(this.accuracy));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            System.out.println("Version :: " + android.os.Build.VERSION.SDK_INT);
            System.out.println("Version :: " + android.os.Build.VERSION.SDK_INT);
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, MY_PERMISSIONS_REQUEST_LOCATION_INITIAL);
            }
        }

        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);

        double defaultLatitude = 0;
        double defaultLongitude = 0;
        if (gpsTracker != null) {
            try {
                Location location = gpsTracker.getLocation();
                if (location != null) {
                    defaultLatitude = location.getLatitude();
                    defaultLongitude = location.getLongitude();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        LatLng latLng = new LatLng(defaultLatitude, defaultLongitude);
        this.googleMap.addMarker(new MarkerOptions().position(latLng).title("Pick Location").snippet("(" + defaultLatitude + "," + defaultLongitude + ")"));
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

        this.googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                progressDialog.setMessage("Loading location coordinates, Please Wait....");
                progressDialog.show();
                // getCurrentUserLocation(progressDialog);
                return false;
            }
        });

        this.googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                progressDialog.setMessage("Loading location coordinates, Please Wait....");
                progressDialog.show();
                LatLng latLng1 = latLng;
                if (latLng1 != null) {
                    getCurrentUserLocation(progressDialog, latLng1);
                } else progressDialog.dismiss();
            }
        });
    }

    private void getCurrentUserLocation(ProgressDialog progressDialog, LatLng latLng) {
        final MaterialDialog applicationDomainDialog = new AppMaterialDialog().appMaterialDialog(R.layout.popup_location_info,
                activity, AppConstants.USER_LOCATION_TITLE, R.string.default_text, R.string.negtive_dialog_text,
                R.string.save_text, Boolean.FALSE);

        View dialogView = applicationDomainDialog.getView();
        placeNameField = (TextView) dialogView.findViewById(R.id.placeNameField);
        longitudeField = (TextView) dialogView.findViewById(R.id.longitudeField);
        latitudeField = (TextView) dialogView.findViewById(R.id.latitudeField);
        accuracyField = (TextView) dialogView.findViewById(R.id.accuracyField);

        if (gpsTracker.canGetLocation()) {
            try {
                if (latLng != null) {
                    this.latitude = latLng.latitude;
                    this.longitude = latLng.longitude;
                    this.accuracy = 0.0;

                    System.out.println("Cordinates latitude : " + latitude);
                    System.out.println("Cordinates longitude : " + longitude);
                    geocoder = new Geocoder(activity, Locale.getDefault());
                    if (geocoder != null) {
                        List<Address> listAddresses = geocoder.getFromLocation(this.latitude, this.longitude, 1);
                        if (null != listAddresses && listAddresses.size() > 0) {
                            this.locationName = (listAddresses.get(0).getAddressLine(0) != null ? listAddresses.get(0).getAddressLine(0) : "Not found");
                        }
                    }
                    //new GetAddressPositionTask().onPreExecute();
                    setPopUpFields();
                } else {
                    setPopUpFields();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            gpsTracker.showSettingsAlert();
            setPopUpFields();
        }

        final MDButton save = (MDButton) applicationDomainDialog.getActionButton(DialogAction.POSITIVE);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (String.valueOf(longitudeField.getText()) == null || String.valueOf(latitudeField.getText()) == null
                        || String.valueOf(longitudeField.getText()).isEmpty() || String.valueOf(latitudeField.getText()).isEmpty()) {
                    AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Empty location coordinates", activity);
                    applicationDomainDialog.hide();
                } else {
                    CapturedLocation capturedLocation = new CapturedLocation(userId, String.valueOf(locationName), String.valueOf(longitude), String.valueOf(latitude), String.valueOf(accuracy));
                    capturedLocation.setMethodType(MethodType.MANUAL_INPUT.getName());
                    capturedLocation.status = RecordStatus.UN_SYNCED.getStatus();
                    capturedLocation.date = AppUtils.getCurrentDate();
                    new UpdateDatabaseUtil().saveOrUpdateCapturedLocation(capturedLocation);
                    applicationDomainDialog.hide();
                }
            }
        });
        applicationDomainDialog.show();
        progressDialog.dismiss();
    }

    private class GetAddressPositionTask extends AsyncTask<String, Integer, LatLng> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected LatLng doInBackground(String... plookupString) {

            String lookupString = plookupString[0];
            LatLng position = null;
            // best effort zoom
            try {
                if (geocoder != null) {
                    List<Address> addresses = geocoder.getFromLocationName(lookupString, 1);
                    if (addresses != null && !addresses.isEmpty()) {
                        Address first_address = addresses.get(0);
                        locationName = first_address.toString();
                        System.out.println("Cordinates locationName : " + locationName);
                        //position = new LatLng(first_address.getLatitude(), first_address.getLongitude());
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            // try HTTP lookup to the maps API

            return position;
        }

        @Override
        protected void onPostExecute(LatLng result) {
            super.onPostExecute(result);
        }

    }

    private void getCurrentUserLocation(ProgressDialog progressDialog) {
        progressDialog.show();
        final MaterialDialog applicationDomainDialog = new AppMaterialDialog().appMaterialDialog(R.layout.popup_location_info,
                activity, AppConstants.USER_LOCATION_TITLE, R.string.default_text, R.string.negtive_dialog_text,
                R.string.save_text, Boolean.FALSE);

        View dialogView = applicationDomainDialog.getView();
        placeNameField = (TextView) dialogView.findViewById(R.id.placeNameField);
        longitudeField = (TextView) dialogView.findViewById(R.id.longitudeField);
        latitudeField = (TextView) dialogView.findViewById(R.id.latitudeField);
        accuracyField = (TextView) dialogView.findViewById(R.id.accuracyField);

        if (gpsTracker.canGetLocation()) {
            Location location = null;
            try {
                location = gpsTracker.getLocation();
                if (location != null) {
                    this.latitude = location.getLatitude();
                    this.longitude = location.getLongitude();
                    this.accuracy = location.getAccuracy();

                    Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
                    List<Address> listAddresses = geocoder.getFromLocation(this.latitude, this.longitude, 1);
                    if (null != listAddresses && listAddresses.size() > 0) {
                        this.locationName = (listAddresses.get(0).getAddressLine(0) != null ? listAddresses.get(0).getAddressLine(0) : "Not found");
                    }
                    setPopUpFields();
                } else {
                    setPopUpFields();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            gpsTracker.showSettingsAlert();
            setPopUpFields();
        }

        final MDButton save = (MDButton) applicationDomainDialog.getActionButton(DialogAction.POSITIVE);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (String.valueOf(longitudeField.getText()) == null || String.valueOf(latitudeField.getText()) == null
                        || String.valueOf(longitudeField.getText()).isEmpty() || String.valueOf(latitudeField.getText()).isEmpty()) {
                    AppUtils.showMessageWithOutButton(true, AppMessages.ERROR_MESSAGE_TITLE, "Empty location coordinates", activity);
                    applicationDomainDialog.hide();
                } else {
                    CapturedLocation capturedLocation = new CapturedLocation(userId, String.valueOf(locationName), String.valueOf(longitude), String.valueOf(latitude), String.valueOf(accuracy));
                    capturedLocation.setMethodType(MethodType.MANUAL_INPUT.getName());
                    capturedLocation.status = RecordStatus.UN_SYNCED.getStatus();
                    capturedLocation.date = AppUtils.getCurrentDate();
                    capturedLocation.recordId = AppUtils.generateUserId();
                    new UpdateDatabaseUtil().saveOrUpdateCapturedLocation(capturedLocation);
                    applicationDomainDialog.hide();
                }
            }
        });
        applicationDomainDialog.show();
        progressDialog.dismiss();
    }

}
