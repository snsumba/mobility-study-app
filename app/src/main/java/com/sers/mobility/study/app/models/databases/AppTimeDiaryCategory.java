package com.sers.mobility.study.app.models.databases;


import com.sers.mobility.study.app.constants.AppConstants;

import org.sers.kpa.datasource.annotations.Column;
import org.sers.kpa.datasource.annotations.Entity;
import org.sers.kpa.datasource.annotations.Table;

@Entity
@Table(name = "time_diary_categories")
public class AppTimeDiaryCategory extends AppBaseEntity {

    @Column(name = "category", nullable = false)
    public String category;

    public AppTimeDiaryCategory() {
    }

    public AppTimeDiaryCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return (category != null ? category : AppConstants.DEFAULT_DROP_DOWN_TEXT);
    }
}
