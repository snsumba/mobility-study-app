package com.sers.mobility.study.app.sessionManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.sers.mobility.study.app.constants.MethodType;

import java.util.HashMap;


public class AppTypeManager {
    // Shared Preferences
    public SharedPreferences pref;
    // Editor for Shared preferences
    Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREF_NAME = "AppTypeManager";
    private static final String DEFAULT_APP_TYPE = MethodType.GPS_DETERMINED.getName();

    /**
     * make variables public to access from outside
     */
    public static final String KEY_DESIRED_METHOD = "desiredMethod";

    public AppTypeManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createAppPreference() {
        editor.putString(KEY_DESIRED_METHOD, DEFAULT_APP_TYPE);
        editor.commit();
    }


    public void editAppType(String appType) {
        if (appType != null) {
            editor.putString(KEY_DESIRED_METHOD, appType);
            editor.commit();
            System.out.println("GPS Commit Successful");
        }
    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getAppTypeSettings() {
        HashMap<String, String> domain = new HashMap<String, String>();
        //  Put application domain details in the hash map
        domain.put(KEY_DESIRED_METHOD, pref.getString(KEY_DESIRED_METHOD, null));
        // return user
        return domain;
    }

    /**
     * Clear session details
     */
    public boolean clearAppTypeManager() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        return true;

    }


}