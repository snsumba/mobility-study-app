package com.sers.mobility.study.app.activities;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.sers.mobility.study.app.R;
import com.sers.mobility.study.app.baseFiles.BaseMainFragment;
import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.constants.MethodType;
import com.sers.mobility.study.app.custom.NavigationController;
import com.sers.mobility.study.app.databaseUtils.UpdateDatabaseUtil;
import com.sers.mobility.study.app.fragments.MainApplicationFragment;
import com.sers.mobility.study.app.gpsServices.GPSTracker;
import com.sers.mobility.study.app.gpsServices.LocationService;
import com.sers.mobility.study.app.sessionManager.AppTypeManager;
import com.sers.mobility.study.app.sessionManager.SessionManager;
import com.sers.mobility.study.app.utils.LocationUtils;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements BaseMainFragment.OnFragmentInteractionListener {
    private Toolbar toolbar = null;
    private CharSequence titles[] = null;
    private int tabCount;
    private SessionManager sessionManager = null;
    private LocationUtils locationUtils = null;
    private final int MY_PERMISSIONS_REQUEST_LOCATION_INITIAL = 2;
    private LocationService localWordService;
    private GPSTracker gpsTracker;
    private AppTypeManager appTypeManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            System.out.println("Version SDK is greater");
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            System.out.println("Version :: " + android.os.Build.VERSION.SDK_INT);
            System.out.println("Version :: " + android.os.Build.VERSION.SDK_INT);
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, MY_PERMISSIONS_REQUEST_LOCATION_INITIAL);
            }
        }

        new UpdateDatabaseUtil().clearSyncedCapturedLocations();

        sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        locationUtils = new LocationUtils(this);

        gpsTracker = new GPSTracker(MainActivity.this);

        appTypeManager = new AppTypeManager(MainActivity.this);
        HashMap<String, String> appType = appTypeManager.getAppTypeSettings();
        String methodType = appType.get(AppTypeManager.KEY_DESIRED_METHOD);

        if (methodType != null) {
            if (methodType.equalsIgnoreCase(MethodType.GPS_DETERMINED.getName())) {
                //titles = new CharSequence[]{AppConstants.TAB_HOME, AppConstants.TAB_TIME_DIARY, AppConstants.TAB_PLACES_VISITED, AppConstants.TAB_MAPPED_LOCATIONS};
                titles = new CharSequence[]{AppConstants.TAB_TIME_DIARY, AppConstants.TAB_PLACES_VISITED, AppConstants.TAB_MAPPED_LOCATIONS};
            } else if (methodType.equalsIgnoreCase(MethodType.MANUAL_INPUT.getName())) {
                //titles = new CharSequence[]{AppConstants.TAB_HOME, AppConstants.TAB_TIME_DIARY, AppConstants.TAB_PLACES_VISITED, AppConstants.TAB_MAPPED_LOCATIONS, AppConstants.TAB_CLICKABLE_MAP};
                titles = new CharSequence[]{AppConstants.TAB_TIME_DIARY, AppConstants.TAB_PLACES_VISITED, AppConstants.TAB_MAPPED_LOCATIONS, AppConstants.TAB_CLICKABLE_MAP};
            } else {
                //titles = new CharSequence[]{AppConstants.TAB_HOME, AppConstants.TAB_TIME_DIARY, AppConstants.TAB_PLACES_VISITED, AppConstants.TAB_MAPPED_LOCATIONS};
                titles = new CharSequence[]{AppConstants.TAB_TIME_DIARY, AppConstants.TAB_PLACES_VISITED, AppConstants.TAB_MAPPED_LOCATIONS};
            }
        } else {
            //titles = new CharSequence[]{AppConstants.TAB_HOME, AppConstants.TAB_TIME_DIARY, AppConstants.TAB_PLACES_VISITED, AppConstants.TAB_MAPPED_LOCATIONS};
            titles = new CharSequence[]{AppConstants.TAB_TIME_DIARY, AppConstants.TAB_PLACES_VISITED, AppConstants.TAB_MAPPED_LOCATIONS};
        }
        tabCount = titles.length;
        getSupportFragmentManager().beginTransaction().replace(R.id.flContent,
                new MainApplicationFragment(getSupportFragmentManager(), titles, tabCount, MainActivity.this)).commit();

    }

    private void requestPermission(String permission, int request_code) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            Log.e("Location Request", "Asking for permissions");
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, request_code);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.aboutUs) {
            NavigationController.navigateToActivity(this, getApplicationContext(), AboutUsActivity.class);
            return true;
        } else if (id == R.id.profile) {
            NavigationController.navigateToActivity(this, getApplicationContext(), ProfileActivity.class);
            return true;
        } else if (id == R.id.settings) {
            NavigationController.navigateToActivity(this, getApplicationContext(), ApplicationSettingsActivity.class);
            return true;
        } else if (id == R.id.logout) {
            sessionManager.clearSessionManager();
            NavigationController.navigateToActivity(this, getApplicationContext(), LoginActivity.class);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            System.out.println("Version :: " + android.os.Build.VERSION.SDK_INT);
            System.out.println("Version :: " + android.os.Build.VERSION.SDK_INT);
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, MY_PERMISSIONS_REQUEST_LOCATION_INITIAL);
            }
        }
        Intent intent = new Intent(this, LocationService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(mConnection);
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder binder) {
            LocationService.MyBinder b = (LocationService.MyBinder) binder;
            localWordService = b.getService();
            Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT)
                    .show();
        }

        public void onServiceDisconnected(ComponentName className) {
            localWordService = null;
        }
    };


    /**
     * Is called when the Activity is being destroyed either by the system, or by the user,
     * say by hitting back, until the app exits.
     * Its compulsory that you save any user data that you want to persist in onDestroy(),
     * because the system will not do it for you.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
