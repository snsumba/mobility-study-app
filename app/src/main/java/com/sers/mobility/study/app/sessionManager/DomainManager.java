package com.sers.mobility.study.app.sessionManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;


public class DomainManager {
    // Shared Preferences
    public SharedPreferences pref;
    // Editor for Shared preferences
    Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREF_NAME = "DomainPreferences";
    //private static final String DEFAULT_APPLICATION_DOMAIN = "http://192.168.43.74:1919/mobility-study-gui";
    private static final String DEFAULT_APPLICATION_DOMAIN = "http://mcrops.jvmhost.net/study";

    /**
     * make variables public to access from outside
     */
    public static final String KEY_APPLICATION_DOMAIN = "applicationDomain";

    public DomainManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void createAppDomain() {
        editor.putString(KEY_APPLICATION_DOMAIN, DEFAULT_APPLICATION_DOMAIN);
        // commit changes
        editor.commit();
    }

    /**
     * Create login session
     */
    public void editAppDomain(String appDomain) {
        if (appDomain != null) {
            // Storing user id in pref
            editor.putString(KEY_APPLICATION_DOMAIN, appDomain);
            // commit changes
            editor.commit();
            System.out.println("*** Application domain edited in domain manager ..." + appDomain);
        }
    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getApplicationDomain() {
        HashMap<String, String> domain = new HashMap<String, String>();
        //  Put application domain details in the hash map
        domain.put(KEY_APPLICATION_DOMAIN, pref.getString(KEY_APPLICATION_DOMAIN, null));
        // return user
        return domain;
    }

    /**
     * Clear session details
     */
    public boolean clearApplicationDomain() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        return true;

    }


}