package com.sers.mobility.study.app.constants;


public interface AppMessages {
    String SUCCESS = "0";
    String FAILED = "-1";
    String SUCCESSFUL_LOGIN = "Successful Login";
    String ERROR_MESSAGE_TITLE = "Action Failed";
    String OPERATION_SUCCESSFUL_MESSAGE_TITLE = "Successful";
    String TURN_ON_GPS = "Dont you want to Enable GPS ?";
    String TURN_OFF_GPS = "Dont you want to Disable GPS ?";
    String GPS_STATUS_INITIAL_TEXT = "GPS Is Turned : ";
    String GPS_ALREADY_ON="GPS Is Already Turned ON";
    String GPS_ALREADY_OFF="GPS Is Already Turned OFF";
    String REBOOT_DEVICE = "Please ReBoot your Device for this feature to be effected";
    String REBOOT_TITLE = "Reboot";
}
