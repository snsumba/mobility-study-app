package com.sers.mobility.study.app.gpsServices;

/**
 * Created by kdeo on 4/14/2017.
 */

import java.util.Calendar;
import java.util.HashMap;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sers.mobility.study.app.sessionManager.AppDurationManager;
import com.sers.mobility.study.app.sessionManager.DomainManager;
import com.sers.mobility.study.app.sessionManager.SessionManager;

public class GPSIntervalScheduleReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmManager service = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intervalIntent = new Intent(context, StartGPSIntervalScheduleServiceReceiver.class);
        PendingIntent pending = PendingIntent.getBroadcast(context, 0, intervalIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        Calendar cal = Calendar.getInstance();
        // start 30 seconds after boot completed
        cal.add(Calendar.SECOND, 10);

        AppDurationManager appDurationManager = new AppDurationManager(context);
        HashMap<String, String> domain = appDurationManager.getAppDurationSettings();
        String distanceApart = domain.get(AppDurationManager.KEY_DISTANCE_IN_METRES);
        String durationInSeconds = domain.get(AppDurationManager.KEY_DURATION_IN_SECONDS);
        long longDistanceApart = 100;
        long longDurationInSeconds = 30;
        if (distanceApart != null) {
            if (distanceApart.matches("[0-9]+")) {
                longDistanceApart = new Long(distanceApart);
            }
        }

        if (durationInSeconds != null) {
            if (durationInSeconds.matches("[0-9]+")) {
                longDurationInSeconds = new Long(durationInSeconds);
            }
        }

        long REPEAT_TIME = 1000 * longDurationInSeconds;

        // fetch every REPEAT_TIME seconds
        // InexactRepeating allows Android to optimize the energy consumption
        service.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), REPEAT_TIME, pending);
        service.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), REPEAT_TIME, pending);

    }
}
