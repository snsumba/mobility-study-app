package com.sers.mobility.study.app.constants;


public enum AppToolBarTitles {
    SETTINGS("Settings"),
    USER_PROFILE("Profile"),
    REGISTER_ACCOUNT("Register Account"),
    ABOUT_US("About Us"),
    TIME_DIARY_CATEGORY("Time Diary Category"),
    LOGOUT("Logout");

    private String name;

    AppToolBarTitles(String name) {
        this.name = name;
    }

    /**
     * This method gets the name or title of the enumerator element
     *
     * @return the name
     */
    public String getName() {
        return name;
    }
}
