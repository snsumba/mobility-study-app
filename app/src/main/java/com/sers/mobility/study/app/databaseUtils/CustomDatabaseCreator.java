package com.sers.mobility.study.app.databaseUtils;


import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.sers.mobility.study.app.constants.AppConstants;
import com.sers.mobility.study.app.models.databases.AppTimeDiaryCategory;
import com.sers.mobility.study.app.models.databases.AppTimeDiaryCategorySuggestion;
import com.sers.mobility.study.app.models.databases.CapturedLocation;
import com.sers.mobility.study.app.models.databases.RegisteredAccount;
import com.sers.mobility.study.app.models.databases.TimeDiary;

import org.sers.kpa.datasource.datamanager.DatabaseAdapter;

import java.io.Serializable;

public class CustomDatabaseCreator implements Serializable {

    String DATABASE_NAME = "db_mobility_study";
    private Context context;

    public CustomDatabaseCreator(Context context) {
        this.context = context;
        DatabaseAdapter.Util.createInstance(this.context, DATABASE_NAME,
                new Class<?>[]{CapturedLocation.class, RegisteredAccount.class,
                        AppTimeDiaryCategory.class, AppTimeDiaryCategorySuggestion.class, TimeDiary.class});
    }


}
