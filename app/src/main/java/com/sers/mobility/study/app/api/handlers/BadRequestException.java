package com.sers.mobility.study.app.api.handlers;

import retrofit.RetrofitError;

public class BadRequestException extends Throwable {
    private String detail;
    private RetrofitError error;



    public BadRequestException(String s, RetrofitError error) {
        detail = s;
        this.error = error;
    }
    public String getDetail() {
        return detail;
    }
    public void setDetail(String content) {
        this.detail = content;
    }

    public RetrofitError getError() {
        return error;
    }

    public void setError(RetrofitError error) {
        this.error = error;
    }

}
